# Redux简介

Redux 是 JavaScript 状态容器，提供可预测化的状态管理

# Redux的工作流程图

<img src="E:\01.前端\15.React\React(张天宇)\1.React\images\Redux的工作流程图.png" style="zoom:150%;" />

# Redux的三个核心概念

## action

> 动作的对象
>
> 包含两个属性：type：标识属性，值为字符串，唯一，必要属性------data：数据属性，值类型任意，可选属性
>
> {type:"addCount",data:{age:10}}

## reducer

> 用于初始化状态，加工状态
>
> 加工的时候根据旧的state和action，产生新的state的纯函数

## store

> 将state、action、reducer联系在一起的对象
>
> 如何得到此对象
>
> ​	import {createStore} from"redux
>
> ​	import reducer from "./reducers"
>
> 此对象的功能
>
> ​	getState() 得到state
>
> ​	dispatch(action) 分发action，触发reducer调用，产生新的state
>
> ​	subscribe(listener) 注册监听，产生了新的state时，自动调用

# 求和案例--react版本

```jsx
import React, {Component} from 'react';

class Count extends Component {
    constructor(props) {
        super(props)
        this.state = {value: 1, count: 0}
    }

    /*双向数据绑定*/
    changeHandle = (event) => {
        const {value} = event.target
        this.setState({value: Number(value)})
    }

    //加
    increment = () => {
        let {value, count} = this.state
        this.setState({count: value += count})
    }
    //减
    decrement = () => {
        let {value, count} = this.state
        this.setState({count: count - value})
    }

    // 求和为奇数加
    incrementIfAdd = () => {
        let {value, count} = this.state
        if (count % 2 !== 0) {
            this.setState({count: value + count})
        }
    }
    //异步加
    incrementAsync = () => {
        let {value, count} = this.state
        setTimeout(() => {
            this.setState({count: value + count})
        }, 1000)

    }


    render() {
        const {count, value} = this.state
        return (
            <div className="count">
                <h1>当前求和为:{count}</h1>
                <select value={value} onChange={this.changeHandle}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
                <button onClick={this.increment}>+</button>
                <button onClick={this.decrement}>-</button>
                <button onClick={this.incrementIfAdd}>当前求和为奇数再加</button>
                <button onClick={this.incrementAsync}>异步加</button>
            </div>
        );
    }
}

export default Count;
```

# 求和案例--redux精简版

```jsx
import React, {Component} from 'react';
// 引入store用于获取redux中保存的状态
import countStore from "../../redux/countStore/countStore/countStore";


class Count extends Component {
    constructor(props) {
        super(props)
        this.state = {value: 1}
    }

    /*双向数据绑定*/
    changeHandle = (event) => {
        const {value} = event.target
        this.setState({value: Number(value)})
    }

    //加
    increment = () => {
        const {value} = this.state
        countStore.dispatch({type: "increment", data: value})

    }
    //减
    decrement = () => {
        const {value} = this.state
        countStore.dispatch({type: "decrement", data: value})
    }

    // 求和为奇数加
    incrementIfAdd = () => {
        const {value} = this.state
        const count = countStore.getState()
        if (count % 2 !== 0) {
            countStore.dispatch({type: "increment", data: value})
        }
    }
    //异步加
    incrementAsync = () => {
        setTimeout(() => {
            const {value} = this.state
            countStore.dispatch({type: "increment", data: value})
        },1000)
    }

    componentDidMount() {
        // 使用store.subscribe(callback)检测redux中状态的变化，只要变化了就会调用store.subscribe传入的回调函数
        countStore.subscribe(() => {
            // 强制更新视图 this.forceUpdate()
            // 调用setState()更新
            this.setState({})
        })
    }

    render() {
        const {value} = this.state
        const {getState} = countStore
        return (
            <div className="count">
                <h1>当前求和为:{getState()}</h1>
                <select value={value} onChange={this.changeHandle}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
                <button onClick={this.increment}>+</button>
                <button onClick={this.decrement}>-</button>
                <button onClick={this.incrementIfAdd}>当前求和为奇数再加</button>
                <button onClick={this.incrementAsync}>异步加</button>
            </div>
        );
    }
}

export default Count;
```

```jsx
/*
* 该文件专门用于暴露一个store对象，整个应用只有一个store对象
* */

// 引入createStore，专门用于创建redux最核心的store对象
import {createStore} from "redux"

// 引入为Count组件服务的reducer
import countReducer from "../countReducer/countReducer";

// 默认导出调用createStore函数并且传入为Count组件服务的countReeducer
export default createStore(countReducer)
```

```jsx
/*
* 1.该文件用于创建一个为Count组件服务的reducer，（reducer的本质是一个函数）
* 2.reducer函数可以接到两个参数分别为：之前的状态(preState)、动作对象(action)
* */

// 初始化的状态
const initSate = 0
export default function countReducer(preState = initSate, action) {
    // 从action动作对象中解构赋值得到type和data
    const {data, type} = action
    // 根据type决定如何加工数据
    switch (type) {
        case "increment":
            return preState + data
        case "decrement":
            return preState -data
        default:
            return preState
    }
}
```

# 求和案例-redux完整版

```jsx
import React, {Component} from 'react';

import countStore from "../../redux/countStore/countStore/countStore"

import {createIncrementAction, createDecrementAction} from "../../redux/countStore/countAction/countAction"

class Count extends Component {
    constructor(props) {
        super(props)
        this.state = {value: 1}
    }

    /*双向数据绑定*/
    changeHandle = (event) => {
        const {value} = event.target
        this.setState({value: Number(value)})
    }

    //加
    increment = () => {
        const {value} = this.state
        countStore.dispatch(createIncrementAction(value))
    }
    //减
    decrement = () => {
        const {value} = this.state
        countStore.dispatch(createDecrementAction(value))
    }

    // 求和为奇数加
    incrementIfAdd = () => {
        const {value} = this.state
        if (countStore.getState() % 2 !== 0) {
            countStore.dispatch(createIncrementAction(value))
        }

    }
    //异步加
    incrementAsync = () => {
        setTimeout(() => {
            const {value} = this.state
            countStore.dispatch(createIncrementAction(value))
        }, 1000)
    }

    componentDidMount() {
        countStore.subscribe(()=>{
            this.forceUpdate()
        })
    }


    render() {
        const {value} = this.state

        return (
            <div className="count">
                <h1>当前求和为:{countStore.getState()}</h1>
                <select value={value} onChange={this.changeHandle}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
                <button onClick={this.increment}>+</button>
                <button onClick={this.decrement}>-</button>
                <button onClick={this.incrementIfAdd}>当前求和为奇数再加</button>
                <button onClick={this.incrementAsync}>异步加</button>
            </div>
        );
    }
}

export default Count;
```

```jsx
import {createStore} from "redux"
import countReducer from "../countReducer/countReducer"
export default createStore(countReducer)
```

```jsx
/*
* 该文件用于创建action对象
*
* */
import {decrement, increment} from "../countConst/countConst";
export const createIncrementAction = (data) => ({type: increment, data})
export const createDecrementAction = (data) => ({type: decrement, data})
```

```JavaScript
export const increment = "increment"
export const decrement = "decrement"
```

```JavaScript
import {decrement, increment} from "../countConst/countConst";

const initState = 0

export default function countReducer(preState = initState, action) {
    const {type, data} = action
    switch (type) {
        case increment:
            return preState + data
        case decrement:
            return preState - data
        default:
            return preState
    }
}
```

# 求和案例-redux-异步action

```jsx
import React, {Component} from 'react';

import countStore from "../../redux/countStore/countStore/countStore"

import {createIncrementAction, createDecrementAction, createIncrementAsyncAction} from "../../redux/countStore/countAction/countAction"

class Count extends Component {
    constructor(props) {
        super(props)
        this.state = {value: 1}
    }

    /*双向数据绑定*/
    changeHandle = (event) => {
        const {value} = event.target
        this.setState({value: Number(value)})
    }

    //加
    increment = () => {
        const {value} = this.state
        countStore.dispatch(createIncrementAction(value))
    }
    //减
    decrement = () => {
        const {value} = this.state
        countStore.dispatch(createDecrementAction(value))
    }

    // 求和为奇数加
    incrementIfAdd = () => {
        const {value} = this.state
        if (countStore.getState() % 2 !== 0) {
            countStore.dispatch(createIncrementAction(value))
        }

    }
    //异步加
    incrementAsync = () => {
        const {value} = this.state
        countStore.dispatch(createIncrementAsyncAction(value, 1000))
    }

    componentDidMount() {
        countStore.subscribe(() => {
            this.forceUpdate()
        })
    }


    render() {
        const {value} = this.state

        return (
            <div className="count">
                <h1>当前求和为:{countStore.getState()}</h1>
                <select value={value} onChange={this.changeHandle}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
                <button onClick={this.increment}>+</button>
                <button onClick={this.decrement}>-</button>
                <button onClick={this.incrementIfAdd}>当前求和为奇数再加</button>
                <button onClick={this.incrementAsync}>异步加</button>
            </div>
        );
    }
}

export default Count;
```

```jsx
import {createStore, compose, applyMiddleware} from 'redux'
// 引入redux-thunk用于支持函数类型的action
import thunk from 'redux-thunk'
import countReducer from "../countReducer/countReducer"

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
export default createStore(countReducer, composeEnhancers(applyMiddleware(thunk)))

```

```JavaScript
export const increment = "increment"
export const decrement = "decrement"
```

```JavaScript
/*
* 该文件用于创建action对象
*
* */
import countStore from "../countStore/countStore";
import {decrement, increment} from "../countConst/countConst";
/*
* 所谓的同步action，就是指action返回的值为对象
* */
export const createIncrementAction = (data) => ({type: increment, data})
export const createDecrementAction = (data) => ({type: decrement, data})

/*
* 所谓的异步action，就是指action返回的值为函数
* */
export const createIncrementAsyncAction = (data, time) => {
    return () => {
        setTimeout(() => {
            countStore.dispatch(createIncrementAction(data))
        }, time)
    }
}
```

```JavaScript
import {decrement, increment} from "../countConst/countConst";

const initState = 0

export default function countReducer(preState = initState, action) {
    const {type, data} = action
    switch (type) {
        case increment:
            return preState + data
        case decrement:
            return preState - data
        default:
            return preState
    }
}
```