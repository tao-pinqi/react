/*
* 1.该文件用于创建一个为Count组件服务的reducer，（reducer的本质是一个函数）
* 2.reducer函数可以接到两个参数分别为：之前的状态(preState)、动作对象(action)
* */

// 初始化的状态
const initSate = 0
export default function countReducer(preState = initSate, action) {
    // 从action动作对象中解构赋值得到type和data
    const {data, type} = action
    // 根据type决定如何加工数据
    switch (type) {
        case "increment":
            return preState + data
        case "decrement":
            return preState - data
        default:
            return preState
    }
}
