import {createStore, compose, applyMiddleware} from 'redux'
// 引入redux-thunk用于支持函数类型的action
import thunk from 'redux-thunk'
import countReducer from "../countReducer/countReducer"

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
export default createStore(countReducer, composeEnhancers(applyMiddleware(thunk)))
