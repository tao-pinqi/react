/*
* 该文件用于创建action对象
*
* */
import countStore from "../countStore/countStore";
import {decrement, increment} from "../countConst/countConst";
/*
* 所谓的同步action，就是指action返回的值为对象
* */
export const createIncrementAction = (data) => ({type: increment, data})
export const createDecrementAction = (data) => ({type: decrement, data})

/*
* 所谓的异步action，就是指action返回的值为函数
* */
export const createIncrementAsyncAction = (data, time) => {
    return () => {
        setTimeout(() => {
            countStore.dispatch(createIncrementAction(data))
        }, time)
    }
}
