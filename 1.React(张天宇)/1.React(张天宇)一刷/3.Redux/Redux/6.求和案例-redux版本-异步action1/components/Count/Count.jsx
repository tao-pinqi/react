import React, {Component} from 'react';

import countStore from "../../redux/countStore/countStore/countStore"

import {createIncrementAction, createDecrementAction, createIncrementAsyncAction} from "../../redux/countStore/countAction/countAction"

class Count extends Component {
    constructor(props) {
        super(props)
        this.state = {value: 1}
    }

    /*双向数据绑定*/
    changeHandle = (event) => {
        const {value} = event.target
        this.setState({value: Number(value)})
    }

    //加
    increment = () => {
        const {value} = this.state
        countStore.dispatch(createIncrementAction(value))
    }
    //减
    decrement = () => {
        const {value} = this.state
        countStore.dispatch(createDecrementAction(value))
    }

    // 求和为奇数加
    incrementIfAdd = () => {
        const {value} = this.state
        if (countStore.getState() % 2 !== 0) {
            countStore.dispatch(createIncrementAction(value))
        }

    }
    //异步加
    incrementAsync = () => {
        const {value} = this.state
        countStore.dispatch(createIncrementAsyncAction(value, 1000))
    }

    componentDidMount() {
        countStore.subscribe(() => {
            this.forceUpdate()
        })
    }


    render() {
        const {value} = this.state

        return (
            <div className="count">
                <h1>当前求和为:{countStore.getState()}</h1>
                <select value={value} onChange={this.changeHandle}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
                <button onClick={this.increment}>+</button>
                <button onClick={this.decrement}>-</button>
                <button onClick={this.incrementIfAdd}>当前求和为奇数再加</button>
                <button onClick={this.incrementAsync}>异步加</button>
            </div>
        );
    }
}

export default Count;
