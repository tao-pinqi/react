/*
* 该文件专门用于暴露一个store对象，整个应用只有一个store对象
* */

// 引入createStore，专门用于创建redux最核心的store对象
import {createStore} from "redux"

// 引入为Count组件服务的reducer
import countReeducer from "./count_reducer"

// 调用createStore函数并且传入为Count组件服务的countReeducer
const store = createStore(countReeducer)

// 默认导出创建好的store
export default store
