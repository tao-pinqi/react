/*
* 1.该文件用于创建一个为Count组件服务的reducer，（reducer的本质是一个函数）
* 2.reducer函数可以接到两个参数分别为：之前的状态(preState)、动作对象(action)
* */

const initState = 0 // 初始化的状态

export default function countReducer(preState = initState, action) {
    // 从action动作对象中解构赋值得到type和data
    const {type, data} = action
    // 根据type决定如何加工数据
    switch (type) {
        case "increment": // 加
            return preState + data
        case "decrement": // 减
            return preState - data
        default:
            return preState
    }
}
