import React, {Component} from 'react';

class Count extends Component {
    constructor(props) {
        super(props)
        this.state = {value: 1, count: 0}
    }

    /*双向数据绑定*/
    changeHandle = (event) => {
        const {value} = event.target
        this.setState({value: Number(value)})
    }

    //加
    increment = () => {
        let {value, count} = this.state
        this.setState({count: value += count})
    }
    //减
    decrement = () => {
        let {value, count} = this.state
        this.setState({count: count - value})
    }

    // 求和为奇数加
    incrementIfAdd = () => {
        let {value, count} = this.state
        if (count % 2 !== 0) {
            this.setState({count: value + count})
        }
    }
    //异步加
    incrementAsync = () => {
        let {value, count} = this.state
        setTimeout(() => {
            this.setState({count: value + count})
        }, 1000)

    }


    render() {
        const {count, value} = this.state
        return (
            <div className="count">
                <h1>当前求和为:{count}</h1>
                <select value={value} onChange={this.changeHandle}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
                <button onClick={this.increment}>+</button>
                <button onClick={this.decrement}>-</button>
                <button onClick={this.incrementIfAdd}>当前求和为奇数再加</button>
                <button onClick={this.incrementAsync}>异步加</button>
            </div>
        );
    }
}

export default Count;
