/*
* 该文件用于创建action对象
*
* */
import {decrement, increment} from "../countConst/countConst";

export const createIncrementAction = (data) => ({type: increment, data})

export const createDecrementAction = (data) => ({type: decrement, data})
