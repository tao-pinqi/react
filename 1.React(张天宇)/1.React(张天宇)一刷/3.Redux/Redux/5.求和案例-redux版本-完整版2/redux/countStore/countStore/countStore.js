import {createStore} from "redux"

import countReducer from "../countReducer/countReducer"

export default createStore(countReducer)
