# ![]()SPA应用的理解

单页Web应用（single page web application，SPA），就是只有一张Web页面的应用，是加载单个HTML 页面并在用户与应用程序交互时动态更新该页面的Web应用程序。

## SPA应用的优缺点

> 优点：
> 1、用户操作体验好，用户不用刷新页面，整个交互过程都是通过Ajax来操作。
> 2、适合前后端分离开发，服务端提供http接口，前端请求http接口获取数据，使用JS进行客户端渲染。

> 缺点：
> 1、首页加载慢
> 单页面应用会将js、 css打包成一个文件，在加载页面显示的时候加载打包文件，如果打包文件较大或者网速慢则用户体验不好，首屏时需要请求一次html，同时还要发送一次js请求，两次请求回来了，首屏才会展示出来。相对于多页应用
>
> 2、SEO不友好
> SEO效果差，因为搜索引擎只认识html里的内容，不认识js的内容，而单页应用的内容都是靠js渲染生成出来的，搜索引擎不识别这部分内容，也就不会给一个好的排名，会导致单页应用做出来的网页在百度和谷歌上的排名差。使用单页面应用将大大减少搜索引擎对网站的收录。

# 路由的理解

## 什么是路由

一个路由就是一个映射关系(key:value)

key为路径，value可能是function或者component

## 路由分类

<img src="E:\01.前端\15.React\React(张天宇)\1.React\images\路由分类.png" style="zoom: 200%;" />

# 下载react-router-dom

```
npm i react-router-dom@5   
```

# 路由的基本使用

我们需要做的第一件事是将我们的 `<App>` 组件包裹在一个 `<Router>` 组件中(由React Router提供)

由于我们正在建立的是一个基于浏览器的 web 应用程序，我们可以使用 React Router API 中的两种类型的路由：分别是BrowserRouter、HashRouter

```
// <BrowserRouter>
http://example.com/about

// <HashRouter>
http://example.com/#/about
```

## BrowserRouter

```javascript
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, HashRouter} from "react-router-dom";
import App from './App';
// 创建一个history模式的路由
ReactDOM.render(<BrowserRouter><App></App></BrowserRouter>, document.querySelector("#root"))
```

## HashRouter

```javascript
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, HashRouter} from "react-router-dom";
import App from './App';
// 创建一个hash模式的路由
ReactDOM.render(<HashRouter><App></App></HashRouter>, document.querySelector("#root"))
```

## Link 

`<Link>` 组件被用来在页面之间进行导航，它其实就是 HTML 中的 `<a>` 标签的上层封装，不过在其源码中使用 `event.preventDefault` 禁止了其默认行为，然后使用history API自己实现了跳转,如果使用 `<a>` 标签去进行导航的话，整个页面都会被刷新

```jsx
import React, {Component} from 'react';
// 引入Link声明式导航
import {Link} from "react-router-dom";
import "./Aside.css"

class Aside extends Component {
    render() {
        return (
            <div className="aside">
                {/*
                   Link组件是声明式导航组件里面有个属性to表示要去的路径
                        to：要访问的路径，当Link组件的to属性和Route组件的path属性匹配后则展示Route所指定的component属性的组件
                */}
                <Link to="/home">home</Link>
                <Link to="/about">about</Link>
            </div>
        );
    }
}

export default Aside;
```

## Redirect

React Router的Redirect组件将会用一个新的位置替换历史栈中的当前位置，新的位置是由 `to` 属性来指向的

```html
// 路由重定向
<Redirect to="/home"></Redirect>
```

## Route

`<Route>` 组件是 React Router 中最重要的组件了，如果当前的位置与路由的路径匹配，就会渲染对应的 UI

理想情况下，`<Route>` 组件应该有一个名为 `path` 的属性，如果路径名称与当前位置匹配，`component`属性所对应的组件就会被渲染

```html
<Route path="/home" component={Home}></Route>

<Route path="/about" component={About}></Route>
```

```JavaScript
import React from 'react';
import ReactDOM from 'react-dom';
/*
引入BrowserRouter, HashRouter组件
	BrowserRouter：路径没有#
	HashRouter：路径后面携带hash值
*/
import {BrowserRouter, HashRouter} from "react-router-dom";
import App from './App';
ReactDOM.render(<BrowserRouter><App></App></BrowserRouter>, document.querySelector("#root"))
```

```jsx
import React, {Component} from 'react';
// 引入Link声明式导航
import {Link} from "react-router-dom";
import "./Aside.css"

class Aside extends Component {
    render() {
        return (
            <div className="aside">
                {/*
                   Link组件是声明式导航组件里面有个属性to表示要去的路径
                        to：要访问的路径，当Link组件的to属性和Route组件的path属性匹配后则展示Route所指定的component属性的组件
                */}
                <Link to="/home">home</Link>
                <Link to="/about">about</Link>
            </div>
        );
    }
}

export default Aside;
```

```jsx
import React from "react";
import Header from "./components/Header/Header";
import Aside from "./components/Aside/Aside";
import Home from "./components/Home/Home";
import About from "./components/About/About";
// 引入Redirect路由重定向组件和Route路由规则组件
import {Redirect, Route} from "react-router-dom";
import "./App.css"

export default class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="app">
                {/*Header头部组件*/}
                <Header></Header>
                <div className="body">
                    {/*侧边栏组件*/}
                    <Aside></Aside>
                    {/*
                        Route是路由的规则组件里面有两个属性分别是path路径和component组件
                        路由规则:
                            path属性：当访问path为/home的时候则展示Home组件
                            component属性：所匹配的组件
                    */}
                    <Redirect to="/home"></Redirect>
                    <Route path="/home" component={Home}></Route>
                    <Route path="/about" component={About}></Route>
                </div>
            </div>
        );
    }

}
```

# 路由组件接收到的props参数

![](E:\01.前端\15.React\React(张天宇)\1.React\images\路由组件接收到的props参数.png)

# NavLink

路径选中时，对应的a元素变为红色，这个时候要使用NavLink组件来替代Link组件

> activeStyle属性：活跃时（匹配时）的样式
>
> activeClassName属性：活跃时添加的class（默认是active类名）
>
> exact属性：是否精准匹配

```jsx
import React, {Component} from 'react';
import {Link, NavLink} from "react-router-dom";
import "./Aside.css"

class Aside extends Component {
    render() {
        return (
            <div className="aside">
                {/*
                 路径选中时，对应的a元素变为红色，这个时候要使用NavLink组件来替代Link组件
                     activeStyle属性：活跃时（匹配时）的样式
                     activeClassName属性：活跃时添加的class（默认是active类名）
                     exact属性：是否精准匹配
                */}
                <NavLink to="/home">home</NavLink>
                <NavLink to="/about">about</NavLink>
            </div>
        );
    }
}

export default Aside;
```

二次封装NavLink

```jsx
import React, {Component} from 'react';
import {Link, NavLink} from "react-router-dom";
import MyNavLink from "../MyNavLink/MyNavLink";
import "./Aside.css"

class Aside extends Component {
    render() {
        return (
            <div className="aside">
                <MyNavLink to="/home">home</MyNavLink>
                <MyNavLink to="/about">about</MyNavLink>
            </div>
        );
    }
}

export default Aside;
```

```jsx
import React, {Component} from "react";
import {Link, NavLink} from "react-router-dom";

export default class MyNavLink extends Component {
    constructor(props) {
        super(props)
        this.state = {color: "#fff"}
    }

    render() {
        const {props, state: {color}} = this
        // return <NavLink activeClassName="active" activeStyle={{color}} {...props}>{props.children}</NavLink>
        return <NavLink activeClassName="active" activeStyle={{color}} {...props}></NavLink>
    }
}
```

# Switch

Switch组件可以提高路由的匹配效率，Switch只显示匹配到的第一个路由组件

```jsx
import React from "react";
import Header from "./components/Header/Header";
import Aside from "./components/Aside/Aside";
import Home from "./views/Home/Home";
import About from "./views/About/About";
import Test from "./views/Test/Test";
import {Redirect, Route, Switch} from "react-router-dom";
import "./App.css"

export default class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="app">
                {/*Header头部组件*/}
                <Header></Header>
                <div className="body">
                    {/*侧边栏组件*/}
                    <Aside></Aside>
                    {/*
                        Route是路由的规则组件里面有两个属性分别是path路径和component组件
                        路由规则:
                            path属性：当访问path为/home的时候则展示Home组件
                            component属性：所匹配的组件
                    */}
                    <Redirect to="/home"></Redirect>
                    <Switch>
                        <Route path="/home" component={Home}></Route>
                        <Route path="/about" component={About}></Route>
                        <Route path="/about" component={Test}></Route>
                    </Switch>
                </div>
            </div>
        );
    }
}
```

# 路由的严格匹配和模糊匹配

模糊匹配是React Router的默认匹配方式。在模糊匹配中，路由会根据URL的路径部分进行匹配。当URL的路径部分与路由的路径部分部分匹配时，就会触发匹配。

```jsx
import React from "react";
import Header from "./components/Header/Header";
import Aside from "./components/Aside/Aside";
import Home from "./views/Home/Home";
import About from "./views/About/About";
import Test from "./views/Test/Test";
import {Redirect, Route, Switch} from "react-router-dom";
import "./App.css"

export default class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="app">
                {/*Header头部组件*/}
                <Header></Header>
                <div className="body">
                    {/*侧边栏组件*/}
                    <Aside></Aside>
                    {/*
                        Route是路由的规则组件里面有两个属性分别是path路径和component组件
                        路由规则:
                            path属性：当访问path为/home的时候则展示Home组件
                            component属性：所匹配的组件
                    */}
                    <Redirect to="/home"></Redirect>
                    <Switch>
                   {/*
          模糊匹配是React Router的默认匹配方式。在模糊匹配中，路由会根据URL的路径部分进行匹配。当URL的路径部分与路由的路径部分部分匹配时，就会触发匹配。
          exact属性开启精准匹配
                   */}
                        <Route path="/home" component={Home} exact></Route>
                        <Route path="/about" component={About} exact></Route>
                    </Switch>
                </div>
            </div>
        );
    }
}
```

# Redirect路由重定向

`Redirect`组件用于在路由匹配时进行页面重定向。当某个路由匹配成功时，`Redirect`组件会将用户重定向到指定的URL。

使用`Redirect`组件可以实现以下功能：

> 页面重定向：在路由匹配时将用户重定向到指定的URL

```jsx
import React from "react";
import Header from "./components/Header/Header";
import Aside from "./components/Aside/Aside";
import Home from "./views/Home/Home";
import About from "./views/About/About";
import Test from "./views/Test/Test";
import {Redirect, Route, Switch} from "react-router-dom";
import "./App.css"

export default class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="app">
                {/*Header头部组件*/}
                <Header></Header>
                <div className="body">
                    {/*侧边栏组件*/}
                    <Aside></Aside>
                    {/*路由重定向*/}
                    <Redirect to="/home"></Redirect>
                    {/*Switch组件可以提高路由的匹配效率，Switch只显示匹配到的第一个路由组件*/}
                    <Switch>
                    {/*
                        Route是路由的规则组件里面有两个属性分别是path路径和component组件
                        路由规则:
                            path属性：当访问path为/home的时候则展示Home组件
                            component属性：所匹配的组件
                    */}
                        <Route path="/home" component={Home}></Route>
                        <Route path="/about" component={About}></Route>
                    </Switch>
                </div>
            </div>
        );
    }
}
```

# 嵌套路由

```jsx
import React from "react";
import Header from "./components/Header/Header";
import Aside from "./components/Aside/Aside";
import Home from "./views/Home/Home";
import About from "./views/About/About";
import Test from "./views/Test/Test";
import {Redirect, Route, Switch} from "react-router-dom";
import "./App.css"

export default class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="app">
                {/*Header头部组件*/}
                <Header></Header>
                <div className="body">
                    {/*侧边栏组件*/}
                    <Aside></Aside>
                    {/*
                        Route是路由的规则组件里面有两个属性分别是path路径和component组件
                        路由规则:
                            path属性：当访问path为/home的时候则展示Home组件
                            component属性：所匹配的组件
                    */}
                    <Redirect to="/home"></Redirect>
                    <Switch>
                        <Route path="/home" component={Home}></Route>
                        <Route path="/about" component={About}></Route>
                    </Switch>
                </div>
            </div>
        );
    }
}
```

```jsx
import React, {Component} from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import MyNavLink from "../../components/MyNavLink/MyNavLink";
import Message from "./Message/Message";
import News from "./News/News";
import "./Home.css"

class Home extends Component {
    render() {
        return (
            <div className="home">
                我是home
                <div className="nav-list">
                    <MyNavLink to="/home/message">message</MyNavLink>
                    <MyNavLink to="/home/news">news</MyNavLink>
                </div>
                {/*子级路由的路径需要携带父级路由的路径*/}
                <Redirect to="/home/message"></Redirect>
                <Switch>
                    <Route path="/home/message" component={Message}></Route>
                    <Route path="/home/news" component={News}></Route>
                </Switch>
            </div>
        );
    }
}

export default Home;
```

# 传递params参数

![](E:\01.前端\15.React\React(张天宇)\1.React\images\路由组件传递params.png)

```jsx
import React, {Component} from 'react';
import {Link, Redirect, Route} from "react-router-dom";
import Detail from "./Detail";
class Message extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: [{id: "01", title: "消息1"}, {id: "02", title: "消息2"}, {id: "03", title: "消息3"}]
        }
    }

    render() {
        const {message} = this.state
        return (
            <div className="message">
                <ul>
                {/*1.向路由组件传递params参数*/}
 				{message.map(item =><Link key={item.id} to={`/home/message/detail/${item.id}/${item.title}`}>{item.title}</Link>)}
                </ul>
                <hr/>
                <Redirect to="/home/message/detail"></Redirect>
                {/*2.声明params参数占位符*/}
                <Route path="/home/message/detail/:id/:title" component={Detail}></Route>
            </div>
        );
    }
}

export default Message;
```

```jsx
import React, {Component} from 'react';
class Detail extends Component {
    render() {
        /*3.接收传递过来的params参数*/
        const {id, title} = this.props.match.params
        return (
            <div className="detail">
                <ul>
                    <li>ID:{id}</li>
                    <li>TITLE:{title}</li>
                </ul>
            </div>
        );
    }
}

export default Detail;
```

# 传递search参数

![](E:\01.前端\15.React\React(张天宇)\1.React\images\路由传递search参数.png)

```jsx
import React, {Component} from 'react';
import {Link, Redirect, Route} from "react-router-dom";
import Detail from "./Detail";

class Message extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: [{id: "01", title: "消息1"}, {id: "02", title: "消息2"}, {id: "03", title: "消息3"}]
        }
    }

    render() {
        const {message} = this.state
        return (
            <div className="message">
                <ul>
{/*向路由组件传递search（query）参数*/}
{message.map(item => <li key={item.id}><Link key={item.id} to={`/home/message/detail/?id=${item.id}&title=${item.title}`}>{item.title}</Link></li>)}
                </ul>
                <hr/>
                <Redirect to="/home/message/detail"></Redirect>
                <Route path="/home/message/detail" component={Detail}></Route>
            </div>
        );
    }
}

export default Message;
```

```jsx
import React, {Component} from 'react';

class Detail extends Component {
    render() {
        /*接收传递过来的search（query）参数*/
        console.log(this.props.location.search)
        return (
            <div className="detail">
                <ul>
                    <li>ID:{}</li>
                    <li>TITLE:{}</li>
                </ul>
            </div>
        );
    }
}

export default Detail;
```

# 传递state参数

![](E:\01.前端\15.React\React(张天宇)\1.React\images\路由传递state参数.png)

```jsx
import React, {Component} from 'react';
import {Link, Redirect, Route} from "react-router-dom";
import Detail from "./Detail";

class Message extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: [{id: "01", title: "消息1"}, {id: "02", title: "消息2"}, {id: "03", title: "消息3"}]
        }
    }

    render() {
        const {message} = this.state
        return (
            <div className="message">
                <ul>
                    {/*向路由组件传递state参数*/}
                    {message.map(item => <li key={item.id}><Link key={item.id} to={{pathname: "/home/message/detail", state: {id: item.id, title: item.title}}}>{item.title}</Link></li>)}
                </ul>
                <hr/>
                <Redirect to="/home/message/detail"></Redirect>
                <Route path="/home/message/detail" component={Detail}></Route>
            </div>
        );
    }
}

export default Message;
```

```jsx
import React, {Component} from 'react';

class Detail extends Component {
    render() {
        /*接收传递过来的state参数*/
        const {state}=this.props.location
        return (
            <div className="detail">
                <ul>
                    <li>ID:{state.id}</li>
                    <li>TITLE:{state.title}</li>
                </ul>
            </div>
        );
    }
}

export default Detail;
```

# 编程式路由

![](E:\01.前端\15.React\React(张天宇)\1.React\images\编程式路由导航.png)

```jsx
import React, {Component} from 'react';
import "./Home.css"

class Home extends Component {
    pushShow = () => {
        // this.props.history.replace(`/about?id=1`)
        // this.props.history.replace(`/about/1`)
        this.props.history.replace({pathname: "/about", state: {id: 1}})

    }

    replaceShow = () => {
        // this.props.history.push(`/home?id=2`)
        // this.props.history.replace(`/about/2`)
        this.props.history.replace({pathname: "/home", state: {id: 2}})

    }

    render() {
        return (
            <div className="home">
                我是home
                <button onClick={this.pushShow}>push</button>
                <button onClick={this.replaceShow}>replace</button>
            </div>
        );
    }
}

export default Home;
```

# withRouter函数

使用Route包裹组件， 将react-router的history, location, match信息通过props传递给包裹的组件

默认情况下必须是经过路由匹配渲染的组件才存在this.props，才拥有路由参数，才能使用 函数跳转 的写法，执行this.props.history.push('/***')跳转到对应路由的页面。

然而不是所有组件都直接与路由相连（通过路由跳转到此组件）的，当这些组件需要路由参数时，使用withRouter就可以给此组件传入路由参数，此时就可以使用this.props。

```jsx
import React from 'react'
import { withRouter } from 'react-router-dom'

export default withRouter((props) => {
	const { history, location, match } = props
	return <div>{location.pathname}</div>
})
```

