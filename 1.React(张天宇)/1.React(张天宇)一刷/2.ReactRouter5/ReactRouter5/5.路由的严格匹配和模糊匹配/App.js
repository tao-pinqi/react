import React from "react";
import Header from "./components/Header/Header";
import Aside from "./components/Aside/Aside";
import Home from "./views/Home/Home";
import About from "./views/About/About";
import Test from "./views/Test/Test";
import {Redirect, Route, Switch} from "react-router-dom";
import "./App.css"

export default class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="app">
                {/*Header头部组件*/}
                <Header></Header>
                <div className="body">
                    {/*侧边栏组件*/}
                    <Aside></Aside>
                    {/*
                        Route是路由的规则组件里面有两个属性分别是path路径和component组件
                        路由规则:
                            path属性：当访问path为/home的时候则展示Home组件
                            component属性：所匹配的组件
                    */}
                    <Redirect to="/home"></Redirect>
                    <Switch>
                        {/*
                        模糊匹配是React Router的默认匹配方式。在模糊匹配中，路由会根据URL的路径部分进行匹配。当URL的路径部分与路由的路径部分部分匹配时，就会触发匹配。
                        exact属性开启精准匹配
                        */}
                        <Route path="/home" component={Home} exact></Route>
                        <Route path="/about" component={About} exact></Route>
                    </Switch>
                </div>
            </div>
        );
    }
}