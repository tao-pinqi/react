import React, {Component} from 'react';
import {Link, NavLink} from "react-router-dom";
import MyNavLink from "../MyNavLink/MyNavLink";
import "./Aside.css"

class Aside extends Component {
    render() {
        return (
            <div className="aside">
                <MyNavLink to="/home">home</MyNavLink>
                <MyNavLink to="/about">about</MyNavLink>
            </div>
        );
    }
}

export default Aside;