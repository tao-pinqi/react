import React, {Component} from 'react';

class Detail extends Component {
    render() {
        /*接收传递过来的state参数*/
        const {state}=this.props.location
        return (
            <div className="detail">
                <ul>
                    <li>ID:{state.id}</li>
                    <li>TITLE:{state.title}</li>
                </ul>
            </div>
        );
    }
}

export default Detail;