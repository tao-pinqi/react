import React, {Component} from 'react';
import {Redirect, Route} from "react-router-dom";
import MyNavLink from "../../components/MyNavLink/MyNavLink";
import Message from "./Message/Message";
import News from "./News/News";
import "./Home.css"

class Home extends Component {
    render() {
        return (
            <div className="home">
                我是home
                <div className="nav-list">
                    <MyNavLink to="/home/message">message</MyNavLink>
                    <MyNavLink to="/home/news">news</MyNavLink>
                </div>
                {/*子级路由的路径需要携带父级路由的路径*/}
                <Redirect to="/home/message"></Redirect>
                <Route path="/home/message" component={Message}></Route>
                <Route path="/home/news" component={News}></Route>
            </div>
        );
    }
}

export default Home;