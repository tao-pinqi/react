import React, {Component} from "react";
import {Link, NavLink} from "react-router-dom";

export default class MyNavLink extends Component {
    constructor(props) {
        super(props)
        this.state = {color: "#fff"}
    }

    render() {
        const {props, state: {color}} = this
        // return <NavLink activeClassName="active" activeStyle={{color}} {...props}>{props.children}</NavLink>
        return <NavLink activeClassName="active" activeStyle={{color}} {...props}></NavLink>
    }
}