import React, {Component} from 'react';
import {Link, Redirect, Route} from "react-router-dom";
import Detail from "./Detail";

class Message extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: [{id: "01", title: "消息1"}, {id: "02", title: "消息2"}, {id: "03", title: "消息3"}]
        }
    }

    render() {
        const {message} = this.state
        return (
            <div className="message">
                <ul>
                    {/*向路由组件传递search（query）参数*/}
                    {message.map(item => <li key={item.id}><Link key={item.id} to={`/home/message/detail/?id=${item.id}&title=${item.title}`}>{item.title}</Link></li>)}
                </ul>
                <hr/>
                <Redirect to="/home/message/detail"></Redirect>
                <Route path="/home/message/detail" component={Detail}></Route>
            </div>
        );
    }
}

export default Message;