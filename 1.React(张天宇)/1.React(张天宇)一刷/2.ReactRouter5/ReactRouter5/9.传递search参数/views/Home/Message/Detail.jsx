import React, {Component} from 'react';

class Detail extends Component {
    render() {
        /*接收传递过来的search（query）参数*/
        console.log(this.props.location.search)
        return (
            <div className="detail">
                <ul>
                    <li>ID:{}</li>
                    <li>TITLE:{}</li>
                </ul>
            </div>
        );
    }
}

export default Detail;