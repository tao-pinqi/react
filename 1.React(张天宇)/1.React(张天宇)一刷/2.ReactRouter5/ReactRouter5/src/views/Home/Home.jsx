import React, {Component} from 'react';
import "./Home.css"

class Home extends Component {
    pushShow = () => {
        // this.props.history.replace(`/about?id=1`)
        // this.props.history.replace(`/about/1`)
        this.props.history.replace({pathname: "/about", state: {id: 1}})

    }

    replaceShow = () => {
        // this.props.history.push(`/home?id=2`)
        // this.props.history.replace(`/about/2`)
        this.props.history.replace({pathname: "/home", state: {id: 2}})

    }

    render() {
        return (
            <div className="home">
                我是home
                <button onClick={this.pushShow}>push</button>
                <button onClick={this.replaceShow}>replace</button>
            </div>
        );
    }
}

export default Home;