import React, {Component} from 'react';

class Detail extends Component {
    render() {
        /*接收传递过来的params参数*/
        const {id, title} = this.props.match.params
        return (
            <div className="detail">
                <ul>
                    <li>ID:{id}</li>
                    <li>TITLE:{title}</li>
                </ul>
            </div>
        );
    }
}

export default Detail;