import React, {Component} from 'react';
import {Link, NavLink} from "react-router-dom";
import "./Aside.css"

class Aside extends Component {
    render() {
        return (
            <div className="aside">
                {/*
                    Link组件是声明式导航组件里面有个属性to表示要去的路径
                    to：要访问的路径，当Link组件的to属性和Route组件的path属性匹配后则展示Route所指定的component属性的组件
                    <Link to="/home">home</Link>
                    <Link to="/about">about</Link>
               */}

                {/*
                 路径选中时，对应的a元素变为红色，这个时候要使用NavLink组件来替代Link组件
                     activeStyle属性：活跃时（匹配时）的样式
                     activeClassName属性：活跃时添加的class（默认是active类名）
                     exact属性：是否精准匹配
                */}
                <NavLink to="/home">home</NavLink>
                <NavLink to="/about">about</NavLink>
            </div>
        );
    }
}

export default Aside;