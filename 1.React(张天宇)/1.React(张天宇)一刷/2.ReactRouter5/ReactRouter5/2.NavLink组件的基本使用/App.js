import React from "react";
import Header from "./components/Header/Header";
import Aside from "./components/Aside/Aside";
import Home from "./views/Home/Home";
import About from "./views/About/About";
import {Redirect, Route} from "react-router-dom";
import "./App.css"

export default class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="app">
                {/*Header头部组件*/}
                <Header></Header>
                <div className="body">
                    {/*侧边栏组件*/}
                    <Aside></Aside>
                    {/*
                        Route是路由的规则组件里面有两个属性分别是path路径和component组件
                        路由规则:
                            path属性：当访问path为/home的时候则展示Home组件
                            component属性：所匹配的组件
                    */}
                    <Redirect to="/home"></Redirect>
                    <Route path="/home" component={Home}></Route>
                    <Route path="/about" component={About}></Route>
                </div>
            </div>
        );
    }
}