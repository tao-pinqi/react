import React, {Component} from 'react';
import {Link} from "react-router-dom";
import "./Aside.css"

class Aside extends Component {
    render() {
        return (
            <div className="aside">
                {/*
                   Link组件是声明式导航组件里面有个属性to表示要去的路径
                        to：要访问的路径，当Link组件的to属性和Route组件的path属性匹配后则展示Route所指定的component属性的组件
                */}
                <Link to="/home">home</Link>
                <Link to="/about">about</Link>
            </div>
        );
    }
}

export default Aside;