const proxy = require("http-proxy-middleware")
module.exports = function (app) {
    app.use(
        proxy("/api1", { // 遇见api前缀的请求触发代理
            target: "http://ajax-api.itheima.net",  // 请求转发的地址
            changeOrigin: true, // 控制服务器收到的响应头Host字段的值
            pathRewrite: {"^/api": ""}  // 路径重写
        })
    )
}