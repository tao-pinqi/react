import request from "../utils/request";
/*登录的接口*/
export const loginAPI=(data)=>{
    return request({
        method:"POST",
        url:"/login",
        data
    })
}