import React from "react";
import {loginAPI} from "./API/loginAPI";

export default class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {username: "admin", password: "123456"}
    }

    // 发送登录请求的方法
    handleSubmit = async (event) => {
        event.preventDefault()
        try {
            const {data: {message}} = await loginAPI(this.state)
            alert(message)
        } catch (e) {
            alert(e.message)
        }
    }

    // 数据绑定
    saveFormData = (dataType) => {
        return (event) => {
            this.setState({
                [dataType]: event.target.value
            })
        }
    }

    render() {
        const {username, password} = this.state
        return (
            <div className="app">
                <form onSubmit={this.handleSubmit}>
                    <input type="text" value={username} onChange={this.saveFormData("username")}/>
                    <input type="password" value={password} onChange={this.saveFormData("password")}/>
                    <button>登录</button>
                </form>
            </div>
        )
    }
}