import React from "react";
import Item from "../Item/Item";
import "./Main.css"

export default class Main extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        // 从props接收父组件传递过来的todos和changeTodo
        const {todos,changeTodo,deleteTodo} = this.props
        return (
            <div className="Main">
                {/*循环渲染每一个Item并且给每一个子组件Item传递id唯一标识 name名字和done状态*/}
                {/* {todos.map(todo => <Item key={todo.id} name={todo.name} done={todo.done}></Item>)} */}
                {/*批量传递props*/}
                {todos.map(todo => <Item key={todo.id} {...todo} changeTodo={changeTodo} deleteTodo={deleteTodo}></Item>)}
            </div>
        );
    }
}