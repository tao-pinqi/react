import React from "react";

export default class Header extends React.Component {
    constructor(props) {
        super(props);
        // todo的名字
        this.state = {name: ""}
    }


    // 双向数据绑定的方法
    handleChange = (event) => {
        this.setState({name: event.target.value})
    }

    // 处理键盘抬起的方法
    handleKeyUp = (event) => {
        if (event.keyCode !== 13) return
        if (this.state.name.trim()==="") return alert("您输入的todo不能为空")

        // 创建一个新的todo
        const newTodo = {name: this.state.name, id: Math.random(), done: true}
        // 调用父组件传递过来的props函数并且传值用于子组件将值传递给父组件
        this.props.addTodo(newTodo)
        // 清空数据
        this.setState({name:""})
    }

    render() {
        const {name} = this.state
        return (
            <div className="Header">
                <input type="text" name="" placeholder="请输入您的任务名称按回车确认" className="layui-input" value={name} onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
            </div>
        );
    }
}