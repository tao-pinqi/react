import React from "react";
import "./Footer.css"

export default class Footer extends React.Component {
    constructor(props) {
        super(props);
    }

    // 点击删除已完成任务的方法
    deleteChecked = () => {
        this.props.deleteCheckedTodo()
    }


    // 全选按钮的回调函数
    changeAllChecked = (event) => {
        this.props.changeAllChecked(event.target.checked)

    }

    render() {
        const {todos} = this.props
        // 是否全选
        const isAllChecked = todos.every(todo => todo.done)
        return (
            <div className="Footer">
                <div className="left">
                    <input type="checkbox" checked={isAllChecked&&todos.length!==0} onChange={this.changeAllChecked}/>
                    <span>已经完成{todos.filter(todo => todo.done).length}/ 全部{todos.length}</span>
                </div>
                <div className="right">
                    <button type="button" className="layui-btn layui-bg-red layui-btn-sm" onClick={this.deleteChecked}>删除已完成任务</button>
                </div>
            </div>
        );
    }
}