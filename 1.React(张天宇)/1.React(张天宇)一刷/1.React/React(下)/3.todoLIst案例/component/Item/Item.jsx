import React from "react";
import "./Item.css"

export default class Item extends React.Component {
    constructor(props) {
        super(props);
        this.state = {flag: false}
    }

    // 修改flag的方法
    changeFlag = (flag) => {
        return () => {
            this.setState({flag})
        }
    }

    // 复选框发生改变的方法
    changeChecked = (done, id) => {
        this.props.changeTodo({done, id})
    }

    // 点击删除按钮的方法
    clickHandle = (id) => {
        return () => {
            this.props.deleteTodo(id)
        }
    }

    render() {
        // 接收父组件传递过来的id、name、done
        const {id, name, done} = this.props
        const {flag} = this.state
        return (
            <div className="Item" onMouseEnter={this.changeFlag(true)} onMouseLeave={this.changeFlag(false)} style={{backgroundColor: flag ? "#ddd" : ""}}>
                <div className="state">
                    <input type="checkbox" checked={done} onChange={event => this.changeChecked(event.target.checked, id)}/>
                    <span>{name}</span>
                </div>
                <div className="delete">
                    <button type="button" className="layui-btn layui-btn-sm" style={{display: flag ? "block" : "none"}} onClick={this.clickHandle(id)}>删除</button>
                </div>
            </div>
        );
    }
}