import React from "react";
import "./App.css"
import Children from "./components/Children/Children";

export default class App extends React.Component {
    constructor(props) {
        super(props)
        this.state={count:0}
    }

    setFatherCount=(count)=>{
        this.setState({count})
    }

    render() {
        const {count}=this.state
        return (
            <div className="app">
                <h1>我是App父组件 接收到子组件Children传递过来的值是{count}</h1>
                {/*父组件给子组件传递一个方法*/}
                <Children setFatherCount={this.setFatherCount}></Children>
            </div>
        )
    }
}