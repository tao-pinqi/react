import React from "react";
import "./Children.css"

export default class Children extends React.Component {
    constructor(props) {
        super(props)
        this.state = {count: 0}
    }

    clickHandle = () => {
        this.setState({
            count: this.state.count -= 1
        }, () => {
            //子组件调用父组件传递过来的方法，并且进行传参数
            this.props.setFatherCount(this.state.count)
        })
    }

    render() {
        const {count} = this.state
        return (
            <div className="children">
                <h1>我是Children子组件 count值是{count}</h1>
                <button onClick={this.clickHandle}>+1</button>
            </div>
        )
    }

}