import request from "../utils/request";
export const searchAPI = (keyword) => {
    return request({
        url: "/search/users",
        params: {
            q: keyword
        }
    })
}