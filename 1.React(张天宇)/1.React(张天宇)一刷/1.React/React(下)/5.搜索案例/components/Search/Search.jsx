import React from "react";
import "./Search.css"
import {searchAPI} from "../../api/SearchAPI";

export default class Search extends React.Component {
    constructor(props) {
        super(props)
    }

    clickHandle = async () => {
        try {
            const {keyWordElement: {value: keyword}} = this
            const {items} = await searchAPI(keyword)
            this.props.saveUsers(items)
        } catch (error) {
            alert(error.message)
        }
    }

    render() {
        return (
            <section className="jumbotron">
                <h3 className="jumbotron-heading">搜索Github用户：</h3>
                <div>
                    <input type="text" placeholder="输入关键字点击搜索" ref={c => this.keyWordElement = c}/>
                    <button onClick={this.clickHandle}>搜索</button>
                </div>
            </section>
        )
    }
}