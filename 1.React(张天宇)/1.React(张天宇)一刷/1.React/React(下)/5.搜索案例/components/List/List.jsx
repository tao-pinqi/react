import React from "react";
import "./List.css"

export default class List extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {users} = this.props
        return (
            <div className="row">
                {
                    users.map(user => {
                        return (
                            <div className="card" key={user.id}>
                                <a href={user.html_url} target="_blank">
                                    <img src={user.avatar_url} style={{width: "100px"}}/>
                                </a>
                                <p className="card-text">{user.login}</p>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}