import React from "react";
import Search from "./components/Search/Search";
import List from "./components/List/List";
import "./App.css"

export default class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            users: []
        }
    }

    saveUsers = (items) => {
        this.setState({
            users: items
        })
    }

    render() {
        return (
            <div className="app">
                <Search saveUsers={this.saveUsers}></Search>
                <List  users={this.state.users}></List>
            </div>
        )
    }
}