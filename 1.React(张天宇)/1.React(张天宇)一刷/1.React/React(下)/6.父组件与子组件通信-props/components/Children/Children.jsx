import React from "react";
import "./Children.css"
export default class Children extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        // 接收父组件传递过来的props
        const {count}=this.props
        return (
            <div className="children">
                <h1>我是Children子组件 接收到父组件传递过来的count值是{count}</h1>
            </div>
        )
    }

}