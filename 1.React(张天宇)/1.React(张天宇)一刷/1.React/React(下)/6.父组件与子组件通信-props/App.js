import React from "react";
import "./App.css"
import Children from "./components/Children/Children";

export default class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {count: 0}
    }
    addCount=()=>{
        this.setState({
            count:this.state.count+=1
        })
    }
    render() {
        const {count}=this.state
        return (
            <div className="app">
                <h1>我是App父组件 count的值是{count}</h1>
                <button onClick={this.addCount}>+1</button>
                {/*通过props将数据传递给子组件*/}
                <Children count={count}></Children>
            </div>
        )
    }
}