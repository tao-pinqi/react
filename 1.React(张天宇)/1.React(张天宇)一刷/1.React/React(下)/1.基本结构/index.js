// 引入React核心库
import React from "react";
// 引入ReactDom库
import ReactDom from "react-dom"
// 引入App根组件
import App from "./App";
// 使用render函数渲染App组件并且指定挂载点
ReactDom.render(<App></App>,document.querySelector("#root"))