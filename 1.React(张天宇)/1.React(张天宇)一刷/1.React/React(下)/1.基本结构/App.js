// 引入React核心库
import React from "react";
// 默认导出App组件并且基继承React.Component
export default class App extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return(
            <div className="app"></div>
        )
    }
}