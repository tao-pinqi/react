import React from "react";
import Header from "./component/Header/Header";
import Footer from "./component/Footer/Footer";

export default class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="app">
                <Header></Header>
                <Footer></Footer>
            </div>
        )
    }
}