# 什么是React

React是用于构建用户界面的JavaScript框架。



# React的优点

React采用声明范式可以轻松描述应用。通过虚拟DOM,最大限度的减少与DOM的交互，速度很快

使用JSX代码可读性很好，React可以与已有的库或者框架很好的配合

使用React构建组件使代码更加容易得到复用，可以很好的应用在大项目的开发过程中，其单向数据流减少了重复的代码，轻松实现数据绑定。




# React核心包

<img src="E:\01.前端\15.React\images\React的核心包.png" style="zoom:150%;" />

## React

React：React核心库

## ReactDom

ReactDom：用于支持React操作DOM

## babel

babel：用于将jsx转换为js





# 初体验React

```html
<body>
<!--准备好一个容器-->
<div id="root"></div>
<!--引入React核心库-->
<script src="../../javascript/react.development.js"></script>
<!--引入React-dom，用于支持React操作DOM-->
<script src="../../javascript/react-dom.development.js"></script>
<!--引入babel，用于将jsx转换为js-->
<script src="../../javascript/babel.min.js"></script>

<script type="text/babel">
    // 创建一虚拟DOM
    const vdom=<h1>hello-react</h1>
    // 渲染虚拟DOM到指定的容器
    ReactDOM.render(vdom,document.querySelector("#root"))
</script>
</body>
```





# 创建虚拟DOM两种方式

在React中，虚拟DOM是一种用于优化UI渲染性能的技术。

它是React的核心概念之一，通过使用虚拟DOM，React能够高效地比较和更新实际DOM的变化，在React中有两种主要的方式来创建虚拟DOM：JSX和React.createElement()

## JSX创虚拟DOM

```html
<body>
<div id="root"></div>
<script src="../../javascript/react.development.js"></script>
<script src="../../javascript/react-dom.development.js"></script>
<script src="../../javascript/babel.min.js"></script>

<script type="text/babel">
    // 使用JSX语法创建一虚拟DOM
    const vdom = <h1>hello-jsx</h1>
    console.log(vdom)
    ReactDOM.render(vdom, document.querySelector("#root"))
</script>
</body>
```

## React.createElement()

```html
<body>
<div id="root"></div>
<script src="../../javascript/react.development.js"></script>
<script src="../../javascript/react-dom.development.js"></script>
<script>
    const vdom = React.createElement("div", {className: "box"}, "我是一个div元素")
    console.log(vdom)
    ReactDOM.render(vdom, document.querySelector("#root"))
</script>
</body>
```





# 虚拟DOM与真实DOM

![](E:\01.前端\15.React\images\虚拟DOM与真实DOM.png)

```html
<body>
<div id="root"></div>
<script src="../../javascript/react.development.js"></script>
<script src="../../javascript/react-dom.development.js"></script>
<script src="../../javascript/babel.min.js"></script>

<script type="text/babel">
    /*
    * 关于虚拟DOM：
    *   本质是Object类型的对象（一般对象）
    *   虚拟DOM比较'轻',真实DOM比较'重'，因为虚拟DOM是React内部在使用，不需要真实DOM上那么的属性
    *   虚拟DOM最终会被React转换为真实DOM渲染在页面上
    * */
    const vdom = <h1>hello-jsx</h1>
    const tdom = document.querySelector("#root")
    console.log('虚拟DOM', vdom)
    console.log('真实DOM', tdom)
    ReactDOM.render(vdom, document.querySelector("#root"))
</script>
</body>
```





# JSX语法规则

> * 定义虚拟DOM的时候不需要写引号
> * 标签中混入JS表达式的时候要使用{}
> * 样式的类名指定不要用class、需要使用className
> * 内联的样式要用style={{key:value}}的形式书写
> * 必须只有一个根标签
> * 标签必须严格的闭合
> * 标签首字母：如果小写字母开头，则将标签改成html中同名元素、如果大写字母开头，则渲染对应的组件

```html
<body>
<div id="root"></div>
<script src="../../javascript/react.development.js"></script>
<script src="../../javascript/react-dom.development.js"></script>
<script src="../../javascript/babel.min.js"></script>
<script type="text/babel">
    /*
    * JSX的语法规则：
    *   定义虚拟DOM的时候不需要写引号
    *   标签中混入JS表达式的时候要使用{}
    *   样式的类名指定不要用class、需要使用className
    *   内联的样式要用style={{key:value}}的形式书写
    *   必须只有一个根标签
    *   标签必须严格的闭合
    *   标签首字母：如果小写字母开头，则将标签改成html中同名元素、如果大写字母开头，则渲染对应的组件
    * */
    const message="您好"
    const vdom = (
        <div>
            <h1 style={{color: "red"}}>hello</h1>
            <h1 className={"box"}>hello</h1>
            <h1 className={"box"}>{message}</h1>
        </div>
    )
    ReactDOM.render(vdom, document.querySelector("#root"))
</script>
</body>
```





# JSX初体验

> * JS语句和JS表达式：
>   * 表达式会产生一个值，可以放在任何一个需要值的地方。
>   * 语句不会产生一个值
>   * 一般情况下，在js里每一行就是一个语句。
>   * 语句是为了完成某种任务而进行的操作，比如赋值语句：var a = 1+3 在这条语句中，上面的1+3就是表达式。
>   * 语句和表达式的区别在于，语句是为了进行某种操作，一般情况下不需要返回值，而表达式都是为了得到返回值，一定会返回一个值（这里的值不包括undefined）

```html
<body>
<div id="root"></div>

<script src="../../javascript/react.development.js"></script>
<script src="../../javascript/react-dom.development.js"></script>
<script src="../../javascript/babel.min.js"></script>

<script type="text/babel">
    /*
    * JS语句和JS表达式：
    *   表达式会产生一个值，可以放在任何一个需要值的地方
    *   语句不会产生一个值
    *  一般情况下，在js里每一行就是一个语句。
    *   语句是为了完成某种任务而进行的操作，比如赋值语句：var a = 1+3 在这条语句中，上面的1+3就是表达式。
    *   语句和表达式的区别在于，语句是为了进行某种操作，一般情况下不需要返回值，而表达式都是为了得到返回值，一定会返回一个值（这里的值不包括undefined）
    * */
    const array = ["Vue", "React", "Angular"]
    const vdom = (
        <div>
            <h1>前端JavaScript三大框架</h1>
            <ul>
                {array.map((item, index) => <li key={index}>{item}</li>)}
            </ul>
        </div>
    )
    ReactDOM.render(vdom, document.querySelector("#root"))
</script>
</body>
```





# 模块与组件

## 模块

> 理解：向外提供特点功能的js程序，一般就是一个js文件
>
> 为什么要拆分成模块：随着业务的增长，代码越来越多且复杂
>
> 作用：复用js的编写提高js运行效率

## 组件

> 理解：用来实现局部功能效果的代码集合
>
> 为什么：一个界面的功能更复杂
>
> 作用：复用代码，简化项目编码提高运行效率



# 模块化与组件化

## 模块化

> 当应用的js都以模块来编写，这个应用就是一个模块化的应用

## 组件化

> 当应用是是以多组件的方式实现，这个应用就是一个组件化的应用





# 类的基本知识

> 类中的构造器不是必须要写的，要对实例进行初始化操作的时候，如指定属性时才写
>
> 如果A类继承了B类，且A类中写了构造函数，那么A类中的构造函数中super()必须要写
>
> 类中定义的方法放在了类的原型对象上，供类的实例使用 

```JavaScript
/*
*类中的构造器不是必须要写的，要对实例进行初始化操作的时候，如指定属性时才写
*如果A类继承了B类，且A类中写了构造函数，那么A类中的构造器函数中super()必须要写   
*类中定义的方法放在了类的原型对象上，供类的实例使用 
* */
// 创建一个类
class Person {
    constructor(name, age) {
        // 实例属性 (构造器中的this指向类的实例对象)
        this.name = name
        this.age = age
    }

    // 实例方法(放在类的原型对象上，给实例使用,其this指向调用者)
    speak() {
        console.log(`我叫${this.name},我的年龄是${this.age}`)
    }
}

// 创建一个Person的实例对象
const p1 = new Person("马云", 55)
const p2 = new Person("马化腾", 50)
console.log(p1)
console.log(p2)
p1.speak()
p2.speak()


//创建一个Student类继承Person类
class Student extends Person {
    constructor(name, age, grade) {
        super(name, age)
        this.grade = grade
    }

    // 重写父类的实例方法
    speak() {
        console.log(`我叫${this.name},我的年龄是${this.age},我读的是${this.grade}`)
    }

    // 实例方法(放在类的原型对象上，给实例使用,其this指向调用者)
    study() {
        console.log(`我叫${this.name}我在学习`)
    }
}

// 创建一个Student的实例对象
const s1 = new Student("张三", 16, "高一")
const s2 = new Student("李四", 18, "高二")
console.log(s1)
console.log(s2)
s1.speak()
s2.speak()
s1.study()
s2.study()
```





# React中定义组件

React的组件分为函数组件和类组件

组件名称首必须字母大写、组件的返回值只能有一个根元素

## 函数组件

函数组件，就是通过函数编写的形式去实现一个`React`组件，是`React`中定义组件最简单的方式，函数第一个参数为`props`用于接收父组件传递过来的参数

```html
<body>
<div id="root"></div>
<script src="../../javascript/react.development.js"></script>
<script src="../../javascript/react-dom.development.js"></script>
<script src="../../javascript/babel.min.js"></script>

<script type="text/babel">
    // 创建一个函数组件
    function App() {
        console.log(this) // 此处的this是undefined因为经过babel编译后开启了严格模式
        return (<div className="app">我是一个函数组件（适合简单组件的定义）</div>)
    }

    // 渲染组件到容器中
    ReactDOM.render(<App></App>, document.querySelector("#root"))
    /*
    * 执行了ReactDOM.render()之后发生了什么？
    *  1. React解析组件标签，找到了App组件
    *  2. 发现组件是使用函数定义的，随后调用该函数将返回的虚拟DOM转换为真实DOM，然后呈现在页面中
    * */
</script>
</body>
```

## 类组件

类组件，也就是通过使用`ES6`类的编写形式去编写组件，该类必须继承`React.Component`

如果想要访问父组件传递过来的参数，可通过`this.props`的方式去访问

在组件中必须实现`render`方法，在`return`中返回`React`对象

```JavaScript
    // 创建一个类组件
    class App extends React.Component {
        render() {
            //render()是放在哪里的？App的原型对象上供实例使用
            //render()中的this是谁？App的实例对象
            console.log("render中的this", this)
            return (
                <div className="app">
                    我是类组件(适合复杂组件的定义)
                </div>
            );
        }
    }

    // 渲染组件到容器中
    ReactDOM.render(<App></App>, document.querySelector("#root"))
    /*
    * 执行了ReactDOM.render()之后发生了什么？
    *  1. React解析组件标签，找到了App组件
    *  2. 发现组件是使用类定义的，随后创建一个该类的实例，并且通过该实例调用原型上的render()方法
    *  3. 将render()返回的虚拟DOM转为真实DOM随后呈现在页面中
    * */
```





# 组件实例的三大属性

![](E:\01.前端\15.React\images\组件实例的三大属性.png)

## state

state是组件对象最重要的属性，值是对象(可以包含多个键值对的集合)

组件被称为状态机，通过更新组件的state来更新页面的显示(重新渲染组件)

> 组件中render函数中的this指向的是组件实例对象
>
> 组件的自定义方法中this的指向为undefined，如何解决：1.通过bind函数绑定this、2.类的实例方法、3.箭头函数
>
> state状态不能直接修改需要调用setState()

```JavaScript
    /*
    *state是组件对象最重要的属性，值是对象(可以包含多个键值对的集合)
    *组件被称为状态机，通过更新组件的state来更新页面的显示(重新渲染组件)
    * */
    class App extends React.Component {
        constructor(props, context) {
            super(props, context)
            this.state = {flag: true}
            this.changeFlag = this.changeFlag.bind(this)
        }

        render() {
            const {flag} = this.state
            console.log("render函数被触发了")
            return (
                <div className="app">
                    <h1>今天天气很{flag ? "炎热" : "凉爽"}</h1>
                    <button onClick={this.changeFlag}>切换</button>
                </div>
            );
        }

        changeFlag() {
            // this.state.flag=!this.state.flag 注意state状态不可以直接更改，需要借助setState函数来修改

            // setState函数做了两件事件：1.修改数据、2.触发render()函数更新视图
            this.setState({flag: !this.state.flag}, () => {
                 // 能拿到更新后的最新值
                console.log("修改了flag", this.state.flag)
            })
            // 拿不到更新后的最新值
            console.log(this.state.flag) 
        }

    }

    ReactDOM.render(<App></App>, document.querySelector("#root"))
```



**state的简写**

```javascript
class App extends React.Component {
    // 初始化状态
    state = {flag: true}
    render() {
        const {flag} = this.state
        return (
            <div className="app">
                <h1>今天天气很{flag ? "炎热" : "凉爽"}</h1>
                <button onClick={this.changeFlag}>切换</button>
            </div>
        );
    }
    // 自定义方法----赋值语句+箭头函数
    changeFlag = () => {
        this.setState({flag: !this.state.flag})
    }
}
ReactDOM.render(<App></App>, document.querySelector("#root"))
```

## props

state 和 props 主要的区别在于 props 是不可变的，而 state 可以根据与用户交互来改变。这就是为什么有些组件需要定义state来更新和修改数据，而子组件只能通过props来传递数据

> 理解props：每个组件对象都会有props属性，组件标签的所有属性都保存在props中
>
> props的作用：通过标签属性从组件外向组件内传递变化的数据

> 组件的props是只读属性，不能直接对prop的值进行修改

```JavaScript
/*
* state 和 props 主要的区别在于 props 是不可变的，而 state 可以根据与用户交互来改变。这就是为什么有些组件需要定义state来更新和修改数据，而子组件只 能通过props来传递数据
* 组件的props是只读属性，不能直接对prop的值进行修改
* */
class Person extends React.Component {
    constructor(props, context) {
        super(props, context)
    }

    render() {
        // 从组件实例的props中解构赋值拿到外部传递过来的自定义属性prop
        const {name, age, sex} = this.props
        return (
            <div className="app">
                <ul>
                    <li>姓名是：{name}</li>
                    <li>年龄是：{age}</li>
                    <li>性别是：{sex}</li>
                </ul>
            </div>
        );
    }
}

// 给Person组件传递三个props分别是name、age、sex
ReactDOM.render(<Person name="陶品奇" age="22" sex="男"></Person>, document.querySelector("#root"))
```

**批量的传递props**

```JavaScript
class Person extends React.Component {
    constructor(props, context) {
        super(props, context)
    }
    render() {
        const {name, age, sex} = this.props
        return (
            <div className="app">
                <ul>
                    <li>姓名是：{name}</li>
                    <li>年龄是：{age}</li>
                    <li>性别是：{sex}</li>
                </ul>
            </div>
        );
    }
}

// ReactDOM.render(<Person name="陶品奇" age="22" sex="男"></Person>, document.querySelector("#root")) 一个一个的传递props
const data = {name: "马云", age: 50, sex: "男"}
ReactDOM.render(<Person {...data}></Person>, document.querySelector("#root")) // 批量的传递props属性
```

**props的类型校验**

```JavaScript
<body>
<div id="root"></div>
<script src="../../javascript/react.development.js"></script>
<script src="../../javascript/react-dom.development.js"></script>
<!--引入prop-types用于对props的类型进行限制-->
<script src="../../javascript/prop-types.js"></script>
<script src="../../javascript/babel.min.js"></script>

<script type="text/babel">
    class Person extends React.Component {
        constructor(props, context) {
            super(props, context)
        }

        render() {
            const {name, age, sex} = this.props
            return (
                <div className="app">
                    <ul>
                        <li>姓名是：{name}</li>
                        <li>年龄是：{age}</li>
                        <li>性别是：{sex}</li>
                    </ul>
                </div>
            );
        }
    }
    // 对props的值进行类型和必填的限制
    Person.propTypes = {
        name: PropTypes.string.isRequired, // 指定name为string类型且是必填项
        age: PropTypes.number.isRequired,// 指定age为number类型且是必填项
        getAge:PropTypes.func

    }
    // 对props的值进行默认值的指定
    Person.defaultProps = {
        sex: "男" // 指定sex的默认值为男
    }

    ReactDOM.render(<Person name="陶品奇" age={22} sex="男"></Person>, document.querySelector("#root"))

</script>
</body>
```

**props的简写**

```JavaScript
class Person extends React.Component {
    // 静态属性(类属性)
    static propTypes = {
        name: PropTypes.string.isRequired,
        age: PropTypes.number.isRequired,
        getAge: PropTypes.func
    }
    // 静态属性(类属性)
    static defaultProps = {
        sex: "男"
    }

    constructor(props, context) {
        super(props, context)
    }

    render() {
        const {name, age, sex} = this.props
        return (
            <div className="app">
                <ul>
                    <li>姓名是：{name}</li>
                    <li>年龄是：{age}</li>
                    <li>性别是：{sex}</li>
                </ul>
            </div>
        );
    }
}

ReactDOM.render(<Person name="陶品奇" age={22} sex="男"></Person>, document.querySelector("#root"))
```

**构造器中的props**

> 在React组件挂载之前会调用组件的构造函数，在为React.Component子类实现构造函数时，应该在其它语句之前调用super(props)
>
> 否则this.props在构造函数中可能会出现未定义的bug 
>
> 构造器是否接收props是否传递给super()，取决于：是否希望在构造器中通过this访问props

```JavaScript
class Person extends React.Component {
    static propTypes = {
        name: PropTypes.string.isRequired,
        age: PropTypes.number.isRequired,
        getAge: PropTypes.func
    }
    static defaultProps = {
        sex: "男"
    }
    /*
    * 在React中构造函数仅仅用于以下两种情况： 1.通过给this.state赋值对象来初始化内部的state 2.为事件处理函数绑定this的指向
    * */
    constructor(props, context) {
        /* 
        *在React组件挂载之前会调用组件的构造函数，在为React.Component子类实现构造函数时，应该在其它语句之前调用super(props)
        *否则this.props在构造函数中可能会出现未定义的bug
        */
        super(props, context)
        //构造器是否接收props是否传递给super()，取决于：是否希望在构造器中通过this访问props
        console.log(this.props)
        console.log(props)
    }

    render() {
        const {name, age, sex} = this.props
        return (
            <div className="app">
                <ul>
                    <li>姓名是：{name}</li>
                    <li>年龄是：{age}</li>
                    <li>性别是：{sex}</li>
                </ul>
            </div>
        );
    }
}

ReactDOM.render(<Person name="陶品奇" age={22} sex="男"></Person>, document.querySelector("#root"))
```

**函数组件的props**

```JavaScript
    /*
    * 函数组件通过函数形参接收props
    * */
    function App(props) {
        return (
            <div className="app">
                <h1>姓名：{props.name}</h1>
                <h1>年龄：{props.age}</h1>
                <h1>性别：{props.sex}</h1>
            </div>
        )
    }

    App.propTypes = {
        name: PropTypes.string.isRequired,
        age: PropTypes.number.isRequired,
        getAge: PropTypes.func
    }
    App.defaultProps = {
        sex: "男"
    }

    ReactDOM.render(<App name="陶品奇" age={22} sex="男"></App>, document.querySelector("#root"))
```



## refs

React 中的 Refs提供了一种方式，允许我们访问 DOM节点或在 render方法中创建的 React元素

本质为ReactDOM.render()返回的组件实例，如果是渲染组件则返回的是组件实例，如果渲染dom则返回的是具体的dom节点

**字符串形式的ref**

```JavaScript
class App extends React.Component {
    constructor(props, context) {
        super(props, context)
    }

    render() {
        return (
            <div className="app">
                /*字符串形式的ref*/
                <input type="text" placeholder="点击按钮提示数据" ref="input1" onClick={this.showData}/>
                <input type="text" placeholder="失去焦点提示数据" ref="input2" onBlur={this.showData}/>
                <button ref="button" onClick={this.btnClick}>点击</button>
            </div>
        );
    }

    showData = () => {
        console.log(this.refs.input1.value)
        console.log(this.refs.input2.value)
    }
    btnClick=()=>{
        this.refs.button.style.color="red"
    }
}

ReactDOM.render(<App></App>, document.querySelector("#root"))
```

**回调函数形式的ref**

```JavaScript
class App extends React.Component {
    constructor(props, context) {
        super(props, context)
    }

    render() {
        return (
            <div className="app">
                /*回调函数形式的ref*/
                <input type="text" placeholder="点击按钮提示数据" ref={c => this.input1 = c} onClick={this.showData}/>
                <input type="text" placeholder="失去焦点提示数据" ref={c => this.input2 = c} onBlur={this.showData}/>
                <button ref={c => this.button = c} onClick={this.btnClick}>点击</button>
            </div>
        );
    }

    showData = () => {
        console.log(this.input1.value)
        console.log(this.input2.value)
    }
    btnClick = () => {
        this.button.style.color = "red"
    }
}

ReactDOM.render(<App></App>, document.querySelector("#root"))
```

```JavaScript
class App extends React.Component {
    constructor(props, context) {
        super(props, context)
        this.state = {count: 1}
    }

    render() {
        return (
            <div className="app">
                /*回调函数形式的ref在更新的过程中会被执行两次，第一次传入参数null，第二次传入的参数为DOM元素*/
                <input type="text" placeholder="点击按钮提示数据" ref={currentNode => {
                    console.log("回调函数形式ref的回调执行了", currentNode)
                    this.input = currentNode
                }}/>
                <button onClick={this.btnClick}>点击</button>
                <button onClick={this.addCount}>点击{this.state.count}</button>
            </div>
        );
    }

    btnClick = () => {
        console.log(this.input.value)
    }
    addCount = () => {
        this.setState((state) => {
            return {
                count: state.count += 1
            }
        })
    }
}

ReactDOM.render(<App></App>, document.querySelector("#root"))
```

**createRef**

参数 

`createRef` 不接受任何参数。

返回值 

`createRef` 返回一个对象，该对象只有一个属性：

`current`：初始值为 `null`，你可以稍后设置为其他内容。如果你把 ref 对象作为 JSX 节点的 `ref` 属性传递给 React，React 将设置其 `current` 属性。

> createRef创建一个ref对象，该对象可以包含任意值

```JavaScript
class App extends React.Component {
    constructor(props, context) {
        super(props, context)
        /*React.createRef()创建出来的ref*/
        this.input = React.createRef()
    }

    render() {
        return (
            <div className="app">
                <input type="text" placeholder="点击按钮提示数据" ref={this.input}/>
                <button onClick={this.btnClick}>点击</button>
            </div>
        );
    }

    btnClick = () => {
        console.log(this.current.input.value)
    }

}

ReactDOM.render(<App></App>, document.querySelector("#root"))
```





# 类中方法的this指向

> 调用方法都是有()的，所以这里只是把onClick指向堆中的changeFlag,就没有通过实例调用————所以this会指向window
>
> 类中的方法默认开启了严格模式+使用babel编译后也会是严格模式（严格模式下this的指向不是window，而是undefined)

```JavaScript
 /*
 class App {
        constructor(name, age) {
            this.name = name
            this.age = age
        }
        study() {
            /!*
            * study方法放在了哪里？类的原型对象上供实例使用
            * 通过App实例调用study方法的时候，this指向App实例
            * *!/
            console.log(this)
        }
    }

    const p = new App("马玉", 55)
    console.log(p)
    p.study() // App的实例
    const x = p.study
    x() // undefined (类中的方法默认开启了严格模式)
*/


 class App extends React.Component {
     constructor(props) {
         super(props)
         this.state = {flag: true}
     }
     render() {
         const {flag} = this.state
         return (
             <div className="app">
                 <h1>今天天气很{flag ? "炎热" : "凉爽"}</h1>
                 <button onClick={this.changeFlag}>切换</button>
             </div>
         );
     }
     changeFlag() {
         /*
         * changeFlag放在哪里？App的原型对象上，供实例使用
         * 由于changeFlag是作为onClick的回调，不是通过实例调用的，是直接调用
         * 类中的方法默认开启了局部的严格模式，所以changeFlag方法中的this指向的是undefined
         * */
         console.log(this)
     }
 }
 ReactDOM.render(<App></App>, document.querySelector("#root"))

 const a=new App()
 a.changeFlag() // 实例调用this指向App实例对象
```





# 事件绑定

> React为事件处理函数绑定事件的时候this默认指向的不是组件实例而是undefined
>
> 因此在为React绑定事件的时候需要解决this的指向问题

## bind()

```JavaScript
class App extends React.Component {
    constructor(props, context) {
        super(props, context)
        this.state = {count: 1}
        // 使用bind绑定this指向并且返回一个修改this之后的函数
        this.addCount = this.addCount.bind(this)
    }

    render() {
        return (
            <div className="app">
                <button onClick={this.addCount}>点击{this.state.count}</button>
            </div>
        );
    }

    addCount() {
        this.setState({
            count: this.state.count += 1
        })
    }
}
ReactDOM.render(<App></App>, document.querySelector("#root"))
```

## 类实例方法

```JavaScript
class App extends React.Component {
    constructor(props, context) {
        super(props, context)
        this.state = {count: 1}
    }

    render() {
        return (
            <div className="app">
                <button onClick={this.addCount}>点击{this.state.count}</button>
            </div>
        );
    }
    // 箭头函数+赋值语句的形式定义事件处理方法
    addCount = () => {
        this.setState({
            count: this.state.count += 1
        })
    }
}

ReactDOM.render(<App></App>, document.querySelector("#root"))
```

## 箭头函数

```JavaScript
class App extends React.Component {
    constructor(props, context) {
        super(props, context)
        this.state = {count: 1}
    }

    render() {
        return (
            <div className="app">
                // 使用箭头函数函数的形式返回一个事件处理函数(高阶函数)
                <button onClick={() => {this.addCount()}}>点击{this.state.count}</button>
            </div>
        );
    }

    // 箭头函数+赋值语句的形式定义事件处理方法
    addCount() {
        this.setState({
            count: this.state.count += 1
        })
    }
}

ReactDOM.render(<App></App>, document.querySelector("#root"))
```



# setState函数

> setState这个方法在调用的时候是同步的，但是引起React的状态更新是异步的 【React状态更新是异步的】
>
> setState第一个参数可以是一个对象，或者是一个函数，而第二个参数是一个回调函数

<img src="E:\01.前端\15.React\images\setState函数.png"  />

**setState第二个参数的作用**

因为setState是一个异步的过程，所以说执行完setState之后不能立刻更改state里面的值。如果需要对state数据更改监听，setState提供第二个参数，就是用来监听state里面数据的更改，当数据更改完成，调用回调函数，用于可以实时的获取到更新之后的数据

```JavaScript
    class App extends React.Component {
        constructor(props, context) {
            super(props, context)
            this.state = {count: 1}
        }
        render() {
            return (
                <div className="app">
                    <button onClick={() => {
                        this.addCount()
                    }}>点击{this.state.count}</button>
                </div>
            );
        }

        addCount() {
            this.setState({count: this.state.count += 1}, () => {
                console.log(this.state.count) // setState函数的第二个参数里面能拿到更新后的值
            })
        }
    }
    ReactDOM.render(<App></App>, document.querySelector("#root"))
```

**为什么setState设计为异步的**

> setState设计为异步，可以显著的提升性能
>
> 如果每次调用setState都进行一次更新，那么意味着render函数会被频繁调用，界面重新渲染，这样效率是很低的，最好的办法应该是获取到多个更新，之后进行批量更新



**调用setState的时候，发生了什么事**

> 1.将传递给 setState 的对象合并到组件的当前状态，触发所谓的调和过程（Reconciliation）
>
> 2.然后生成新的DOM树并和旧的DOM树使用Diff算法对比
>
> 3.根据对比差异对界面进行最小化重渲染

```JavaScript
class App extends React.Component {
    constructor(props, context) {
        super(props, context)
        this.state = {count: 1}
    }

    render() {
        return (
            <div className="app">
                <button onClick={() => {
                    this.addCount()
                }}>点击{this.state.count}</button>
            </div>
        );
    }

    addCount() {
        /*
        setState函数的第一个参数为对象
        this.setState({count: this.state.count += 1}, () => {
            console.log(this.state.count) // setState函数的第二个参数里面能拿到更新后的值
        })
        */

        // setState函数的第一个参数为函数
        // 当调用了setState函数会做两件事情：1.更新数据、2.触发render()重新渲染视图
        this.setState(() => {
            return {
                count: this.state.count += 1
            }
        }, () => {
            console.log(this.state.count)
        })
    }
}

ReactDOM.render(<App></App>, document.querySelector("#root"))
```

# 收集表单元素

## 受控组件

受控组件通过props获取其当前值，并通过回调函数(比如onChange)通知变化

> 表单状态发生变化时，都会通知React，将状态交给React进行处理，比如可以使用useState存储
>
> 受控组件中，组件渲染出的状态与它的value或checked属性相对应
>
> 受控组件会更新state的流程

```JavaScript
    /*
    * 受控组件通过props获取其当前值，并通过回调函数(比如onChange)通知变化
    * 表单状态发生变化时，都会通知React，将状态交给React进行处理，比如可以使用useState存储
    * 受控组件中，组件渲染出的状态与它的value或checked属性相对应
    * 受控组件会更新state的流程
    * */
    class App extends React.Component {
        constructor(props, context) {
            super(props, context)
            // 初始化表单数据
            this.state = {userName: "", password: ""}
        }

        render() {
            const {userName, password} = this.state
            return (
                <div className="app">
                    <form onSubmit={this.handleSubmit}>
                        <input type="text" value={userName} onChange={this.changeUserName}/>
                        <input type="text" value={password} onChange={this.changePassword}/>
                        <button>登录</button>
                    </form>
                </div>
            );
        }

        changeUserName = (event) => {
            // 保存用户名到状态中
            const value = event.target.value
            this.setState((state) => {
                return {
                    userName: value
                }
            })
        }
        changePassword = () => {
            // 保存密码到状态中
            const value = event.target.value
            this.setState((state) => {
                return {
                    password: value
                }
            })
        }

        handleSubmit=(event)=>{
            // 阻止表单的默认提交行为
            event.preventDefault()
            // 解构赋值拿到数据
            const {userName, password} = this.state
            alert(`用户名是${userName},密码是${password}`)

        }

    }
    ReactDOM.render(<App></App>, document.querySelector("#root"))
```

## 非受控组件

非受控组件将数据存储在`DOM`中，而不是组件内，这比较类似于传统的`HTML`表单元素

> 非受控组件的值不受组件自身的`state`和`props`控制
>
> 非受控组件使用`ref`从`DOM`中获取元素数据

```JavaScript
    class App extends React.Component {
        constructor(props, context) {
            super(props, context)
        }

        render() {
            return (
                <div className="app">
                    <form onSubmit={this.handleSubmit}>
                        // 给input元素绑定回调函数形式的ref
                        <input type="text" ref={c => this.name = c}/>
                        // 给input元素绑定回调函数形式的ref
                        <input type="text" ref={c => this.password = c}/>
                        <button>登录</button>
                    </form>
                </div>
            );
        }

        handleSubmit = (event) => {
            // 阻止表单的默认提交行为
            event.preventDefault()
            // 解构赋值拿到DOM节点
            const {name, password} = this
            alert(`用户名是${name.value},密码是${password.value}`)
        }
    }
    ReactDOM.render(<App></App>, document.querySelector("#root"))
```



# 高阶函数与函数柯里化

**高阶函数**

> 高阶函数：函数作为参数、函数作为返回值

```JavaScript
/*
* 高阶函数：函数作为参数、函数作为返回值
* */
function fun(callback) {
    if (typeof callback !== "function") return
    callback()
}

// 函数可以作为参数此时fun函数是一个高阶函数
fun(() => {
    console.log("函数执行了")
})

function sum(...args) {
    // 函数可以作为返回值此时sum函数是一个高阶函数
    return () => {
        return args.reduce((sum, item) => sum + item, 0)
    }
}

const res = sum(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
console.log(res())
```

**函数柯里化**

```JavaScript
/*
* 函数的柯里化
* */
function sum(a) {
    return (b) => {
        return (c) => {
            return a + b + c
        }
    }
}

const result = sum(10)(20)(30)
console.log(result)
```

```JavaScript
/*
* 高阶函数：如果一个函数符合下面2个规范中的任何一个，那该函数就是高阶函数 (函数作为参数，函数内部返回函数)
*   若A函数，接收的参数是一个函数，那么A函数可以称之为高阶函数
*   若A函数，调用的返回值还是一个函数，那么A函数可以称之为高阶函数
*
* 函数柯里化：通过函数调用继续返回函数的方式，实现多次接收参数最后统一处理的函数编码形式
* */
class App extends React.Component {
    constructor(props, context) {
        super(props, context)
        this.state = {userName: "", password: ""}
    }

    render() {
        const {userName, password} = this.state
        return (
            <div className="app">
                <form>
                    {/*将saveFormData函数的返回值作为onChange的回调*/}
                    <input type="text" value={userName} onChange={this.saveFormData("userName")}/>
                    {/*将saveFormData函数的返回值作为onChange的回调*/}
                    <input type="text" value={password} onChange={this.saveFormData("password")}/>
                </form>
            </div>
        );
    }


    // 高阶函数
    saveFormData = (dataType) => {
        //saveFormData函数返回的是一个函数，将这个返回的函数作为事件的回调去使用
        return (event) => {
            this.setState({[dataType]: event.target.value})
        }
    }

}

ReactDOM.render(<App></App>, document.querySelector("#root"))
```

```javascript
class App extends React.Component {
    constructor(props, context) {
        super(props, context)
        this.state = {userName: "", password: ""}
    }

    render() {
        const {userName, password} = this.state
        return (
            <div className="app">
                <form>
                    <input type="text" value={userName} onChange={event => this.saveFormData("userName", event.target.value)}/>
                    <input type="text" value={password} onChange={event => this.saveFormData("password", event.target.value)}/>
                </form>
            </div>
        );
    }
    saveFormData = (dataType, value) => {
        this.setState({[dataType]: value})
    }
}
ReactDOM.render(<App></App>, document.querySelector("#root"))
```



# 组件生命周期

组件从创建到死亡会经历一些特定的阶段

React组件中包含了一系列钩子函数(生命周期回调函数)会在特定的时刻调用

在定义组件的时候，会在特定的生命周期函数中做特定的工作

组件从被创建到被销毁的过程称为组件的生命周期

组件的生命周期可分成三个状态：

> Mounting（挂载时）
>
> Updating（更新时）
>
> Unmounting（卸载时）

## 生命周期流程图(旧)

**1.初始化阶段:** 由ReactDOM.render()触发---初次渲染

> constructor()
>
> componentWillMount()
>
> render()
>
> componentDidMount()

**2.** **更新阶段:** 由组件内部this.setSate()或父组件重新render触发

> shouldComponentUpdate()
>
> componentWillUpdate()
>
> render()
>
> componentDidUpdate()

**3.** **卸载组件:** 由ReactDOM.unmountComponentAtNode()触发

> componentWillUnmount()

![](E:\01.前端\15.React\images\生命周期流程图(旧).png)

```JavaScript
class App extends React.Component {
    constructor(props, context) {
        console.log("constructor()方法在组件初始化的时候调用")
        super(props, context)
        this.state = {count: 1}
    }

    componentWillMount() {
        console.log("componentWillMount()方法在组件挂载之前(插入DOM树)之前调用")
    }

    componentWillReceiveProps(props){
    console.log("componentWillReceiveProps()在初始化render不会执行，在Component接受到新状态(Props)触发，用于父组件状态更新时子组件的重新渲染")
    }
    
    shouldComponentUpdate(nextProps, nextState) {
    //  组件是否需要更新，需要返回一个布尔值，返回true则更新，返回flase不更新
   //  shouldComponentUpdate()的返回值，判断React组件的输出是否受当前state或props更改的影响。默认行为是state每次发生变化组件都会重新渲染
        console.log("shouldComponentUpdate()方法返回的布尔值确定是否更新",nextProps, nextState)
        return true
    }

    componentWillUpdate(){
        console.log("componentWillUpdate()方法在组件更新之前调用")
    }

    componentDidUpdate(){
        console.log("componentDidUpdatee()方法在组件更新后立即调用")
    }

    render() {
        console.log("render()方法在组件初始化渲染和状态更新后立即调用")
        return (
            <div className="app">
                <h1>当前的count值为{this.state.count}</h1>
                <button onClick={this.addCount}>+1</button>
                <button onClick={this.death}>销毁组件</button>
                <button onClick={this.force}>强制更新组件</button>
            </div>
        );
    }

    componentDidMount() {
        console.log("componentDidMount()方法在组件挂载后(插入DOM树中)立即调用")
    }
    
    componentWillUnmount() {
        console.log("componentWillUnmount()方法在组件卸载及销毁之前直接调用")
    }

    death = () => {
        // 卸载组件(unmountComponentAtNode用于从DOM中移除一个已挂载的React组件)
        ReactDOM.unmountComponentAtNode(document.querySelector("#root"));
    }

    addCount = () => {
        this.setState((state) => {
            return {
                count: state.count += 1
            }
        })
    }

    force=()=>{
        this.forceUpdate(()=>{
            console.log("组件被强制更新了")
        })
    }
}

ReactDOM.render(<App></App>, document.querySelector("#root"))
```



## 生命周期流程图(新)

**1.** **初始化阶段:** 由ReactDOM.render()触发---初次渲染

> constructor()
>
> getDerivedStateFromProps()
>
> render()
>
> componentDidMount()

   **2.** **更新阶段:** 由组件内部this.setSate()或父组件重新render触发

> getDerivedStateFromProps()
>
> shouldComponentUpdate()
>
> render()
>
> getSnapshotBeforeUpdate()
>
> componentDidUpdate()

​    **3.** **卸载组件:** 由ReactDOM.unmountComponentAtNode()触发

> componentWillUnmount()

![](E:\01.前端\15.React\images\生命周期函数(新).png)

```JavaScript
class App extends React.Component {
    constructor(props, context) {
        console.log("constructor()方法在组件初始化的时候调用")
        super(props, context)
        this.state = {count: 1}
    }

    static getDerivedStateFromProps(props, state) {
        console.log("getDerivedStateFromProps()得到派生的状态",props,state)
        return state
    }

    shouldComponentUpdate(nextProps, nextState) {
     //组件是否需要更新，需要返回一个布尔值，返回true则更新，返回flase不更新
     //shouldComponentUpdate()的返回值，判断React组件的输出是否受当前state或props更改的影响。默认行为是state每次发生变化组件都会重新渲染
        console.log("shouldComponentUpdate()方法返回的布尔值确定是否更新", nextProps, nextState)
        return true
    }
    
    render() {
        console.log("render()方法在组件初始化渲染和状态更新后立即调用")
        return (
            <div className="app">
                <h1>当前的count值为{this.state.count}</h1>
                <button onClick={this.addCount}>+1</button>
                <button onClick={this.death}>销毁组件</button>
                <button onClick={this.force}>强制更新组件</button>
            </div>
        );
    }

    getSnapshotBeforeUpdate(){
        console.log("getSnapshotBeforeUpdate()")
        return "尚硅谷"
    }

    componentDidUpdate(preProps,preState,value) {
        console.log("componentDidUpdatee()方法在组件更新后立即调用",preProps,preState,value)
    }
    
    componentDidMount() {
        console.log("componentDidMount()方法在组件挂载后(插入DOM树中)立即调用")
    }

    componentWillUnmount() {
        console.log("componentWillUnmount()方法在组件卸载及销毁之前直接调用")
    }

    death = () => {
        // 卸载组件(unmountComponentAtNode用于从DOM中移除一个已挂载的React组件)
        ReactDOM.unmountComponentAtNode(document.querySelector("#root"));
    }

    addCount = () => {
        this.setState((state) => {
            return {
                count: state.count += 1
            }
        })
    }

    force = () => {
        this.forceUpdate(() => {
            console.log("组件被强制更新了")
        })
    }
}

ReactDOM.render(<App name="陶品奇"></App>, document.querySelector("#root"))
```



# React脚手架

> 1. xxx脚手架: 用来帮助程序员快速创建一个基于xxx库的模板项目
> 2. 包含了所有需要的配置（语法检查、jsx编译、devServer…）
> 3. 下载好了所有相关的依赖
> 4. 可以直接运行一个简单效果
> 5. react提供了一个用于创建react项目的脚手架库: create-react-app
> 6. 项目的整体技术架构为: react + webpack + es6 + eslint
> 7. 使用脚手架开发的项目的特点: 模块化, 组件化, 工程化

## 创建项目并启动

create-react-app 是官方支持的创建单页 React 应用的方法

```
npx create-react-app my-app

cd my-app

npm start
```

```
npm i -g create-react-app

create-react-app my-app

cd my-app

npm start
```

## 脚手架项目结构

> public ---- 静态资源文件夹
>
> ​            favicon.icon ------ 网站页签图标
>
> ​            **index.html --------** **主页面**
>
> ​            logo192.png ------- logo图
>
> ​            logo512.png ------- logo图
>
> ​            manifest.json ----- 应用加壳的配置文件
>
> ​            robots.txt -------- 爬虫协议文件
>
> src ---- 源码文件夹
>
> ​            App.css -------- App组件的样式
>
> ​            **App.js --------- App****组件**
>
> ​            App.test.js ---- 用于给App做测试
>
> ​            index.css ------ 样式
>
> ​            **index.js -------** **入口文件**
>
> ​            logo.svg ------- logo图
>
> ​            reportWebVitals.js
>
> ​                    --- 页面性能分析文件(需要web-vitals库的支持)
>
> ​            setupTests.js
>
> ​                    ---- 组件单元测试的文件(需要jest-dom库的支持)



## 一个简单的组件

```jsx
// 引入React核心库
import React from "react";
// 默认导出App组件并且基继承React.Component
export default class App extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return(
            <div className="app"></div>
        )
    }
}
```

```JavaScript
// 引入React核心库
import React from "react";
// 引入ReactDom库
import ReactDom from "react-dom"
// 引入App根组件
import App from "./App";
// 使用render函数渲染App组件并且指定挂载点
ReactDom.render(<App></App>,document.querySelector("#root"))
```

# 组件化编码流程

1.拆分组件: 拆分界面,抽取组件

2.实现静态组件: 使用组件实现静态页面效果

3.实现动态组件：动态显示初始化数据、数据类型、数据名称、保存在哪个组件?

4.交互(从绑定事件监听开始)



# React中的ajax

## 下载axios

```
npm i axios
```

## 配置拦截器

```JavaScript
import axios from "axios";

const request = axios.create({
    baseURL: "http://ajax-api.itheima.net/api",
    timeout: 2000,
})
// 添加请求拦截器
request.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
request.interceptors.response.use(function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么
    return response;
}, function (error) {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    return Promise.reject(error);
});
export default request
```

## 封装接口

```JavaScript
import request from "../utils/request";
/*登录的接口*/
export const loginAPI=(data)=>{
    return request({
        method:"POST",
        url:"/login",
        data
    })
}
```

## 发送请求

```jsx
import React from "react";
import {loginAPI} from "./API/loginAPI";

export default class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {username: "admin", password: "123456"}
    }

    handleSubmit = async (event) => {
        event.preventDefault()
        try {
            const {data: {message}} = await loginAPI(this.state)
            alert(message)
        } catch (e) {
            alert(e.message)
        }
    }

    saveFormData = (dataType) => {
        return (event) => {
            this.setState({
                [dataType]: event.target.value
            })
        }
    }

    render() {
        const {username, password} = this.state
        return (
            <div className="app">
                <form onSubmit={this.handleSubmit}>
                    <input type="text" value={username} onChange={this.saveFormData("username")}/>
                    <input type="password" value={password} onChange={this.saveFormData("password")}/>
                    <button>登录</button>
                </form>
            </div>
        )
    }
}
```

## 配置代码方式一

```
"proxy": "http://ajax-api.itheima.net"
```

## 配置代码方式二

```JavaScript
const proxy = require("http-proxy-middleware")
module.exports = function (app) {
    app.use(
        proxy("/api", { // 遇见api前缀的请求触发代理
            target: "http://ajax-api.itheima.net",  // 请求转发的地址
            changeOrigin: true, // 控制服务器收到的响应头Host字段的值
            pathRewrite: {"^/api": ""}  // 路径重写
        })
    )
}
```

# 消息订阅与发布

- 当我们想要父组件传值给子组件时，通常会使用props传值。
- 子组件传值给父组件时，通常会子组件中的事件触发一个回调函数（也是props），父组件中的对应函数再去修改值。
- 兄弟组件间传值，通常会将子组件A的值传回父组件，父组件再传给子组件B

以上三种情况都可以**使用消息订阅与发布机制**来解决。

- 当然父传子还是用props比较好。
- 子组件传值给父组件，在子组件中发布，在父组件中订阅，就可以拿到相应的值
- 兄弟组件间传值，在子组件A中发布，在子组件B中订阅

## 下载pubsub-js

```
npm install pubsub-js
```

## 发布消息

```jsx
class App extends React . Component  {
  componentDidMount() {
   // publish 发布消息 消息名为：publish_one 内容为：This is publish
      PubSub.publish("publish_one","This is publish")
  }
  render() {
    return (
      <div className = 'ArticleContainer'>
        <Data/>
      </div>
    )
  }
}
```

## 订阅消息

```jsx
lass Data extends React.Component{
  state={
    publishData:''
  }
  componentDidMount(){
   // 订阅消息 消息名：publish_one  第二个参数是一个函数
   // 此函数又有两个参数：消息名和消息数据
    PubSub.subscribe("publish_one",(msg,data)=>{
      this.setState({publishData:data})
    })
  }
  render(){
    return(
    <div>
      {this.state.publishData}
    </div>)
  }
}
```

## 定义token

```jsx
componentDidMount(){
    this.token = PubSub.subscribe('publish_one',(msg,data)=>{
       this.setState({publishData:data})
    })
}
```

## 取消订阅

```jsx
componentWillUnmount(){
     PubSub.unsubscribe(this.token)
 }
```

