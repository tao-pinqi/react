# 组件化编码流程

1.拆分组件: 拆分界面,抽取组件

2.实现静态组件: 使用组件实现静态页面效果

3.实现动态组件：动态显示初始化数据、数据类型、数据名称、保存在哪个组件?

4.交互(从绑定事件监听开始)



# App组件

```jsx
import React from "react";
import Header from "./component/Header/Header";
import Main from "./component/Main/Main";
import Footer from "./component/Footer/Footer";
import "./App.css"

export default class App extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <div className="app layui-anim layui-anim-up">
                <div className="layui-panel">
                    <Header></Header>
                    <Main></Main>
                    <Footer></Footer>
                </div>
            </div>
        )
    }
}
```

# Header组件

```jsx
import React from "react";

export default class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="Header">
                <input type="text" name="" placeholder="请输入您的任务名称按回车确认" className="layui-input"/>
            </div>
        );
    }
}
```

# Main组件

```jsx
import React from "react";
import Item from "../Item/Item";
import "./Main.css"
export default class Main extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="Main">
                <Item></Item>
                <Item></Item>
                <Item></Item>
                <Item></Item>
            </div>
        );
    }
}
```

# Item组件

```jsx
import React from "react";
import "./Item.css"

export default class Item extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="Item">
                <div className="state">
                    <input type="checkbox"/>
                    <span>xxxxxx</span>
                </div>
                <div className="delete">
                    <button type="button" className="layui-btn layui-btn-sm">删除</button>
                </div>
            </div>
        );
    }
}
```

# Footer组件

```jsx
import React from "react";
import "./Footer.css"
export default class Footer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="Footer">
                <div className="left">
                    <input type="checkbox"/>
                    <span>已经完成0/全部2</span>
                </div>
                <div className="right">
                    <button type="button" className="layui-btn layui-bg-red layui-btn-sm">删除已完成任务</button>
                </div>
            </div>
        );
    }
}
```

# 状态提升

将数据放在App组件中，然后使用props传递数据给子组件

```jsx
import React from "react";
import Header from "./component/Header/Header";
import Main from "./component/Main/Main";
import Footer from "./component/Footer/Footer";
import "./App.css"

export default class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            todos: [
                {id: "1", name: "吃饭", done: true},
                {id: "2", name: "睡觉", done: true},
                {id: "3", name: "打豆豆", done: true},
            ]
        }
    }

    render() {
        return (
            <div className="app layui-anim layui-anim-up">
                <div className="layui-panel">
                    <Header></Header>
                    <Main></Main>
                    <Footer></Footer>
                </div>
            </div>
        )
    }
}
```

# 渲染todo

```jsx
import React from "react";
import Item from "../Item/Item";
import "./Main.css"

export default class Main extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        // 从props接收父组件传递过来的todos
        const {todos} = this.props
        return (
            <div className="Main">
                {/*循环渲染每一个Item并且给每一个子组件Item传递id唯一标识 name名字和done状态*/}
                {/* {todos.map(todo => <Item key={todo.id} name={todo.name} done={todo.done}></Item>)} */}
                {/*批量传递props*/}
                {todos.map(todo => <Item key={todo.id} {...todo}></Item>)}
            </div>
        );
    }
}
```

```jsx
import React from "react";
import "./Item.css"

export default class Item extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        // 接收父组件传递过来的id、name、done
        const {id,name,done}=this.props
        return (
            <div className="Item">
                <div className="state">
                    <input type="checkbox" defaultChecked={done}/>
                    <span>{name}</span>
                </div>
                <div className="delete">
                    <button type="button" className="layui-btn layui-btn-sm">删除</button>
                </div>
            </div>
        );
    }
}
```

# 添加todo

```jsx
import React from "react";
import Header from "./component/Header/Header";
import Main from "./component/Main/Main";
import Footer from "./component/Footer/Footer";
import "./App.css"

export default class App extends React.Component {
    constructor(props) {
        super(props)
        // 状态提升：将数据放在App组件中，然后使用props传递数据给子组件
        this.state = {
            todos: [
                {id: "1", name: "吃饭", done: true},
                {id: "2", name: "睡觉", done: true},
                {id: "3", name: "打豆豆", done: false},
                {id: "4", name: "逛街", done: true},
            ]
        }
    }

    // 添加一个todo的方法
    addTodo = (todoObject) => {
        // 拿到原先的todos数组
        const {todos} = this.state
        // 从原先的数组中查找是否和新添加的todo重复
        const result = todos.find(todo => todo.name === todoObject.name)
        // 如果重复则不添加todo
        if (result) return alert("您添加的todo重复了")
        // 设置一个新的todos数组 
        this.setState({todos: [todoObject, ...todos]})
    }

    render() {
        const {todos} = this.state
        return (
            <div className="app layui-anim layui-anim-up">
                <div className="layui-panel">
                    {/*将addTodo方法传递给子组件用于子组件向父组件传递数据*/}
                    <Header addTodo={this.addTodo}></Header>
                    {/*将App组件中的todos列表使用props传递给Main组件*/}
                    <Main todos={todos}></Main>
                    <Footer></Footer>
                </div>
            </div>
        )
    }
}
```

```jsx
import React from "react";

export default class Header extends React.Component {
    constructor(props) {
        super(props);
        // todo的名字
        this.state = {name: ""}
    }


    // 双向数据绑定的方法
    handleChange = (event) => {
        this.setState({name: event.target.value})
    }

    // 处理键盘抬起的方法
    handleKeyUp = (event) => {
        if (event.keyCode !== 13) return
        if (this.state.name.trim()==="") return alert("您输入的todo不能为空")
        
        // 创建一个新的todo
        const newTodo = {name: this.state.name, id: Math.random(), done: true}
        // 调用父组件传递过来的props函数并且传值用于子组件将值传递给父组件
        this.props.addTodo(newTodo)
        // 清空数据
        this.setState({name:""})
    }

    render() {
        const {name} = this.state
        return (
            <div className="Header">
                <input type="text" name="" placeholder="请输入您的任务名称按回车确认" className="layui-input" value={name} onChange={this.handleChange} onKeyUp={this.handleKeyUp}/>
            </div>
        );
    }
}
```

# 给todo添加鼠标进入和离开事件

```jsx
import React from "react";
import "./Item.css"

export default class Item extends React.Component {
    constructor(props) {
        super(props);
        this.state = {flag: false}
    }

    // 修改flag的方法
    changeFlag = (flag) => {
        return () => {
            this.setState({flag})
        }
    }

    render() {
        // 接收父组件传递过来的id、name、done
        const {id, name, done} = this.props
        const {flag} = this.state
        return (
            <div className="Item" onMouseEnter={this.changeFlag(true)} onMouseLeave={this.changeFlag(false)} style={{backgroundColor: flag ? "#ddd" : ""}}>
                <div className="state">
                    <input type="checkbox" defaultChecked={done}/>
                    <span>{name}</span>
                </div>
                <div className="delete">
                    <button type="button" className="layui-btn layui-btn-sm" style={{display:flag?"block":"none"}}>删除</button>
                </div>
            </div>
        );
    }
}
```

# 修改一个todo状态

```jsx
import React from "react";
import Header from "./component/Header/Header";
import Main from "./component/Main/Main";
import Footer from "./component/Footer/Footer";
import "./App.css"

export default class App extends React.Component {
    constructor(props) {
        super(props)
        // 状态提升：将数据放在App组件中，然后使用props传递数据给子组件
        this.state = {
            todos: [
                {id: "1", name: "吃饭", done: true},
                {id: "2", name: "睡觉", done: true},
                {id: "3", name: "打豆豆", done: false},
                {id: "4", name: "逛街", done: true},
            ]
        }
    }

    // 添加一个todo的方法
    addTodo = (todoObject) => {
        // 拿到原先的todos数组
        const {todos} = this.state
        // 从原先的数组中查找是否和新添加的todo重复
        const result = todos.find(todo => todo.name === todoObject.name)
        // 如果重复则不添加todo
        if (result) return alert("您添加的todo重复了")
        // 设置一个新的todos数组
        this.setState({todos: [todoObject, ...todos]})
    }

    // 修改一个todo的方法
    changeTodo = ({done, id}) => {
        // 找到要修改的todo
        const currentTodo = this.state.todos.find(todo => todo.id === id)
        // 修改状态
        currentTodo.done = done
        // 更新数据
        this.setState({
            todos: this.state.todos
        })
    }

    render() {
        const {todos} = this.state
        return (
            <div className="app layui-anim layui-anim-up">
                <div className="layui-panel">
                    {/*将addTodo方法传递给子组件用于子组件向父组件传递数据*/}
                    <Header addTodo={this.addTodo}></Header>
                    {/*将App组件中的todos列表使用props传递给Main组件*/}
                    <Main todos={todos} changeTodo={this.changeTodo}></Main>
                    <Footer></Footer>
                </div>
            </div>
        )
    }
}
```

```jsx
import React from "react";
import "./Item.css"

export default class Item extends React.Component {
    constructor(props) {
        super(props);
        this.state = {flag: false}
    }

    // 修改flag的方法
    changeFlag = (flag) => {
        return () => {
            this.setState({flag})
        }
    }

    // 复选框发生改变的方法
    changeChecked = (done, id) => {
        this.props.changeTodo({done,id})
    }

    render() {
        // 接收父组件传递过来的id、name、done
        const {id, name, done} = this.props
        const {flag} = this.state
        return (
            <div className="Item" onMouseEnter={this.changeFlag(true)} onMouseLeave={this.changeFlag(false)} style={{backgroundColor: flag ? "#ddd" : ""}}>
                <div className="state">
                    <input type="checkbox" defaultChecked={done} onChange={event => this.changeChecked(event.target.checked, id)}/>
                    <span>{name}</span>
                </div>
                <div className="delete">
                    <button type="button" className="layui-btn layui-btn-sm" style={{display: flag ? "block" : "none"}}>删除</button>
                </div>
            </div>
        );
    }
}
```

# 删除一个todo

```jsx
import React from "react";
import Header from "./component/Header/Header";
import Main from "./component/Main/Main";
import Footer from "./component/Footer/Footer";
import "./App.css"

export default class App extends React.Component {
    constructor(props) {
        super(props)
        // 状态提升：将数据放在App组件中，然后使用props传递数据给子组件
        this.state = {
            todos: [
                {id: "1", name: "吃饭", done: true},
                {id: "2", name: "睡觉", done: true},
                {id: "3", name: "打豆豆", done: true},
                {id: "4", name: "逛街", done: true},
            ]
        }
    }

    // 添加一个todo的方法
    addTodo = (todoObject) => {
        // 拿到原先的todos数组
        const {todos} = this.state
        // 从原先的数组中查找是否和新添加的todo重复
        const result = todos.find(todo => todo.name === todoObject.name)
        // 如果重复则不添加todo
        if (result) return alert("您添加的todo重复了")
        // 设置一个新的todos数组
        this.setState({todos: [todoObject, ...todos]})
    }

    // 修改一个todo的方法
    changeTodo = ({done, id}) => {
        const currentTodo = this.state.todos.find(todo => todo.id === id)
        currentTodo.done = done
        this.setState({
            todos: this.state.todos
        })
    }

    // 删除一个todo的方法
    deleteTodo = (id) => {
        const {todos} = this.state
        const currentIndex = this.state.todos.findIndex(todo => todo.id === id)
        todos.splice(currentIndex, 1)
        this.setState({
            todos
        })

    }

    render() {
        const {todos} = this.state
        return (
            <div className="app layui-anim layui-anim-up">
                <div className="layui-panel">
                    {/*将addTodo方法传递给子组件用于子组件向父组件传递数据*/}
                    <Header addTodo={this.addTodo}></Header>
                    {/*将App组件中的todos列表使用props传递给Main组件*/}
                    <Main todos={todos} changeTodo={this.changeTodo} deleteTodo={this.deleteTodo}></Main>
                    <Footer></Footer>
                </div>
            </div>
        )
    }
}
```

```jsx
import React from "react";
import "./Item.css"

export default class Item extends React.Component {
    constructor(props) {
        super(props);
        this.state = {flag: false}
    }

    // 修改flag的方法
    changeFlag = (flag) => {
        return () => {
            this.setState({flag})
        }
    }

    // 复选框发生改变的方法
    changeChecked = (done, id) => {
        this.props.changeTodo({done, id})
    }

    // 点击删除按钮的方法
    clickHandle = (id) => {
        return () => {
            this.props.deleteTodo(id)
        }
    }

    render() {
        // 接收父组件传递过来的id、name、done
        const {id, name, done} = this.props
        const {flag} = this.state
        return (
            <div className="Item" onMouseEnter={this.changeFlag(true)} onMouseLeave={this.changeFlag(false)} style={{backgroundColor: flag ? "#ddd" : ""}}>
                <div className="state">
                    <input type="checkbox" defaultChecked={done} onChange={event => this.changeChecked(event.target.checked, id)}/>
                    <span>{name}</span>
                </div>
                <div className="delete">
                    <button type="button" className="layui-btn layui-btn-sm" style={{display: flag ? "block" : "none"}} onClick={this.clickHandle(id)}>删除</button>
                </div>
            </div>
        );
    }
}
```

# 底部交换

```jsx
import React from "react";
import Header from "./component/Header/Header";
import Main from "./component/Main/Main";
import Footer from "./component/Footer/Footer";
import "./App.css"

export default class App extends React.Component {
    constructor(props) {
        super(props)
        // 状态提升：将数据放在App组件中，然后使用props传递数据给子组件
        this.state = {
            todos: [
                {id: "1", name: "吃饭", done: true},
                {id: "2", name: "睡觉", done: true},
                {id: "3", name: "打豆豆", done: true},
                {id: "4", name: "逛街", done: true},
            ]
        }
    }

    // 添加一个todo的方法
    addTodo = (todoObject) => {
        // 拿到原先的todos数组
        const {todos} = this.state
        // 从原先的数组中查找是否和新添加的todo重复
        const result = todos.find(todo => todo.name === todoObject.name)
        // 如果重复则不添加todo
        if (result) return alert("您添加的todo重复了")
        // 设置一个新的todos数组
        this.setState({todos: [todoObject, ...todos]})
    }

    // 修改一个todo的方法
    changeTodo = ({done, id}) => {
        const currentTodo = this.state.todos.find(todo => todo.id === id)
        currentTodo.done = done
        this.setState({
            todos: this.state.todos
        })
    }

    // 删除一个todo的方法
    deleteTodo = (id) => {
        const {todos} = this.state
        const currentIndex = this.state.todos.findIndex(todo => todo.id === id)
        todos.splice(currentIndex, 1)
        this.setState({
            todos
        })
    }

    // 删除选中的todo
    deleteCheckedTodo = () => {
        const todos = this.state.todos.filter(todo => !todo.done)
        this.setState({todos})
    }

    //全选和取消全选的方法
    changeAllChecked = (done) => {
        const {todos} = this.state
        const newTodos = todos.map(todo => ({...todo, done}))
        this.setState({todos: newTodos})
    }

    render() {
        const {todos} = this.state
        return (
            <div className="app layui-anim layui-anim-up">
                <div className="layui-panel">
                    {/*将addTodo方法传递给子组件用于子组件向父组件传递数据*/}
                    <Header addTodo={this.addTodo}></Header>
                    {/*将App组件中的todos列表使用props传递给Main组件*/}
                    <Main todos={todos} changeTodo={this.changeTodo} deleteTodo={this.deleteTodo}></Main>
                    <Footer todos={todos} deleteCheckedTodo={this.deleteCheckedTodo} changeAllChecked={this.changeAllChecked}></Footer>
                </div>
            </div>
        )
    }
}
```

```jsx
import React from "react";
import "./Footer.css"

export default class Footer extends React.Component {
    constructor(props) {
        super(props);
    }

    // 点击删除已完成任务的方法
    deleteChecked = () => {
        this.props.deleteCheckedTodo()
    }


    // 全选按钮的回调函数
    changeAllChecked = (event) => {
        this.props.changeAllChecked(event.target.checked)

    }

    render() {
        const {todos} = this.props
        // 是否全选
        const isAllChecked = todos.every(todo => todo.done)
        return (
            <div className="Footer">
                <div className="left">
                    <input type="checkbox" checked={isAllChecked&&todos.length!==0} onChange={this.changeAllChecked}/>
                    <span>已经完成{todos.filter(todo => todo.done).length}/ 全部{todos.length}</span>
                </div>
                <div className="right">
                    <button type="button" className="layui-btn layui-bg-red layui-btn-sm" onClick={this.deleteChecked}>删除已完成任务</button>
                </div>
            </div>
        );
    }
}
```

# 收获

![](E:\01.前端\15.React\1.React基础\images\todoList案例收获.png)