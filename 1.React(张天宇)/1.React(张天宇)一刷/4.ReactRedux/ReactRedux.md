# React-Redux的工作流程图

![](E:\01.前端\15.React\React(张天宇)\1.React\images\react-redux模型图.png)

# React-Redux的基本使用

使用容器组件

```JavaScript
import React, {Component} from 'react';

// 引入redux(store)
import countStore from "./redux/countStore/countStore";

// 引入容器组件
import Count from "./containers/Count/Count";

class App extends Component {
    render() {
        return (
            <div className="app">
                {/*渲染Count的容器组件，并且给容器组件传递store*/}
                <Count store={countStore}></Count>
            </div>
        );
    }
    
    /*componentDidMount() {
       使用了ReactRedux就不需要检测redux的状态改变
           /!*监测redux中状态的改变，如果redux的状态发生了改变，则调用forceUpdate函数强制更新一次*!/
           countStore.subscribe(() => {
               this.forceUpdate()
           })
       }
       */
}

export default App;
```

容器组件

```jsx
// 引入Count的UI组件
import Count from "../../components/Count/Count";

// 引入connect用于链接UI组件与redux
import {connect} from "react-redux"

// 引入创建action对象的函数
import {createDecrementAction, createIncrementAction, createIncrementAsyncAction, createIncrementIfAddAction} from "../../redux/countAction/countAction";


// 1.mapStateToProps 函数返回的是一个对象
// 2.返回对象中的key就作为传递给UI组件props的key，value就作为传递给UI组件的props的value
// 3.mapStateToProps用于传递状态
const mapStateToProps = (state) => {
    return {count: state}
}

// 1.mapDispatchToProps函数返回的是一个对象
// 2.返回对象中的key就作为传递给UI组件props的key，value就作为传递给UI组件的props的value
// 3.mapDispatchToProps用于传递操作状态的方法
const mapDispatchToProps = (dispatch) => {
    return {
        increment: value => dispatch(createIncrementAction(value)),
        decrement: value => dispatch(createDecrementAction(value)),
        incrementIfAdd: value => dispatch(createIncrementIfAddAction(value)),
        incrementAsync: (value, time) => dispatch(createIncrementAsyncAction(value, time))
    }
}

// connect()创建并且暴露Count的容器组件
export default connect(mapStateToProps, mapDispatchToProps)(Count)

/*
* 总结：
*   一：明确两个概念：
*       1.UI组件：不能使用任何redux的API，只是负责页面的交互和呈现等
*       2.容器组件：负责和redux通信，将结果交给UI组件
*   二：如何创建一个容器组件————靠react-redux的connect()函数
*       connect(mapStateToProps,mapDispatchToProps)(UI组件)
*           mapStateToProps：映射状态，返回值是一个对象
*           mapDispatchToProps：映射操作状态的方法，返回值是一个对象
* 三：备注
*       容器组件中的store是靠props传递的，并不是直接在容器组件中引入store    
* */
```

UI组件

```jsx
import React, {Component} from 'react';
import "./Count.css"

class Count extends Component {
    constructor(props) {
        super(props)
        this.state = {value: 1}
    }


    // 双向数据绑定
    handleChange = (event) => {
        const {value} = event.target
        this.setState({value: Number(value)})
    }

    increment = () => {
        const {value} = this.state
        this.props.increment(value)
    }
    decrement = () => {
        const {value} = this.state
        this.props.decrement(value)
    }

    incrementIfAdd = () => {
        const {value} = this.state
        this.props.incrementIfAdd(value)
    }

    incrementAsync = () => {
        const {value} = this.state
        this.props.incrementAsync(value,1000)
    }


    render() {
        const {count} = this.props
        const {value} = this.state
        console.log(this.props)
        return (
            <div className="count">
                <h1>当前求和count的值为:{count}</h1>
                <select value={value} onChange={this.handleChange}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
                <button onClick={this.increment}>+</button>
                <button onClick={this.decrement}>-</button>
                <button onClick={this.incrementIfAdd}>奇数加</button>
                <button onClick={this.incrementAsync}>异步加</button>
            </div>
        );
    }
}

export default Count
```

store

```JavaScript
import {createStore, applyMiddleware, compose} from "redux"
import thunk from "redux-thunk";
import countReducer from "../counterReducer/counterReducer"
export default createStore(countReducer, compose(applyMiddleware(thunk)))
```

action

```JavaScript
import {increment, decrement} from "../countConst/countConst";
import countStore from "../countStore/countStore";

export const createIncrementAction = (data) => ({type: increment, data})

export const createDecrementAction = (data) => ({type: decrement, data})

export const createIncrementIfAddAction = (data) => {
    return () => {
        if (countStore.getState() % 2 !== 0) {
            countStore.dispatch(createIncrementAction(data))
        }
    }
}
export const createIncrementAsyncAction = (data, time) => {
    return () => {
        setTimeout(() => {
            countStore.dispatch(createIncrementAction(data))
        }, time)
    }
}
```

reducer

```JavaScript
import {increment, decrement} from "../countConst/countConst";

const initState = 0
export default function counterReducer(preState = initState, action) {
    const {type, data} = action
    switch (type) {
        case increment:
            return preState + data
        case decrement:
            return preState - data
        default:
            return preState
    }
}
```



# React-Redux的优化

```jsx
// 引入Count的UI组件
import Count from "../../components/Count/Count";

// 引入connect用于链接UI组件与redux
import {connect} from "react-redux"

// 引入创建action对象的函数
import {createDecrementAction, createIncrementAction, createIncrementAsyncAction, createIncrementIfAddAction} from "../../redux/countAction/countAction";

/*
* 总结：
*   一：明确两个概念：
*       1.UI组件：不能使用任何redux的API，只是负责页面的交互和呈现等
*       2.容器组件：负责和redux通信，将结果交给UI组件
*   二：如何创建一个容器组件————靠react-redux的connect()函数
*       connect(mapStateToProps,mapDispatchToProps)(UI组件)
*           mapStateToProps：映射状态，返回值是一个对象
*           mapDispatchToProps：映射操作状态的方法，返回值是一个对象
* 三：备注
*       容器组件中的store是靠props传递的，并不是直接在容器组件中引入store
*       mapDispatchToProps可以是一个函数也可以是一个对象
*/

/*
1.mapStateToProps 函数返回的是一个对象
2.返回对象中的key就作为传递给UI组件props的key，value就作为传递给UI组件的props的value
3.mapStateToProps用于传递状态
const mapStateToProps = state => ({count: state})
*/

/*
1.mapDispatchToProps函数返回的是一个对象
2.返回对象中的key就作为传递给UI组件props的key，value就作为传递给UI组件的props的value
3.mapDispatchToProps用于传递操作状态的方法
const mapDispatchToProps = dispatch => {
    return {
        increment: value => dispatch(createIncrementAction(value)),
        decrement: value => dispatch(createDecrementAction(value)),
        incrementIfAdd: value => dispatch(createIncrementIfAddAction(value)),
        incrementAsync: (value, time) => dispatch(createIncrementAsyncAction(value, time))
    }
}
connect()创建并且暴露Count的容器组件
export default connect(mapStateToProps,mapDispatchToProps)(Count)
*/



/*
connect()创建并且暴露Count的容器组件
export default connect(state => ({count: state}), dispatch => ({
    increment: value => dispatch(createIncrementAction(value)),
    decrement: value => dispatch(createDecrementAction(value)),
    incrementIfAdd: value => dispatch(createIncrementIfAddAction(value)),
    incrementAsync: (value, time) => dispatch(createIncrementAsyncAction(value, time))
}))(Count)
*/


/*
connect()创建并且暴露Count的容器组件
export default connect(state => ({count: state}), {
    increment: createIncrementAction,
    decrement: createDecrementAction,
    incrementIfAdd: createIncrementIfAddAction,
    incrementAsync: createIncrementAsyncAction
})(Count)
*/

export default connect(state => ({count: state}), {
    createIncrementAction,
    createDecrementAction,
    createIncrementIfAddAction,
    createIncrementAsyncAction
})(Count)
```

# Provider组件的使用

```JavaScript
import React from 'react';
import ReactDOM from 'react-dom';
// 引入store
import countStore from "./redux/countStore/countStore";
// 引入Provider组件
import {Provider} from "react-redux"
import App from './App';
ReactDOM.render(<Provider store={countStore}><App></App></Provider>, document.querySelector("#root"))
```

# 整合容器组件与UI组件

```jsx
import {connect} from "react-redux"
import {createDecrementAction, createIncrementAction, createIncrementAsyncAction, createIncrementIfAddAction} from "../../redux/countAction/countAction";
import React, {Component} from 'react';

class Count extends Component {
    constructor(props) {
        super(props)
        this.state = {value: 1}
    }


    // 双向数据绑定
    handleChange = (event) => {
        const {value} = event.target
        this.setState({value: Number(value)})
    }

    increment = () => {
        const {value} = this.state
        // this.props.increment(value)
        this.props.createIncrementAction(value)
    }
    decrement = () => {
        const {value} = this.state
        // this.props.decrement(value)
        this.props.createDecrementAction(value)
    }

    incrementIfAdd = () => {
        const {value} = this.state
        // this.props.incrementIfAdd(value)
        this.props.createIncrementIfAddAction(value)
    }

    incrementAsync = () => {
        const {value} = this.state
        // this.props.incrementAsync(value, 1000)
        this.props.createIncrementAsyncAction(value, 1000)
    }


    render() {
        const {count} = this.props
        const {value} = this.state
        console.log(this.props)
        return (
            <div className="count">
                <h1>当前求和count的值为:{count}</h1>
                <select value={value} onChange={this.handleChange}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
                <button onClick={this.increment}>+</button>
                <button onClick={this.decrement}>-</button>
                <button onClick={this.incrementIfAdd}>奇数加</button>
                <button onClick={this.incrementAsync}>异步加</button>
            </div>
        );
    }
}

export default connect(state => ({count: state}), {
    createIncrementAction,
    createDecrementAction,
    createIncrementIfAddAction,
    createIncrementAsyncAction
})(Count)
```