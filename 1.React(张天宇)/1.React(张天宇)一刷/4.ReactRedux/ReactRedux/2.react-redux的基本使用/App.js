import React, {Component} from 'react';

// 引入redux(store)
import countStore from "./redux/countStore/countStore";

import Count from "./containers/Count/Count";

class App extends Component {
    render() {
        return (
            <div className="app">
                {/*渲染Count的容器组件，并且给容器组件传递store*/}
                <Count store={countStore}></Count>
            </div>
        );
    }

    componentDidMount() {
        /*监测redux中状态的改变，如果redux的状态发生了改变，则调用forceUpdate函数强制更新一次*/
        countStore.subscribe(() => {
            this.forceUpdate()
        })
    }
}

export default App;