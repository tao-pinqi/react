// 引入Count的UI组件
import Count from "../../components/Count/Count";

// 引入connect用于链接UI组件与redux
import {connect} from "react-redux"

// 引入创建action对象的函数
import {createDecrementAction, createIncrementAction, createIncrementAsyncAction, createIncrementIfAddAction} from "../../redux/countAction/countAction";


// 1.mapStateToProps 函数返回的是一个对象
// 2.返回对象中的key就作为传递给UI组件props的key，value就作为传递给UI组件的props的value
// 3.mapStateToProps用于传递状态
const mapStateToProps = (state) => {
    return {count: state}
}

// 1.mapDispatchToProps函数返回的是一个对象
// 2.返回对象中的key就作为传递给UI组件props的key，value就作为传递给UI组件的props的value
// 3.mapDispatchToProps用于传递操作状态的方法
const mapDispatchToProps = (dispatch) => {
    return {
        increment: value => dispatch(createIncrementAction(value)),
        decrement: value => dispatch(createDecrementAction(value)),
        incrementIfAdd: value => dispatch(createIncrementIfAddAction(value)),
        incrementAsync: (value, time) => dispatch(createIncrementAsyncAction(value, time))
    }
}

// connect()创建并且暴露Count的容器组件
export default connect(mapStateToProps, mapDispatchToProps)(Count)




