import React, {Component} from 'react';
import countStore from "../../redux/countStore/countStore";
import "./Count.css"
import {createDecrementAction, createIncrementAction, createIncrementAsyncAction, createIncrementIfAddAction} from "../../redux/countAction/countAction";

class Count extends Component {
    constructor(props) {
        super(props)
        this.state = {value: 1}
    }


    // 双向数据绑定
    handleChange = (event) => {
        const {value} = event.target
        this.setState({value: Number(value)})
    }

    increment = () => {
        const {value} = this.state
        countStore.dispatch(createIncrementAction(value))
    }
    decrement = () => {
        const {value} = this.state
        countStore.dispatch(createDecrementAction(value))
    }

    incrementIfAdd = () => {
        const {value} = this.state
        countStore.dispatch(createIncrementIfAddAction(value))
    }

    incrementAsync = () => {
        const {value} = this.state
        countStore.dispatch(createIncrementAsyncAction(value, 1000))
    }

    componentDidMount() {
        countStore.subscribe(() => {
            this.forceUpdate()
        })
    }

    render() {
        const {value} = this.state
        const {getState} = countStore
        return (
            <div className="count">
                <h1>当前求和count的值为：{getState()}</h1>
                <select value={value} onChange={this.handleChange}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
                <button onClick={this.increment}>+</button>
                <button onClick={this.decrement}>-</button>
                <button onClick={this.incrementIfAdd}>奇数加</button>
                <button onClick={this.incrementAsync}>异步加</button>
            </div>
        );
    }
}

export default Count;