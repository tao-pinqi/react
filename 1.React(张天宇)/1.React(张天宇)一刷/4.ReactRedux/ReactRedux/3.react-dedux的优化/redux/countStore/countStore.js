import {createStore, applyMiddleware, compose} from "redux"
import thunk from "redux-thunk";
import countReducer from "../counterReducer/counterReducer"

export default createStore(countReducer, compose(applyMiddleware(thunk)))