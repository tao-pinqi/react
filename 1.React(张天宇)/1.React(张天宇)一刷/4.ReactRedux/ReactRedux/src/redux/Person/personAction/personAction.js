import {addPerson} from "../../constant";

export const createAddPersonAction = (data) => ({type: addPerson, data})