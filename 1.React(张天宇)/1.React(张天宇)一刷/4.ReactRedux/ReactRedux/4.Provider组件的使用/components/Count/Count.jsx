import React, {Component} from 'react';
import "./Count.css"
import {createDecrementAction, createIncrementAction, createIncrementAsyncAction, createIncrementIfAddAction} from "../../redux/countAction/countAction";

class Count extends Component {
    constructor(props) {
        super(props)
        this.state = {value: 1}
    }


    // 双向数据绑定
    handleChange = (event) => {
        const {value} = event.target
        this.setState({value: Number(value)})
    }

    increment = () => {
        const {value} = this.state
        // this.props.increment(value)
        this.props.createIncrementAction(value)
    }
    decrement = () => {
        const {value} = this.state
        // this.props.decrement(value)
        this.props.createDecrementAction(value)
    }

    incrementIfAdd = () => {
        const {value} = this.state
        // this.props.incrementIfAdd(value)
        this.props.createIncrementIfAddAction(value)
    }

    incrementAsync = () => {
        const {value} = this.state
        // this.props.incrementAsync(value, 1000)
        this.props.createIncrementAsyncAction(value, 1000)
    }


    render() {
        const {count} = this.props
        const {value} = this.state
        console.log(this.props)
        return (
            <div className="count">
                <h1>当前求和count的值为:{count}</h1>
                <select value={value} onChange={this.handleChange}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
                <button onClick={this.increment}>+</button>
                <button onClick={this.decrement}>-</button>
                <button onClick={this.incrementIfAdd}>奇数加</button>
                <button onClick={this.incrementAsync}>异步加</button>
            </div>
        );
    }
}

export default Count;