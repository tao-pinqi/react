// 引入Count的UI组件
import Count from "../../components/Count/Count";

// 引入connect用于链接UI组件与redux
import {connect} from "react-redux"

// 引入创建action对象的函数
import {createDecrementAction, createIncrementAction, createIncrementAsyncAction, createIncrementIfAddAction} from "../../redux/countAction/countAction";

/*
* 总结：
*   一：明确两个概念：
*       1.UI组件：不能使用任何redux的API，只是负责页面的交互和呈现等
*       2.容器组件：负责和redux通信，将结果交给UI组件
*   二：如何创建一个容器组件————靠react-redux的connect()函数
*       connect(mapStateToProps,mapDispatchToProps)(UI组件)
*           mapStateToProps：映射状态，返回值是一个对象
*           mapDispatchToProps：映射操作状态的方法，返回值是一个对象
* 三：备注
*       容器组件中的store是靠props传递的，并不是直接在容器组件中引入store
*       mapDispatchToProps可以是一个函数也可以是一个对象
*/

/*
1.mapStateToProps 函数返回的是一个对象
2.返回对象中的key就作为传递给UI组件props的key，value就作为传递给UI组件的props的value
3.mapStateToProps用于传递状态
const mapStateToProps = state => ({count: state})
*/

/*
1.mapDispatchToProps函数返回的是一个对象
2.返回对象中的key就作为传递给UI组件props的key，value就作为传递给UI组件的props的value
3.mapDispatchToProps用于传递操作状态的方法
const mapDispatchToProps = dispatch => {
    return {
        increment: value => dispatch(createIncrementAction(value)),
        decrement: value => dispatch(createDecrementAction(value)),
        incrementIfAdd: value => dispatch(createIncrementIfAddAction(value)),
        incrementAsync: (value, time) => dispatch(createIncrementAsyncAction(value, time))
    }
}
connect()创建并且暴露Count的容器组件
export default connect(mapStateToProps,mapDispatchToProps)(Count)
*/



/*
connect()创建并且暴露Count的容器组件
export default connect(state => ({count: state}), dispatch => ({
    increment: value => dispatch(createIncrementAction(value)),
    decrement: value => dispatch(createDecrementAction(value)),
    incrementIfAdd: value => dispatch(createIncrementIfAddAction(value)),
    incrementAsync: (value, time) => dispatch(createIncrementAsyncAction(value, time))
}))(Count)
*/


/*
connect()创建并且暴露Count的容器组件
export default connect(state => ({count: state}), {
    increment: createIncrementAction,
    decrement: createDecrementAction,
    incrementIfAdd: createIncrementIfAddAction,
    incrementAsync: createIncrementAsyncAction
})(Count)
*/


// connect()创建并且暴露Count的容器组件
export default connect(state => ({count: state}), {
    createIncrementAction,
    createDecrementAction,
    createIncrementIfAddAction,
    createIncrementAsyncAction
})(Count)





