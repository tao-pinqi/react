import React from 'react';
import ReactDOM from 'react-dom';
// 引入store
import countStore from "./redux/countStore/countStore";
// 引入Provider组件
import {Provider} from "react-redux"
import App from './App';

ReactDOM.render(<Provider store={countStore}><App></App></Provider>, document.querySelector("#root"))