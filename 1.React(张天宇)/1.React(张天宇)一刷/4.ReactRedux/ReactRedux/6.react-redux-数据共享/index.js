import React from 'react';
import ReactDOM from 'react-dom';
// 引入store
import store from "./redux/store";
// 引入Provider组件
import {Provider} from "react-redux"
import App from './App';

ReactDOM.render(<Provider store={store}><App></App></Provider>, document.querySelector("#root"))