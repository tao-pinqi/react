import React, {Component} from 'react';

import {connect} from "react-redux";

import {createAddPersonAction} from "../../redux/Person/personAction/personAction";

class Person extends Component {
    render() {
        const {persons, count} = this.props
        return (
            <div className="person">
                <h1>我是Person组件</h1>
                <h1>上方组件求和为：{count}</h1>
                <input type="text" placeholder="输入名字" ref={c => this.name = c}/>
                <input type="text" placeholder="输入年龄" ref={c => this.age = c}/>
                <button onClick={this.addPerson}>添加</button>
                <ul>
                    {
                        persons.map(person => (<li key={person.id}>名字：{person.name} 年龄：{person.age}</li>))
                    }
                </ul>
            </div>
        );
    }

    addPerson = () => {
        const {name: {value: nameValue}, age: {value: ageValue}} = this
        const personObject = {name: nameValue, age: ageValue, id: Math.random()}
        this.props.createAddPersonAction(personObject)
    }
}

export default connect(state => ({persons: state.persons, count: state.count}), {
    createAddPersonAction
})(Person);