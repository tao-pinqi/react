import {createStore, applyMiddleware, compose, combineReducers} from "redux"
import thunk from "redux-thunk";

import countReducer from "./count/counterReducer/counterReducer"
import personReducer from "./Person/personReducer/personReducer";

/*合并reducer变为一个总的reducer*/
const allReducer = combineReducers({
    count: countReducer,
    persons: personReducer
})
export default createStore(allReducer, compose(applyMiddleware(thunk)))