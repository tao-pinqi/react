import {addPerson} from "../../constant";

const initPersons = [{id: "1", name: "陶品奇", age: 22}]
export default function personReducer(preState = initPersons, action) {
    const {type, data} = action
    switch (type) {
        case addPerson:
            return [data, ...preState]
        default:
            return preState
    }
}