import {increment, decrement} from "../../constant";
import countStore from "../../store";

export const createIncrementAction = (data) => ({type: increment, data})

export const createDecrementAction = (data) => ({type: decrement, data})

export const createIncrementIfAddAction = (data) => {
    return () => {
        if (countStore.getState() % 2 !== 0) {
            countStore.dispatch(createIncrementAction(data))
        }
    }
}
export const createIncrementAsyncAction = (data, time) => {
    return () => {
        setTimeout(() => {
            countStore.dispatch(createIncrementAction(data))
        }, time)
    }
}
