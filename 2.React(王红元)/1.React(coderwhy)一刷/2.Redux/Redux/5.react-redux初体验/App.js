import React, { Component } from 'react';
/*引入store*/
import store from "./redux/store";
/*引入创建同步action的函数*/
import { createAddAction, createChangeNameAction } from "./redux/actions/action";
import Header from './components/Header';

class App extends Component {
    render() {
        /*从store中解构赋值拿到getState()函数(调用此函数用于获取store中的数据)*/
        const { getState } = store
        return (
            <div>
                <h1>姓名：{getState().name}</h1>
                <h1>年龄：{getState().age}</h1>
                <button onClick={this.addAgeHandle}>年龄+1</button>
                <button onClick={this.changeHandle}>修改姓名</button>
                <Header></Header>
            </div>
        );
    }

    addAgeHandle = () => {
        // 调用store中的dispatch方法去派发一个action修改数据
        store.dispatch(createAddAction(1))
    }
    changeHandle = () => {
        // 调用store中的dispatch方法去派发一个action修改数据
        store.dispatch(createChangeNameAction("马云"))
    }

    componentDidMount() {

        /*订阅store中数据的变化*/
        this.unsubscribe = store.subscribe(() => {
            this.forceUpdate()
        })
    }

    componentWillUnmount() {
        /*取消订阅store中数据变化*/
        this.unsubscribe()
    }
}

export default App;
