import React, { Component } from 'react'
/* 引入connect()函数用于创建高阶组件 */
import { connect } from 'react-redux'
/*  */
import { createAddAction, createChangeNameAction } from '../redux/actions/action'
class Header extends Component {
    render() {
        const { name, age } = this.props
        return (
            <div>
                <h1>姓名：{name}</h1>
                <h1>年龄：{age}</h1>
                <button onClick={this.addAgeHandle}>年龄+1</button>
                <button onClick={this.changeHandle}>修改姓名</button>
            </div>
        )
    }
    addAgeHandle = () => {
        this.props.createAddAction(1)
    }
    changeHandle = () => {
        this.props.createChangeNameAction("马化腾")
    }
}

/*const mapStateToProps = (state) => {
    return {
        name: state.name,
        age: state.age
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        createAddAction: value => dispatch(createAddAction(value)),
        createChangeNameAction: value => dispatch(createChangeNameAction(value))

    }
}*/

/* 
Connect()返回值是一个高阶组件 
第一次调用的时候传入两个函数：mapStateToProps、 mapDispatchToProps（可以是对象也可以是函数）
第二次调用的时候传入要使用store中数据的组件
*/
export default connect(({ name, age }) => ({ name, age }), {
    createAddAction,
    createChangeNameAction
})(Header)