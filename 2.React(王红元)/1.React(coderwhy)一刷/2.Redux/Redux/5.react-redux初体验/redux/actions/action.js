import { addAge,changeName } from "../constants"
export const createAddAction = (value) => ({type: addAge, value})
export const createChangeNameAction = (name) => ({type: changeName, name})
