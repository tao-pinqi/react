/*引入redux中的createStore函数用于创建store*/
import {createStore} from "redux";
/*引入为store服务的reducer*/
import reducer from "./reducer/reducer";
/*调用createStore函数创建store，并且传入reducer*/
const store = createStore(reducer)
/*默认导出store*/
export default store
