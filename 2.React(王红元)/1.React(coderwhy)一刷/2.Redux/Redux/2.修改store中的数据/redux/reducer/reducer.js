/*定义一个初始化的数据*/
const initState = {
    name: "陶品奇",
    age: 0,
}

/*
* reducer函数接收两个参数：
* 参数一：preState(store中目前保存的state)，
* 参数二：本次需要更新的action(dispatch的动作对象)
* 返回值：返回值作为store之后存储的值
*/
export default function reducer(preState = initState, action) {
    /* 如果有新数据更新的时候则返回一个新的state，如果没有新数据的更新则返回一个之前的state*/
    const {type} = action
    switch (type) {
        case "addAge":
            return {...preState, age: preState.age + action.value}
        case "changeName":
            return {...preState, name: action.name}
        default:
            // 将初始化的数据返回出去
            return preState
    }
}
