import React, {Component} from 'react';
/*引入store*/
import store from "./redux/store";

class App extends Component {
    render() {
        /*从store中解构赋值拿到getState()函数(调用此函数用于获取store中的数据)*/
        const {getState} = store
        return (
            <div>
                <h1>initState的值为：{getState()}</h1>
            </div>
        );
    }
}

export default App;
