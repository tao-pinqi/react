# 什么是redux

一个组件里可能会有很多的状态，比如控制某个内容显示的flag，从后端获取的展示数据，那么这些状态可以在自己的单个页面进行管理，也可以选择别的管理方式，redux就是是一种状态管理的方式

<img src="E:\01.前端\15.React\React(why)\1.React\images\redux.png" style="zoom: 50%;" />

# action

定义：Action 是把数据从应用传到 store 的有效载荷。它是 store 数据的唯一来源。一般来说你会通过 store.dispatch() 将 action 传到 store。

**action的本质**

> action的本质是 JavaScript 普通对象。我们约定，action 内必须使用一个字符串类型的 type 字段来表示将要执行的动作。多数情况下，type 会被定义成字符串常量。当应用规模越来越大时，建议使用单独的模块或文件来存放 action

**action 创建函数**

Action 创建函数 就是生成 action 的方法。“action” 和 “action 创建函数” 这两个概念很容易混在一起，使用时最好注意区分。

在 Redux 中的 action 创建函数只是简单的返回一个 action

```JavaScript
import { addAge,changeName } from "../constants"

export const createAddAction = (value) => ({type: addAge, value})
export const createChangeNameAction = (name) => ({type: changeName, name})
```

# reducer

Store 收到 Action 以后，必须给出一个新的 State，这样 View 才会发生变化。这种 State 的计算过程就叫做 Reducer。
Reducer 是一个纯函数，它接受 Action 和当前 State 作为参数，返回一个新的 State。

```JavaScript
import { addAge,changeName } from "../constants"

/*定义一个初始化的数据*/
const initState = {
    name: "陶品奇",
    age: 0,
}

/*
* reducer函数接收两个参数：
* 参数一：preState(store中目前保存的state)，
* 参数二：本次需要更新的action(dispatch的动作对象)
* 返回值：返回值作为store之后存储的值
*/
export default function reducer(preState = initState, action) {
    /* 如果有新数据更新的时候则返回一个新的state，如果没有新数据的更新则返回一个之前的state*/
    const {type} = action
    switch (type) {
        case addAge:
            return {...preState, age: preState.age + action.value}
        case changeName:
            return {...preState, name: action.name}
        default:
            // 将初始化的数据返回出去
            return preState
    }
}

```



# store

> 提供 getState() 方法获取 state
>
> 提供 dispatch(action) 方法更新 state；
>
> 通过 subscribe(listener) 注册监听器
>
> 通过 subscribe(listener) 返回的函数注销监听器

```JavaScript
/*引入redux中的createStore函数用于创建store*/
import {createStore} from "redux";
/*引入为store服务的reducer*/
import reducer from "./reducer/reducer";
/*调用createStore函数创建store，并且传入reducer*/
const store = createStore(reducer)
/*默认导出store*/
export default store
```



# 使用store中的数据

```JavaScript
import React, {Component} from 'react';
/*引入store*/
import store from "./redux/store";

class App extends Component {
    render() {
        /*从store中解构赋值拿到getState()函数(调用此函数用于获取store中的数据)*/
        const {getState} = store
        return (
            <div>
                <h1>initState的值为：{getState()}</h1>
            </div>
        );
    }
}

export default App;
```

```JavaScript
/*引入redux中的createStore函数用于创建store*/
import {createStore} from "redux";
/*引入为store服务的reducer*/
import reducer from "./reducer/reducer";
/*调用createStore函数创建store，并且传入reducer*/
const store = createStore(reducer)
/*默认导出store*/
export default store
```

```JavaScript
/*定义一个初始化的数据*/
const initState = 0

export default function reducer(preState = initState, action) {
    // 将初始化的数据返回出去
    return initState
}
```

# 修改store中的数据

```JavaScript
import React, {Component} from 'react';
/*引入store*/
import store from "./redux/store";
/*引入创建同步action的函数*/
import {createAddAction, createChangeNameAction} from "./redux/actions/action";

class App extends Component {
    render() {
        /*从store中解构赋值拿到getState()函数(调用此函数用于获取store中的数据)*/
        const {getState} = store
        return (
            <div>
                <h1>姓名：{getState().name}</h1>
                <h1>年龄：{getState().age}</h1>
                <button onClick={this.addAgeHandle}>年龄+1</button>
                <button onClick={this.changeHandle}>修改年龄</button>
            </div>
        );
    }

    addAgeHandle = () => {
        // 调用store中的dispatch方法去派发一个action修改数据
        store.dispatch(createAddAction())
    }
    changeHandle = () => {
         // 调用store中的dispatch方法去派发一个action修改数据
        store.dispatch(createChangeNameAction())
    }

    componentDidMount() {
        store.subscribe(() => {
            this.forceUpdate()
        })
    }
}

export default App;
```

```JavaScript
/*引入redux中的createStore函数用于创建store*/
import {createStore} from "redux";
/*引入为store服务的reducer*/
import reducer from "./reducer/reducer";
/*调用createStore函数创建store，并且传入reducer*/
const store = createStore(reducer)
/*默认导出store*/
export default store
```

```JavaScript
export const createAddAction = () => ({type: "addAge", value: 1})
export const createChangeNameAction = () => ({type: "changeName", name: "马云"})
```

```JavaScript
/*定义一个初始化的数据*/
const initState = {
    name: "陶品奇",
    age: 0,
}

/*
* reducer函数接收两个参数：
* 参数一：preState(store中目前保存的state)，
* 参数二：本次需要更新的action(dispatch的动作对象)
* 返回值：返回值作为store之后存储的值
*/
export default function reducer(preState = initState, action) {
    /* 如果有新数据更新的时候则返回一个新的state，如果没有新数据的更新则返回一个之前的state*/
    const {type} = action
    switch (type) {
        case "addAge":
            return {...preState, age: preState.age + action.value}
        case "changeName":
            return {...preState, name: action.name}
        default:
            // 将初始化的数据返回出去
            return preState
    }
}
```

# 数据订阅

```JavaScript
componentDidMount() {
    /*订阅store中数据的变化*/
    this.unsubscribe = store.subscribe(() => {
        this.forceUpdate()
    })
}

componentWillUnmount() {
    /*取消订阅store中数据变化*/
    this.unsubscribe()
}
```



# redux的三大原则

## 单一数据源

整个应用程序的state被存储在一颗object tree中，并且这个object tree只存储在一个store中

redux并没有强制我们不能创建多个store，多个store不利于数据的维护

单一的数据源可以让整个应用程序的state变得方便维护、追踪、修改

## state是只读的

唯一修改state的方法一定是触发action，不要视图在其它任何地方通过任何的方式来修改state

## reducer必须是纯函数

通过reducer将旧的state和actions联系在一起，并且返回一个新的state

随着应用程序的复杂度增加，可以将多个reducer拆分成多个小的reducer，分别操作不同的state tree

所有的reducer必须是纯函数，不能产生副作用
