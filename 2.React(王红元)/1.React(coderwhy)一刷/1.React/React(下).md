# React介绍

## React是什么

React由Meta公司研发，是一个用于构建web和原生交互界面的库

## React的优势

相较于传统基于DOM开发的优势：组件化的开发、不错的性能

相较于其它前端框架的优势：丰富的生态、跨平台支持

![](E:\01.前端\15.React\React(黑马)\images\React的优势.png)



# 开发环境搭建

create-react-app是一个快速创建React开发环境的工具，底层由webpack构建，封装了配置细节开箱即用

```
npx create-react-app my-app
```





# JSX基础

JSX是JavaScript和XML(HTML)的缩写，表示在JavaScript代码中编写HTML模板结构，它是React中编写UI模板的方式

## JSX的本质

JSX并不是标准的JS语法，它是JS的语法扩展，浏览器本身不能识别，需要通过babel解析工具做解析之后才能在浏览器中运行

## JSX中使用表达式

在JSX中可以通过大括号语法{}识别JavaScript中的表达式，比如常见的变量、函数调用以及方法调用

注意：只能识别表达式不能识别JavaScript的语句

`使用引号传递字符串`

```JavaScript
function App(props) {
    return (
        <div className="app">
            {/*使用引号传递字符串*/}
            <h3>{'我是App组件'}</h3>
        </div>
    );
}

export default App;
```

`使用JavaScript变量`

```JavaScript
const count = 100

function App(props) {
    return (
        <div className="app">
            {/*使用JavaScript变量*/}
            <h1>{count}</h1>
        </div>
    );
}

export default App;
```

`函数调用和方法调用`

```JavaScript
const getSum = (x, y) => {
    return x + y
}
const object = {
    sayHai() {
        return "你好啊"
    }
}

function App(props) {
    return (
        <div className="app">
            {/*函数调用和方法调用*/}
            <h1>{getSum(10, 20)}</h1>
            <h1>{object.sayHai()}</h1>
        </div>
    );
}

export default App;
```

`使用JavaScript对象`

```JavaScript
function App(props) {
    return (
        <div className="app">
        	{/*使用JavaScript对象*/}
            <h1>{Math.random()}</h1>
        </div>
    );
}

export default App;
```

## JSX实现列表渲染

```JavaScript
const list = [
    {id: 1, name: "Vue"},
    {id: 2, name: "React"},
    {id: 3, name: "Angular"},
]

function App(props) {
    return (
        <div className="app">
            <ul>
                {/*使用map方法映射列表*/}
                {list.map(item => <li key={item.id}>{item.name}</li>)}
            </ul>
        </div>
    );
}

export default App;
```

## JSX实现条件渲染

在React中，可以通过逻辑与(&&)、三元表达式、以及if语句实现条件渲染

```JavaScript
const mony = 95
const isLogin=true
function App(props) {
    const renderSpan = () => {
        if (mony >= 100) {
            return <span>大于等于100</span>
        } else if (mony >= 90) {
            return <span>大于等于90</span>
        } else {
            return <span>凉凉</span>
        }
    }
    return (
        <div className="app">
            {
           		{/*逻辑与进行条件判断*/}
            	{isLogin && <span>我是span元素</span>}
                 
            	{/*三元表达式进行条件判断*/}
            	{isLogin ? <button>退出登录</button> : <button>登录</button>}
                
            	{/*调用函数进行渲染*/}
            	renderSpan()            
            }
        </div>
    );
}

export default App;
```



# 事件绑定

语法：on+事件名称={事件处理程序}，整体上遵循驼峰命名的写法

```JavaScript
function App(props) {
    // 事件处理函数
    const handleClick=()=>{
        console.log("button被点击了")
    }
    return (
        <div>
            {/*为button元素绑定onClick事件*/}
            <button onClick={handleClick}>点击</button>
        </div>
    );
}

export default App;
```

## 事件对象参数

语法：在事件处理函数形参中设置event即可得到事件对象

```JavaScript
function App(props) {
    const handleClick=(event)=>{
        console.log("button被点击了",event.target)
    }
    return (
        <div>
            <button onClick={handleClick}>点击</button>
        </div>
    );
}

export default App;
```

## 自定义参数和事件对象

语法：事件绑定的形式改造成箭头函数的写法，在执行clickHandle实际处理业务函数的时候传递实参和事件对象

注意：不能直接写函数调用，这里的事件绑定需要一个函数引用

```JavaScript
function App(props) {
    const handleClick=(event,value)=>{
        // event：事件对象参数 value：自定义参数
        console.log(event,value)
    }
    return (
        <div>
            {/*使用箭头函数的形式绑定事件，即可传递事件对象也可传递自定义参数*/}
            <button onClick={(event)=>{handleClick(event,10)}}>点击</button>
        </div>
    );
}

export default App;
```



# React组件

一个组件就是用户界面的一部分，组件有自己的逻辑和外观，组件直接可以相互嵌套也可以复用

组件化化的开发模式可以让开发者像搭积木一样构建整个庞大的应用

在React中，一个组件就是首字母大写的函数，内部存放了组件的逻辑和视图UI，渲染组件只需要把组件当成标签书写即可

```JavaScript
/*定义一个组件*/
const Button = (props) => {
    return (<button>{props.children}</button>)
}

function App(props) {
    return (
        <div>
            {/*以按钮的形式使用组件*/}
            <Button>我是按钮组件</Button>
        </div>
    );
}

export default App;
```



# useState()

useState函数是React Hook(函数)，它应许我们向组件添加一个状态变量，从而控制影响组件的渲染结果

状态变量本质：和普通的JS变量不同的是，状态变量一旦发生变化，组件的视图UI也会跟着更新(数据驱动视图)

![](E:\01.前端\15.React\React(黑马)\images\useState.png)

```JavaScript
import {useState} from "react";

function App(props) {
    // count：状态变量
    // setCount：修改状态变量的方法
    // useState()返回的是一个数组
    const [count,setCount]=useState(0) // 调用useState()添加状态变量
    const handleClick=(value)=>{
        setCount(count+value)
    }
    return (
        <div>
            <h1>count的值为：{count}</h1>
            <button onClick={()=>handleClick(10)}>点击</button>
        </div>
    );
}

export default App;
```



# 修改状态的规则

`状态不可变`：在React中状态被认为是只读的，我们应该始终替换它而不是修改它，直接修改状态不会引发视图的更新

```JavaScript
import {useState} from "react";

function App(props) {
    let [count, setCount] = useState(0)
    const handleClick = (value) => {
        // count+=value  状态不能直接修改(不会引发视图的更新)
        setCount(count + value) // 调用方法去修改状态引发视图更新
    }
    return (
        <div>
            <h1>count的值为：{count}</h1>
            <button onClick={() => handleClick(10)}>点击</button>
        </div>
    );
}

export default App;
```

`修改对象状态`：对于对象类型的状态变量，应该始终给set方法一个全新的对象来进行修改

```JavaScript
import {useState} from "react";

function App(props) {
    const [person, setPerson] = useState({name: "陶品奇", age: 22})

    const handleClick = (value) => {
        /*调用set方法将原先的对象使用展开运算符展开。属性名相同 name会覆盖原先对象的name属性*/
        setPerson({...person, name: value})
    }
    return (
        <div>
            <h1>姓名是：{person.name}</h1>
            <h1>年龄是：{person.age}</h1>
            <button onClick={() => handleClick('马云')}>点击</button>
        </div>
    );
}

export default App;
```

`修改数组状态`：将原先的数组进行浅拷贝一份，然后对浅拷贝的数组进行操作，替换原先的数组

```JavaScript
import {useState} from "react";

function App(props) {
    const [array, setArray] = useState([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

    const handleClick = () => {
        // 将原先的数组先浅拷贝一份
        const newArray = [...array]
        // 向拷贝的数组后面push一个元素
        newArray.push(11)
        // 调用set方法替换原先的数组
        setArray(newArray)
    }
    return (
        <div>
            <ul>
                {array.map(item=><li key={item}>{item}</li>)}
            </ul>
            <button onClick={handleClick}>点击</button>
        </div>
    );
}

export default App;
```



# 组件样式方案

React组件的基础样式控制有两种方式：style行内样式、className类名、module css等

```JavaScript
import React, {useState} from 'react';

function App(props) {
    const [styleObject, setStyleObject] = useState({color: "#c00000", fontSize: '20px'})
    return (
        <div>
            {/*style控制样式*/}
            <div style={styleObject}>我是div元素</div>
            {/*类名控制样式*/}
            <div className='box'>我是div元素</div>
        </div>

    );

}

export default App;
```



# 受控表单绑定

概念：使用React组件的状态(useState)来控制表单的状态

<img src="E:\01.前端\15.React\React(黑马)\images\双向绑定.png" style="zoom:80%;" />

```JavaScript
import {useState} from "react";

function App(props) {
    // 准备一个React状态值
    const [message, setMessage] = useState("")
    return (
        <div>
            <h1>输入框的值是:{message}</h1>
            //通过value属性绑定状态，通过onChange属性绑定状态同步的函数
            <input type="text" value={message} onChange={(e) => {setMessage(e.target.value)}}/>
        </div>
    );
}

export default App;
```



# useRef获取DOM

在React组件中获取或者是操作DOM，需要使用useRef钩子函数

<img src="E:\01.前端\15.React\React(黑马)\images\获取DOM元素.png" style="zoom:80%;" />

```JavaScript
import {useRef} from "react";

function App(props) {
    // 使用useRef()生成一个ref对象
    const inputRef = useRef(null)
    const getDom = () => {
        console.log(inputRef.current)
    }
    return (
        <div>
        	// 绑定到DOM节点中
            <input type="text" ref={inputRef}/>           
            <button onClick={getDom}>获取输入框DOM</button>
        </div>
    );
}

export default App;
```

# 组件通信

组件通信就是组件与组件之间互相传递数据，根据组件的嵌套关系不同有不同的通信方式

## 父传子

父组件传递数据-在子组件标签中绑定自定义属性

子组件接收数据-子组件通过props属性接收数据

```JavaScript
import Son from "./components/Son";

function App() {
    return (
        <div className="App">
            <h1>我是父组件App</h1>
            {/*给子组件传递props*/}
            <Son name={'陶品奇'}></Son>
        </div>
    );
}

export default App;
```

```JavaScript
function Son(props) {
    return (
        <div>
            <h1>我是子组件Son</h1>
            {/*使用props接收父组件传递过来的数据*/}
            <h3>{props.name}</h3>
        </div>
    );
}
export default Son;
```

`props可以传递任意的数据`

数字、字符串、布尔值、数组、对象、函数、JSX

![](E:\01.前端\15.React\React(黑马)\images\props传递任意数据.png)

```JavaScript
import Son from "./components/Son";

function App() {
    return (
        <div className="App">
            <h1>我是父组件App</h1>
            {/*给子组件传递--字符串类型props*/}
            <Son name={'陶品奇'}></Son>
            {/*给子组件传递--数字类型props*/}
            <Son age={22}></Son>
            {/*给子组件传递--布尔类型props*/}
            <Son flag={true}></Son>
            {/*给子组件传递--数组类型props*/}
            <Son persons={[1, 2, 3, 4, 5]}></Son>
            {/*给子组件传递--对象类型props*/}
            <Son obj={{money: 100}}></Son>
            {/*给子组件传递--函数类型props*/}
            <Son callback={() => {console.log(1)}}></Son>
            {/*给子组件传递--JSX结构*/}
            <Son elemen={<span>我是span</span>}></Son>
        </div>
    );
}

export default App;
```

`props是只读的`

子组件只能读取props中的数据，不能直接进行修改，父组件的数据只能由父组件来进行修改

```JavaScript
function Son(props) {
    // 父组件传递过来的props是只读的不能修改
    props.age = 20
    return (
        <div>
            <h1>我是子组件Son</h1>
        </div>
    );
}

export default Son;
```

`props的children属性`

当我们把内容嵌套在子组件标签中的时候，父组件会自动在名为children的prop属性中接收该内容

```JavaScript
import Son from "./components/Son";

function App() {
    return (
        <div className="App">
            <Son>
                {/*给子组件传递一个span元素(类似于插槽的实现)*/}
                <span>我是span</span>
            </Son>
        </div>
    );
}

export default App;
```

```JavaScript
function Son(props) {
    return (
        <div>
            {/*接收子组件传递过来的数据存在children属性中*/}
            {props.children}
        </div>
    );
}

export default Son;
```



## 子传父

父组件传递数据-在子组件标签中绑定自定义属性(这个自定义属性是一个函数)

子组件接收数据-子组件通过props属性调用父组件传递过来的函数props

```JavaScript
import Son from "./components/Son";
import {useState} from "react";

function App() {
    const [message, setMessage] = useState("你好")
    const changeMessage = (message) => {
        setMessage(message)
    }
    return (
        <div className="App">
            <h1>{message}</h1>
            {/*给子组件传递一个函数*/}
            <Son changeMessage={changeMessage}></Son>
        </div>
    );
}

export default App;
```

```JavaScript
function Son(props) {
    const clickHandler=()=>{
        // 调用父组件传递过来的函数
        props.changeMessage("我不好")
    }
    return (
        <div>
            <button onClick={clickHandler}>修改父组件的message</button>
        </div>
    );
}

export default Son;
```



## 兄弟组件







## context

context用于跨层组件之间的通信

> 实现步骤
>
> 1.使用createContext()方法创建一个context上下文对象
>
> 2.在顶层组件中通过Context.Provider组件的value属性提供数据
>
> 3.在底层组件中通过useContext()钩子函数获取消费数据

```JavaScript
import React from "react";
const themeContext = React.createContext(null)
export default themeContext
```

```JavaScript
import {useState} from "react";
import Son from "./components/Son";
import ThemeContext from "./context/themeContext";

function App() {
    const [styleObj, setStyleObj] = useState({color: "#c00000", fontSize: "20px"})
    return (
        <div className="App">
            <ThemeContext.Provider value={styleObj}>
                <Son></Son>
            </ThemeContext.Provider>
        </div>
    );
}

export default App;
```

```JavaScript
import {useContext} from "react";
import themeContext from "../context/themeContext";

function Son(props) {
    const theme = useContext(themeContext)
    return (
        <div>
            <span style={theme}>我是son组件</span>
        </div>
    );
}

export default Son;
```



# useEffect()

useEffect()是一个React的hook函数，用于在React组件中创建不是由事件引起而是由渲染本身引起的操作，比如发送ajax请求、更改DOM等

## 基本使用

```JavaScript
useEfect(()=>{},[])
```

参数一：函数，可以把它叫做副作用函数，在函数内部可以放置要执行的操作

参数二：数组(可选)，在数组里放置依赖项，不同的依赖项会影响第一个参数函数的执行，当是一个空数组的时候副作用函数只会在组件渲染完毕之后执行一次

```JavaScript
import {useEffect, useState} from "react";

function App() {
    // 状态数组
    const [list, setList] = useState([])
    // 发送请求的函数
    const getList = async () => {
        const resul = await fetch("http://geek.itheima.net/v1_0/channels")
        const {data: channels} = await resul.json()
        // 调用setList函数更新状态数据
        setList(channels.channels)
    }
    const deleteItem = (id) => {
        setList(list.filter(item => item.id !== id))
    }
    
    useEffect(() => {
        // 组件初次挂载的时候调用此函数
        getList()
    }, [])
    
    return (
        <div>
            <ul>
                {
                    list.map(item => {
                        return (
                            <li key={item.id}>{item.name}
                                <button onClick={() => deleteItem(item.id)}>删除</button>
                            </li>
                        )
                    })
                }
            </ul>
        </div>
    );
}

export default App;
```

## 依赖项说明

useEffect副作用函数的执行时机存在多种情况，根据传入的依赖项不同也会有不同的表现

![](E:\01.前端\15.React\React(黑马)\images\useEffect()依赖项.png)

`没有依赖项`

> 没有传递依赖项：useEffect副作用函数在组件初始化的时候+组件更新的时候执行

```JavaScript
import {useEffect, useState} from "react";

function App() {
    const [count, setCount] = useState(0)
    // 没有传递依赖项：useEffect副作用函数在组件初始化的时候+组件更新的时候执行
    useEffect(() => {
        console.log("useEffect()副作用函数执行了")
    })
    return (
        <div>
            <h1>count的值是{count}</h1>
            <button onClick={() => setCount(count + 1)}>+1</button>
        </div>
    );
}

export default App;
```

`传入空数组`

> 传递空数组依赖项：useEffect副作用函数在组件初始化的时候执行

```JavaScript
import {useEffect, useState} from "react";

function App() {
    const [count, setCount] = useState(0)
    // 传递空数组依赖项：useEffect副作用函数在组件初始化的时候执行
    useEffect(() => {
        console.log("useEffect()副作用函数执行了")
    },[])
    return (
        <div>
            <h1>count的值是{count}</h1>
            <button onClick={() => setCount(count + 1)}>+1</button>
        </div>
    );
}

export default App;
```

`传入特点依赖项目`

> 传递特点依赖项：useEffect副作用函数在组件初始化的时候+传入的依赖项发生变化的时候执行

```JavaScript
import {useEffect, useState} from "react";

function App() {
    const [count, setCount] = useState(0)
    // 传递特点依赖项：useEffect副作用函数在组件初始化的时候+传入的依赖项发生变化的时候执行
    useEffect(() => {
        console.log("useEffect()副作用函数执行了")
    },[count])
    return (
        <div>
            <h1>count的值是{count}</h1>
            <button onClick={() => setCount(count + 1)}>+1</button>
        </div>
    );
}

export default App;
```

## 清除副作用

在useEffect中编写的由渲染本身引起的对接组件外部的操作，社区也经常把它叫做副作用操作，比如在useEffect中开启了一个定时器，需要在组件卸载的时候把这个定时器清除掉，这个过程就是清理副作用

> 清除副作用的函数最常见的执行时机是在组件卸载的时候自动执行

```JavaScript
import React, {useEffect} from 'react';

function App(props) {
    useEffect(() => {

        // 由于依赖项是空数组-组件初始化的时候执行副作用函数
        const time = setInterval(() => {
            console.log("定时器执行中")
        })

        // 清除副作用-组件卸载的时候执行
        return () => {
            clearInterval(time)
        }
    }, [])
    return (
        <div>

        </div>
    )
}

export default App;
```



# 自定义hook函数

自定义hook函数是以use打头的函数，通过自定义的hook函数可以用来实现逻辑的复用和封装

```JavaScript
import {useToggle} from "./hooks/hooks";

function App(props) {
    const [flag, setFlag] = useToggle(true)
    return (
        <div>
            {flag && <div>我是div元素</div>}
            {flag && <span>我是span元素</span>}
            <button onClick={() => {setFlag(!flag)}}>切换</button>
        </div>
    )
}

export default App;
```

```JavaScript
import {useState} from "react"

export const useToggle = (status) => {
    const [flag, setFlag] = useState(status)
    return [flag, setFlag]
}
```

## ReactHooks函数使用规则

只能在组件中或者其它使用自定义hook函数中调用

只能在组件的顶层调用、不能嵌套在if、for其它函数中

```JavaScript
import {useState} from "react";

// 不能在组件的外层调用hook
const [count,setCount]=useState(0)

function App(props) {
    return (<div></div>)
}

export default App;
```

```JavaScript
import {useState} from "react";

function App(props) {
    // 不能嵌套在其它语句中调用hooks
    if (Math.random()>1){
        const [count,setCount]=useState(0)
    }
    return (<div></div>)
}

export default App;
```



# Redux

Redux是React最常用的集中状态管理工具，类似于Vue中的pinia(vuex)，可以独立于框架运行

作用：通过集中管理的方式管理应用的状态

## Redux快速体验

> 使用步骤
>
> 1.定义一个reducer函数(根据当前想要做的修改返回一个新的状态)
>
> 2.使用createStore方法传入一个reducer函数，生成一个store实例
>
> 3.使用store实例的subscribe方法订阅数据的变化(数据一旦变化可以得到通知)
>
> 4.使用store实例的dispatch方法提交action对象触发数据的更新(告诉reducer怎么改数据)
>
> 5.使用store实例的getState方法获取最新的状态数据更新到视图中

```JavaScript
// 引入createStore方法用于创建一个store
import {createStore} from "redux";
// 引入reducer函数
import countReducer from "./reducer";
// 调用createStore()传入reducer函数用于创建一个store
const store = createStore(countReducer)
//  默认导出store实例
export default store
```

```JavaScript
// 初始化的数据
const initState = 0
// reducer函数用于加工数据
function countReducer(preState = initState, actions) {
    switch (actions.type) {
        case "add":
            return preState + 1
        case "sub":
            return preState - 1
        default:
            return preState
    }
}

export default countReducer
```

```JavaScript
import {useEffect, useState} from "react";
import store from "./store";

function App(props) {
    const [count, setCount] = useState(0)

    const add = () => {
        // 派发一个action
        store.dispatch({type: "add"})
    }
    const sub = () => {
         // 派发一个action
        store.dispatch({type: "sub"})
    }
    useEffect(() => {
         // 监听状态的变化
        store.subscribe(() => {
            setCount(store.getState())
        })
    })

    return (
        <div>
            <h1>count的值是：{count}</h1>
            <button onClick={add}>+1</button>
            <button onClick={sub}>-1</button>
        </div>
    )
}

export default App;
```



## Redux管理数据流程

为了职责清晰数据流向明确，Redux把整个数据修改的流程分成了三个核心概念分别是：state、action、reducer

state：一个对象存放着管理数据的状态

action：一个对象用来描述需要怎么样修改数据

reducer：一个函数，更具有action的描述生成一个新的state



## 环境准备

在React中使用Redux，官方要求安装两个插件：Redux-Toolkit和React-Redux

![](E:\01.前端\15.React\React(黑马)\images\状态管理.png)

```JavaScript
import {useSelector, useDispatch} from "react-redux";
import {add, fetchChannels, sub} from "./redux/modules/countStore";

function App() {
    // 在React组件中使用store中的数据，需要用到一个钩子函数useSelector，它的作用是把store中的数据映射到组件中
    const {count, channels} = useSelector(state => state.count)

    // 在React组件中修改store中的数据，需要用到一个钩子函数useDispatch，它的作用是生成提交action对象的dispatch函数
    const dispatch = useDispatch()

    const addHandle = () => {
        dispatch(add(10))
    }

    const subHandle = () => {
        dispatch(sub(10))
    }
    const asyncHandle = () => {
        dispatch(fetchChannels())
    }

    return (
        <div className="App">
            <h1>count的值是:{count}</h1>
            <button onClick={addHandle}>+1</button>
            <button onClick={subHandle}>-1</button>
            <hr/>
            <button onClick={asyncHandle}>发送请求</button>
            <ul>
                {channels.map(item=><li key={item.id}>{item.name}</li>)}
            </ul>
        </div>
    );

}

export default App;
```

```JavaScript
import {configureStore} from "@reduxjs/toolkit";
import countReducer from "./modules/countStore";

const store = configureStore({
    reducer: {
        count: countReducer
    },
    devTools:true
})
export default store
```

```JavaScript
import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
/*
createAsyncThunk 是一个由 Redux Toolkit 提供的函数
用于创建处理异步操作的 thunk action creator。
使用 createAsyncThunk 可以简化 Redux 中处理异步操作的流程，使代码更加清晰、简洁。

const myAsyncThunk = createAsyncThunk(
   一个字符串类型的 action 名称，用于在 Redux 中识别该 action。
   该名称通常包含操作名称和状态
  "myAsyncOperationStatus",

   异步操作函数，该函数接收两个参数
    第一个参数是 thunk 的 payload，也就是调用的时候传过来的
    第二个参数是thunk对象
    dispatch
    用于 dispatch 其他 action 的函数
    getState
    用于获取当前 Redux store 中的 state 的函数
    extra
    性是用于传递额外参数的对象
  async (arg, { dispatch, getState, extra }) => {
    异步操作函数，必须返回一个 Promise
    const response = await fetch("https://example.com/api/someEndpoint");
    return response.json();
  },
  {} 可选的配置项);
*/

export const fetchChannels = createAsyncThunk("fetchChannels", async (dispatch, getState, extra) => {
    const resul = await fetch("http://geek.itheima.net/v1_0/channels")
    const {data: channels} = await resul.json()
    return channels
})

const countStore = createSlice({
    // 唯一标识符
    name: "countStore",
    // 初始化的状态
    initialState: {
        count: 0,
        channels: []
    },
    // 修改状态的方法(同步方法)
    reducers: {
        add(state, {payload}) {
            state.count += payload
        },
        sub(state, {payload}) {
            state.count -= payload
        },
        setChannels(state, {payload}) {
            state.channels = payload
        }
    },
    extraReducers(builder) {
        builder.addCase(fetchChannels.pending, (state, action) => {
            console.log("pending")
        }).addCase(fetchChannels.fulfilled, (state, action) => {
            state.channels = action.payload.channels
        }).addCase(fetchChannels.rejected, (state, action) => {
            console.log("rejected")
        })
    }
})

//  从countStore.actions解构出来actions函数
export const {add, sub} = countStore.actions

// 导出reducer
export default countStore.reducer
```
