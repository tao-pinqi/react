import React, {Component} from 'react'
import {CSSTransition} from "react-transition-group";
import "./App.css"
export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {flag: false}
    }

    changeFlag = () => {
        this.setState((state) => {
            return {flag: !state.flag}
        })
    }

    render() {
        const {flag} = this.state
        return (
            <div className="app">
                <button onClick={this.changeFlag} >点击</button>
                <CSSTransition in={flag} timeout={2000}  unmountOnExit={true}>
                  <h2>react动画</h2>
                </CSSTransition>
            </div>
        )
    }
}
