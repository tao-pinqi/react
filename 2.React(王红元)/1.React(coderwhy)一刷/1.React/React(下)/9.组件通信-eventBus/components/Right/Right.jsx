import React, { Component } from 'react'
import eventBus from '../../utils/eventBus'
export default class Right extends Component {
    constructor(props) {
        super(props)
        this.state = { count: 0 }
    }
    render() {
        const { count } = this.state
        return (
            <div>
                {count}
            </div>
        )
    }
    changeCount(count) {
        this.setState({ count: this.state.count += count })
    }
    componentDidMount() {
        // 组件挂载完毕使用 eventBus.on("事件名",callback)监听事件
        eventBus.on("changeCount", this.changeCount.bind(this))
    }
    componentWillUnmount() {
        // 组件即将卸载完毕使用 eventBus.off("事件名",要卸载的事件函数)卸载事件
        eventBus.off("changeCount", this.changeCount)
    }
}
