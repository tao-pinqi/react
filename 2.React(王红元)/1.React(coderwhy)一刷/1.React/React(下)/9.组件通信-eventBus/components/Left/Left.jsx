import React, { Component } from 'react'
import eventBus from '../../utils/eventBus'

export default class Left extends Component {
    render() {
        return (
            <div>
                <button onClick={this.clickHandle}>+1</button>
            </div>
        )
    }
    clickHandle() {
        //  使用eventBus.emit("事件名", 值)
        eventBus.emit("changeCount", 1)
    }

}
