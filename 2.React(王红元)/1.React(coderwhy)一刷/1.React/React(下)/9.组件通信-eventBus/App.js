import React, { Component } from 'react'
import Left from './components/Left/Left'
import Right from './components/Right/Right'

export default class App extends Component {
    render() {
        return (
            <div className='app'>
                <Left></Left>
                <Right></Right>
            </div>
        )
    }
}

