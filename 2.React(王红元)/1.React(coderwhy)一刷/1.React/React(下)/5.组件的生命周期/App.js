import React, { Component } from 'react'
import Header from './components/Header/Header'
import Aside from './components/Aside/Aside'
export default class App extends Component {
    render() {
        return (
            <div>
                <Header></Header>
                <Aside></Aside>
            </div>
        )
    }
}
