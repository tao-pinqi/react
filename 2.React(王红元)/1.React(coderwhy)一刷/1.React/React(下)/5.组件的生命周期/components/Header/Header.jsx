import React, { Component } from 'react'
export default class Header extends Component {
  constructor(props) {
    super(props)
    /* 
    *constructor()数据的初始化:
    *  接收props和context，当想在函数内使用这两个参数需要在super传入参数，
    *  当使用constructor时必须使用super，否则可能会有this的指向问题，如果不初始化state或者不进行方法绑定，则可以不为组件实现构造函数
    */
    console.log("Header的constructor")
  }
  componentDidMount() {
    /*
    * componentDidMount()在组件挂在后（插入到dom树中）后立即调用:
    *   可以在这里调用Ajax请求，返回的数据可以通过setState使组件重新渲染，或者添加订阅
    *   但是要在conponentWillUnmount中取消订阅
    */
    console.log("Header的componentDidMount")
  }
  render() {
    /*
    *render()class组件中唯一必须实现的方法:
    *  render函数会插入jsx生成的dom结构，react会生成一份虚拟dom树，在每一次组件更新时
    *  在此react会通过其diff算法比较更新前后的新旧DOM树，比较以后，找到最小的有差异的DOM节点，并重新渲染
    */
    console.log("Header的render")
    return (<div>Header组件</div>)
  }
  componentDidUpdate() {
    /*componentDidUpdate()该函数在组件更新之后执行，它是组件更新的最后一个环节*/
    console.log("Header的componentDidUpdate")
  }
  componentWillUnmount() {
    /* 
    *componentWillUnmount()在组件卸载和销毁之前调用
    *  在这执行必要的清理操作，例如，清除timer（setTimeout,setInterval），取消网络请求，或者取消在componentDidMount的订阅，移除所有监听
    */
    console.log("Header的componentWillUnmount")
  }
}
