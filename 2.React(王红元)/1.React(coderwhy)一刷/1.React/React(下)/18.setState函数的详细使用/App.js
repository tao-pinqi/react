import React, { Component } from 'react'


export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            message: "hello",
            count: 0
        }
    }
    render() {
        const { message, count } = this.state
        return (
            <div className='app'>
                <h1>count的值是：{count}</h1>
                <button onClick={this.addCount}>+</button>

                <h1>message的值是：{message}</h1>
                <button onClick={this.changeText}>修改文本</button>
            </div>
        )
    }
    addCount = () => {

    }
    changeText = () => {
        /*
         1.基本使用(传入对象)
            this.setState({ message: "李银河" })
        */

        /*
        2.传入回调函数返回一个对象
            this.setState((state, props) => {
                return {
                    message: "李银河"
                }
            })
        */

        /* 
        3.
         setState在React的事件处理中是一个异步调用
        如果希望在数据更新之后(合并)想要获取更新后的数据，可以在setState的第二个参数传入callback
        this.setState({ message: "李银河" }, () => {
            console.log(this.state.message);
        })
        console.log(this.state.message) // hello(更新之前的结果)
        */
       
    }
}

