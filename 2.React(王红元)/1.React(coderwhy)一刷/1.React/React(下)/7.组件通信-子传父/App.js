import React, { Component } from 'react'
import Children from './components/Children'

export default class App extends Component {
    constructor(props) {
        super(props)
        /* 父组件的数据value */
        this.state = { value: null }
    }
    render() {
        const { value } = this.state
        return (
            <div>
                {value}
                {/* 给子组件传递一个函数 */}
                <Children changeValue={this.changeValue}></Children>
            </div>
        )
    }
    /* 传递给子组件的函数接收子组件传递过来的数据value */
    changeValue = (value) => {
        /* 调用setState()更新父组件value属性 */
        this.setState(state => ({ value }))
    }
}
