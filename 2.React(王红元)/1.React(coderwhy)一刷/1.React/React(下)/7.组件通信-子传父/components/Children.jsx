import React, { Component } from 'react'

export default class Children extends Component {
  constructor(props) {
    super(props)
    this.state = { count: 0 }
  }
  render() {
    const { count } = this.state
    return (
      <div className='children'>
        {count}
        <button onClick={this.addCount}>+1</button>
      </div>
    )
  }
  addCount = () => {
    this.setState((state) => ({ count: state.count += 1 }), () => {
      /* 立即调用父组件传递过来函数，并且将值作为参数传递过去 */
      this.props.changeValue(this.state.count)
    })

  }
}
