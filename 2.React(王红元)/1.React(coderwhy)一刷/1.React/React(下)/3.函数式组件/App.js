
/* 
函数组件是使用function来定义的，函数组件中的render()返回值和类组件中的render()函数返回值是一样的内容
函数式组件在没有使用hooks的情况下有如下特点：
    1. 没有生命周期，也会被更新并且挂载，但是函数式组件没有生命周期
    2. this关键字不能指向组件实例(因为没有组件实例)
    3. 没有内部的状态(state)
*/
import react, { useState,useEffect } from "react"
export default function App(props) {
    const [info, setInfo] = useState("函数式组件")

    useEffect(()=>{
        console.log("组件挂载完毕");
    },[])

    const changeInfo = () => {
        setInfo("react")
    }

    return (
        <div className="app">
            <h1>{info}</h1>
            <button onClick={changeInfo}>修改文本</button>
        </div>
    )
}