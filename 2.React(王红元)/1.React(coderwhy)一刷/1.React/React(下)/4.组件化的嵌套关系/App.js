import React, {Component} from 'react';
import Header from "./components/Header/Header";
import Aside from "./components/Aside/Aside";
import Main from "./components/Main/Main";
import Footer from "./components/Footer/Footer";
import "./App.css"
class App extends Component {
    render() {
        return (
            <div className="app">
                {/* App组件的子组件 */}
                <Header></Header>
                <div className="content">
                {/* App组件的子组件 */}
                    <Aside></Aside>
                {/* App组件的子组件 */}
                    <Main></Main>
                </div>
                {/* App组件的子组件 */}
                <Footer></Footer>
            </div>
        );
    }
}

export default App;
