import React, { Component } from 'react'
/* 引入类型校验的库 */
import PropTypes from 'prop-types'
export default class Main extends Component {
    /* 类型的校验 */
    static propTypes = {
        // person必须是对象类型且为必填
        person: PropTypes.object.isRequired,
        hoby: PropTypes.string.isRequired
    }
    /*对props的值进行默认值的指定*/
    static defaultProps = {
            hoby:"???"
    }
    constructor(props) {
        super(props)
    }
    render() {
        const { person, hoby } = this.props
        return (
            <ul>
                <li>姓名是：{person.name}</li>
                <li>年龄是：{person.age}</li>
                <li>地址是：{person.address}</li>
                <li>爱好是：{hoby}</li>
                <li>ID是：{person.id}</li>
            </ul>
        )
    }
}
