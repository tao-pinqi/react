import React, { Component } from 'react'
import Main from './components/Main/Main'
/* 
props是组件（包括函数组件和class组件）间的内置属性，其作用是可以传递数据给子组件
state和props主要的区别在于props是不可变的(props遵循单向数据流)，而state可以根据与用户交互来改变
理解props：每个组件对象都会有props属性，组件标签的所有属性都保存在props中
props的作用：通过标签属性从组件外向组件内传递变化的数据
组件的props是只读属性，不能直接对prop的值进行修改
*/
export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            persons: [
                { id: 1, name: "马云", age: 50, address: "杭州" },
                { id: 2, name: "马化腾", age: 49, address: "深圳" },
                { id: 3, name: "马保国", age: 50, address: "广州" },
            ],
            hoby: "喝酒"
        }
    }
    render() {
        const { persons, hoby } = this.state
        return (
            <div className='app'>
                {
                    persons.map(item => <Main key={item.id} person={item}></Main>)
                }
            </div>
        )
    }
}
