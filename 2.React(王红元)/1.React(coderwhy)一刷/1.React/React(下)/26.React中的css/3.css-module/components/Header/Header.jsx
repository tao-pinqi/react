import React, {Component} from 'react';
/* 引入模块化文件的css */
import HeaderStyle from "./Header.module.css"

class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={HeaderStyle.header}>
                <div>我是头部组件</div>
            </div>
        );
    }
}

export default Header;
