import React, {Component} from 'react';
import Header from "./components/Header/Header";

class App extends Component {
    render() {
        return (
            <div className="app">
                <Header></Header>
            </div>
        );
    }
}

export default App;
