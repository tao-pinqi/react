import React, {Component} from 'react';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            width: 100,
            height: 30,
            backgroundColor: "red"
        }
    }

    render() {
        const {width, height, backgroundColor} = this.state
        return (
            <div className="header">
                {/*内联的css样式*/}
                <div style={{width: width + "px", height: height + "px", backgroundColor, display: "flex", justifyContent: "center", alignItems: "center"}}>我是头部组件</div>
                <button onClick={this.changeHeight}>增加高度</button>
            </div>
        );
    }

    changeHeight = () => {
        this.setState((state => {
            return {
                height: state.height += 10
            }
        }))
    }
}

export default Header;
