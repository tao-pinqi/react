import React, { Component } from 'react'
import Tab from './components/Tab/Tab'


export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            navList: ["流行", "新款", "精选"],
            index: 0
        }
    }
    render() {
        const { navList, index } = this.state
        return (
            <div>
                <Tab navList={navList} getIndex={this.getIndex}></Tab>
                <div style={{textAlign:"center"}}>{navList[index]}</div>
            </div>
        )
    }
    getIndex = (index) => {
        this.setState({index})
    }
}
