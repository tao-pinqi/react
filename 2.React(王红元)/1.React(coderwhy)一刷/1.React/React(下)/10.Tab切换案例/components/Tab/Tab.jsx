import React, { Component } from 'react'
import "./Tab.css"
export default class Tab extends Component {
    constructor(props) {
        super(props)
        this.state = { currentIndex: 0 }
    }
    render() {
        const { navList } = this.props
        const { currentIndex } = this.state
        return (
            <ul>
                {
                    navList.map((item, index) => <li key={index}><span className={currentIndex === index ? "active" : ""} onClick={this.changeIndex(index)}>{item}</span></li>)
                }
            </ul>
        )
       
        
    }
    changeIndex=(index)=>{
        return ()=>{
            this.setState({currentIndex:index},()=>{
                this.props.getIndex(index)
            })
        }
    }
}
