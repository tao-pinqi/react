import React, { Component } from 'react'
import pubSub from "pubsub-js"

export default class Right extends Component {
    constructor(props) {
        super(props)
        this.state = { count: 0 }
    }
    render() {
        const { count } = this.state
        return (
            <div>
                {count}
            </div>
        )
    }
    componentDidMount() {
        // 订阅消息 消息名：publish_value 第二个参数是一个函数
        // 此函数又有两个参数：消息名和消息数据
        this.token = pubSub.subscribe("publish_value", (msg, count) => {
            this.setState({ count })
        })
    }
    componentWillUnmount() {
        // 组件快要卸载的时候取消订阅消息
        pubSub.unsubscribe(this.token)
    }
}
