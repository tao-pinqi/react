import React, { Component } from 'react'
import pubSub from "pubsub-js"

export default class Left extends Component {
    render() {
        return (
            <div>
                <button onClick={this.addCount}>+1</button>
            </div>
        )
    }
    addCount = () => {
        // publish发布消息 消息名为：publish_value 内容为：1
        pubSub.publish("publish_value", 1)
    }
}
