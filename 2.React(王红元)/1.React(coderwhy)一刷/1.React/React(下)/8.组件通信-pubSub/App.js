import React, { Component } from 'react'
import Left from './components/Left/Left'
import Right from './components/Right/Right'

export default class App extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <div>
                <Left></Left>
                <Right></Right>
            </div>
        )
    }
}
