import React, { Component } from 'react'
import Header from './components/Header/Header'
import Main from './components/Main/Main'
import Footer from './components/Footer/Footer'
import "./App.css"
export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            todoList: [
                { id: "1", name: "吃饭", done: true },
                { id: "2", name: "睡觉", done: true },
                { id: "3", name: "打豆豆", done: true },
            ]
        }
    }
    render() {
        const { todoList } = this.state
        return (
            <div className='app layui-panel'>
                {/* 头部组件 */}
                <Header></Header>
                {/* 主体组件 */}
                <Main todoList={todoList}></Main>
                {/* 底部组件 */}
                <Footer></Footer>
            </div>
        )
    }
}
