import React, { Component } from 'react'
import "./Footer.css"
export default class Footer extends Component {
    render() {
        return (
            <div className='footer'>
                <div className='left'>
                    <input type="checkbox" />
                    <span>已完成0 / 全部0</span>
                </div>
                <div className='right'>
                    <button type="button" className="layui-btn layui-bg-blue layui-btn-sm">清除已完成任务</button>
                </div>
            </div>
        )
    }
}
