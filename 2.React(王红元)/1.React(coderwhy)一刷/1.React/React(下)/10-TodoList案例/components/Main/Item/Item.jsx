import React, { Component } from 'react'
import PropTypes from 'prop-types';
import "./Item.css"
export default class Item extends Component {
    static propTypes = {
        todo: PropTypes.object.isRequired
    }
    render() {
        const { todo } = this.props
        return (
            <div className='item'>
                <span className='left'>
                    <input type="checkbox" checked={todo.done} onChange={this.changeTodoDone} />
                    <span className='text'>{todo.name}</span>
                </span>
                <button className="layui-btn  layui-bg-red layui-btn-sm">删除</button>
            </div>
        )
    }

    changeTodoDone = () => {

    }
}
