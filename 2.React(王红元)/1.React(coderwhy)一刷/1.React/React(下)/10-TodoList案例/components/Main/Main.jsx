import React, { Component } from 'react'
import PropTypes from 'prop-types';
import Item from './Item/Item'
import "./Main.css"
export default class Main extends Component {
    static propTypes = {
        todoList: PropTypes.array.isRequired
    }
    render() {
        const { todoList } = this.props
        return (
            <div className="main">
                {
                    todoList.map(todo => <Item key={todo.id} todo={todo}></Item>)
                }
            </div>
        )
    }
}
