import React, { Component } from 'react'
import "./Header.css"
export default class Header extends Component {
    inputRef = React.createRef()
    render() {
        return (
            <div className='header'>
                <input type="text" name="" placeholder="请输入任务的名称" className="layui-input" ref={this.inputRef} />
                <button type="button" className="layui-btn layui-btn layui-btn-sm layui-icon-addition layui-icon" onClick={this.clickHandle}>添加任务</button>
            </div>
        )
    }
    clickHandle = () => {
        const { value } = this.inputRef.current

    }
}
