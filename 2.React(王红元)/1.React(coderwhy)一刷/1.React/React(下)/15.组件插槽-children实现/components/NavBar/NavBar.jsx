import React, { Component } from 'react'
import "./NavBar.css"
export default class NavBar extends Component {
    render() {
        /* children实现插槽 */
        const [back, input, fowrk] = this.props.children
        return (
            <div className='navbar'>
                <div className="left">{back}</div>
                <div className="center">{input}</div>
                <div className="right">{fowrk}</div>
            </div>
        )
    }
}
