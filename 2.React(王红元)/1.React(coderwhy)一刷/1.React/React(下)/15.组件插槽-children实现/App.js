import React, { Component } from 'react'
import NavBar from './components/NavBar/NavBar'

export default class App extends Component {
    render() {
        return (
            <div className='app'>
                <NavBar>
                    {/* 在组件标签中书写正常的html结构，此时子组件的children属性中会存储父组件中书写的结构(children可能是数组也可能是对象) */}
                    <button>返回</button>
                    <input type='text' placeholder='请输入关键字' />
                    <button>前进</button>
                </NavBar>
            </div>
        )
    }
}
