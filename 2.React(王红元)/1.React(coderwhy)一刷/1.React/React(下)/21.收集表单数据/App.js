import React, { Component } from 'react'


export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: "",
            password: "",
            message: "",
            done: true,
            hobby: [
                { value: "吃饭", text: "吃饭", isChecked: true },
                { value: "睡觉", text: "睡觉", isChecked: true },
                { value: "打豆豆", text: "打豆豆", isChecked: false },
            ],
            address: "北京"
        }
    }
    render() {
        const { userName, password, message, done, hobby, address } = this.state
        return (
            <div>
                <form>
                    <div>用户名：<input type='text' name='userName' value={userName} onChange={this.handleInput} /></div>
                    <div>密码：<input type='password' name='password' value={password} onChange={this.handleInput} /></div>
                    <div>意见箱：<textarea type='textarea' name='message' value={message} onChange={this.handleInput} /></div>
                    <div>同意协议：<input type='checkbox' checked={done} name='done' onChange={this.handleAgree} /></div>
                    <div>爱好：
                        {
                            hobby.map((item, index) => {
                                return (<label key={item.value}>{item.text}
                                    <input type='checkbox'
                                        value={item.value}
                                        checked={item.isChecked}
                                        onChange={(e) => this.changeHobby(e, index)} /></label>
                                )
                            })
                        }
                    </div>
                    <select value={address} onChange={this.changeAddress}>
                        <option value='北京'>北京</option>
                        <option value='上海'>上海</option>
                        <option value='广州'>广州</option>
                    </select>
                </form>
            </div>
        )
    }
    handleInput = (event) => {
        const { name, value, checked } = event.target
        this.setState({ [name]: value })
    }
    handleAgree = (event) => {
        const { checked } = event.target
        this.setState({ done: checked })
    }
    changeHobby = (event, index) => {
        const { hobby } = this.state
        const { checked } = event.target
        let newHobby = [...hobby]
        newHobby[index].isChecked = checked
        this.setState({ hobby: newHobby })
    }
    changeAddress = (event) => {
        const { value } = event.target
        this.setState({ address: value })
    }
}
