import React, { Component } from 'react'
import { createPortal } from "react-dom"

export default class App extends Component {
    render() {
        return (
            <div>
                <h1>App组件</h1>
                {
                    createPortal(<h1>hello</h1>,document.querySelector("body"))
                }
            </div>
        )
    }
}
