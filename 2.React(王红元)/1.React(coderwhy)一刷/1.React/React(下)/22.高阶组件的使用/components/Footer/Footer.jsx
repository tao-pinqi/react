import React, {Component} from 'react';
import withTheme from "../../hoc/withTheme";
class Footer extends Component {
    render() {
        const {color}=this.props

        return (
            <div>
                <div style={{color}}>footer</div>
            </div>
        );
    }
}

export default withTheme(Footer);
