import React, {Component} from 'react';
import withTheme from "../../hoc/withTheme";

class Main extends Component {
    render() {
        const {color}=this.props
        return (
            <div style={{color}}>main</div>
        );
    }
}

export default withTheme(Main);
