import React, {Component} from 'react';
import hocHeader from "../../hoc/hocHeader";

class Header extends Component {
    render() {
        const {name, age, address} = this.props
        return (
            <div>
                <ul>
                    <li>{name}</li>
                    <li>{age}</li>
                    <li>{address}</li>
                </ul>
            </div>
        )
    }
}

export default hocHeader(Header);
