import React, { Component, PureComponent } from 'react';
import Header from "./components/Header/Header";
import Main from "./components/Main/Main";
import Footer from "./components/Footer/Footer";
import themeContext from "./context/themeContext";

class App extends Component {
    constructor(props) {
        super(props)
        this.state = { color: "#c00000" }
    }

    render() {
        const { color } = this.state
        return (
            <div>
                <themeContext.Provider value={{ color }}>
                    <Header></Header>
                    <Main></Main>
                    <Footer></Footer>
                    <button onClick={this.changeColor}>修改颜色</button>
                </themeContext.Provider>
            </div>
        );
    }

    changeColor = () => {
        this.setState({ color: "#000000" })
    }
}

export default App;
