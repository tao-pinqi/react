import {PureComponent} from "react";
/* 
props的增强
*/
function hocHeader(Component) {
    /*定义一个高阶组件：参数为组件，并且返回一个新组件*/
    // Component参数：一个组件
    return class Hoc extends PureComponent {
        constructor(props) {
            super(props)
            this.state = {
                userInfo: {name: "陶品奇", age: 22, address: "衡阳"}
            }
        }
        render() {
            // 返回一个新的组件，并且这个组件携带name属性的props
            return (<Component {...this.state.userInfo} {...this.props}></Component>)
        }
    }
}

export default hocHeader
