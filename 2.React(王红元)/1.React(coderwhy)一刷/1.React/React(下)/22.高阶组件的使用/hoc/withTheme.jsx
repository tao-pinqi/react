import {PureComponent} from "react";
import themeContext from "../context/themeContext";
/* 
context共享
*/
function withTheme(Component) {
    return class newComponent extends PureComponent {
        render() {
            return (
                <themeContext.Consumer>
                    {value => <Component {...value} {...this.props}></Component>}
                </themeContext.Consumer>
            )
        }
    }
}

withTheme.contextType = themeContext

export default withTheme
