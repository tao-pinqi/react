import React, { Component } from 'react'
/* 
## 受控组件
在HTML中，标签<input>、<textarea>、<select>的值的改变通常是根据用户输入进行更新。
在React中，可变状态通常保存在组件的状态属性中，并且只能使用 setState()更新
而呈现表单的React组件也控制着在后续用户输入时该表单中发生的情况，以这种由React控制的输入表单元素而改变其值的方式，称为：“受控组件”
*/
export default class Header extends Component {
  constructor(props) {
    super(props)
    this.state = { username: "陶品奇" }
  }
  render() {
    const { username } = this.state
    return (
      <div>
        <input type="text" value={username} onChange={this.changeUserName} />
        <span>{username}</span>
      </div>
    )
  }
  changeUserName = (event) => {
    const { value } = event.target
    this.setState({ username: value })
  }
}
