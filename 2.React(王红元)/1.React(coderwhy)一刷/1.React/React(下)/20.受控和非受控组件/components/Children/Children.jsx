import React, { Component } from 'react'
/* 
## 非受控组件
表单数据由DOM本身处理。即不受setState()的控制，与传统的HTML表单输入相似，input输入值即显示最新值（使用 `ref`从DOM获取表单值）
*/
export default class Children extends Component {
  constructor(props) {
    super(props)
    this.state = { username: "" }
  }
  render() {
    const { username } = this.state
    return (
      <div>
        <input type="text" onChange={this.changeUserName} />
        <span>{username}</span>
      </div>
    )
  }
  changeUserName = (event) => {
    const { value } = event.target
    this.setState({ username: value })
  }
}
