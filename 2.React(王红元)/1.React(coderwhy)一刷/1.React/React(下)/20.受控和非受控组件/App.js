import React, { Component } from 'react'
import Children from './components/Children/Children'
import Header from './components/Header/Header'

export default class App extends Component {
    render() {
        return (
            <div>
                <Header></Header>
                <Children></Children>
            </div>
        )
    }
}
