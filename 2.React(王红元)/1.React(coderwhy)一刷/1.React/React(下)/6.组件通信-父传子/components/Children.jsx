import React, { Component } from 'react'

export default class Children extends Component {
  render() {
    // 父组件传递的props自定义属性都会在组件实例的props对象身上
    console.log(this.props);
    const {count}=this.props
    return (
      <div>{count}</div>
    )
  }
}
