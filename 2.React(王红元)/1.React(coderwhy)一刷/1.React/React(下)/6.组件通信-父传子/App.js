import React, { Component } from 'react'
import Children from './components/Children'
export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = { count: 1 ,name:"陶品奇",age:22}
    }
    render() {
        const {state}=this
        return (
            <div>
             {/* 使用props自定义属性将父组件的值传递给子组件（单个传递props）
             <Children count={count} ></Children>*/}
              
              {/* 使用props自定义属性将父组件的值传递给子组件（批量传递props） */}
              <Children {...state}></Children>
                <button onClick={this.addCount}>+1</button>
            </div>
        )
    }

    addCount=()=>{
        // 更新父组件的state，子组件用到了父组件传递过去的props，当父组件更新数据时子组件也会被更新
        this.setState((state)=>({count:state.count+=1}))
    }
}
