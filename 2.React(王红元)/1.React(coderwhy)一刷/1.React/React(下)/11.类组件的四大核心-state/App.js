import React, { Component } from 'react'
import Main from './components/Main/Main'
/* 
state是组件对象最重要的属性，值是对象(可以包含多个键值对的集合)
组件被称为状态机，通过更新组件的state来更新页面的显示(重新渲染组件)
组件中render函数中的this指向的是组件实例对象
组件的自定义方法中this的指向为undefined，如何解决：1.通过bind函数绑定this、2.类的实例方法、3.箭头函数
state状态不能直接修改需要调用setState()
*/
export default class App extends Component {
    state = { message: "react" }
    render() {
        const { message } = this.state
        return (
            <div className='app'>
                <div>{message}</div>
                <Main></Main>
            </div>
        )
    }
}
