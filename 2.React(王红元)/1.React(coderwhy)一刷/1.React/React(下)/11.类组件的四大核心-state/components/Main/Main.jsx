import React, { Component } from 'react'

export default class Main extends Component {
    constructor(props) {
        super(props)
        this.state = {
            count: 1
        }
    }
    render() {
        const { count } = this.state
        return (
            <div>
                <div>{count}</div>
                <button onClick={this.addCount}>+1</button>
            </div>
        )
    }
    addCount = () => {
        this.setState({
            count: this.state.count += 1
        })
    }
}
