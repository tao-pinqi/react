import React, { Component } from 'react'
import NavBar from './components/NavBar/NavBar'

export default class App extends Component {
    render() {
        return (
            <div className='app'>
                {/* props实现插槽，使用props传递一个html元素 */}
                <NavBar
                    leftSlot={<button>后退</button>}
                    centerSlot={<input placeholder='关键字' />}
                    rightSlot={<button>前进</button>}>
                </NavBar>
            </div>
        )
    }
}
