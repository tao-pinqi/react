import React, { Component } from 'react'
import "./NavBar.css"
export default class NavBar extends Component {
    render() {
        /* 接收父组件传递过来的html元素 */
        const { leftSlot, centerSlot, rightSlot } = this.props
        return (
            <div className='navbar'>
                <div className="left">{leftSlot}</div>
                <div className="center">{centerSlot}</div>
                <div className="right">{rightSlot}</div>
            </div>
        )
    }
}
