import React, { Component, PureComponent } from 'react'

export default class Children extends PureComponent {
    render() {
        console.log("Children组件的render");
        return (
            <div>Children组件</div>
        )
    }
}
