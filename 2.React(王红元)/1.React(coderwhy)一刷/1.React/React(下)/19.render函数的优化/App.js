import React, { Component,PureComponent } from 'react'
import Children from './components/Children/Children';
import Header from './components/Header/Header';


export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = { count: 0 }
    }
    render() {
        const { message, count } = this.state
        console.log("App组件的render");
        return (
            <div className='app'>
                <h1>count的值是：{count}</h1>
                <button onClick={this.addCount}>+1</button>
                <Children></Children>
                <Header></Header>
            </div>
        )
    }
    addCount = () => {
        this.setState((state) => {
            return {
                count: state.count += 1
            }
        })
    }

}

