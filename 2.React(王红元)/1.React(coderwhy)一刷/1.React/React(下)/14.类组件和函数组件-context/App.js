import React, { Component } from 'react'
import Children from './components/Children/Children'
/* 引入创建好的context */
import ThemeContext from './context/ThemeContext'

export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = { color: "#c00000" }
    }
    render() {
        const { color } = this.state
        return (
            <div className='app'>
                {/* 使用context中的Provider的value属性提供数据 */}
                <ThemeContext.Provider value={{ color }}>
                    <Children></Children>
                </ThemeContext.Provider>
            </div>
        )
    }
}
