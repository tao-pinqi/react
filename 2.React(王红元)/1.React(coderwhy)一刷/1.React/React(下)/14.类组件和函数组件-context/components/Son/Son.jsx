import React, { Component } from 'react'
/*引入创建好的context */
import ThemeContext from '../../context/ThemeContext';
import Sun from "../Sun/Sun";
class Son extends Component {

    render() {
        const { color } = this.context
        return (
            <div>
                <span style={{ color }}>context</span>
                <Sun></Sun>
            </div>
        )
    }
}
/*设置组件的contextType类型为某一个context */
Son.contextType = ThemeContext
export default Son
