import React, { Component } from 'react';
import ThemeContext from "../../context/ThemeContext";
class Sun extends Component {
    render() {
        return (
            <div>
                <ThemeContext.Consumer>
                    {value => <span style={{color:value.color}}>context</span>}
                </ThemeContext.Consumer>
            </div>
        );
    }
}

Sun.contextType = ThemeContext
export default Sun;
