import React, { Component } from 'react'
import Son from '../Son/Son'

export default class Children extends Component {
    render() {
        return (
            <div>
                <Son></Son>
            </div>
        )
    }
}
