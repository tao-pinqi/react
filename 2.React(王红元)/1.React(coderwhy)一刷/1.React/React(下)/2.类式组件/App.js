import React, {Component} from 'react';
/*
*一：类组件的定义必须有如下要求：
*       组件的名称必须是大写字母开头、类组件必须继承React.Component、类组件必须实现render方法
*
*二：使用class定义一个组件
*       1.constructor方法是可选的
*       2.this.state中维护的就是组件内部的数据
*       3.render()方法是class组件中必须实现的方法
*
*三：render()函数的返回值  
*       1.react元素
        2.组件或者fragments
        3.Protals
        4.字符串/数字类型
        5.布尔类型、null、undefined(不会进行显示)
* */
class App extends Component {
    constructor(props) {
        super(props)
        this.state = {info: "类组件"}
    }

    render() {
        const {info} = this.state
        return (
            <div className="app"><h1>{info}</h1></div>
        );
    }
}

export default App;
