import React, {Component} from 'react';
import Header from "./components/Header/Header";
import Aside from "./components/Aside/Aside";
import Main from "./components/Main/Main";
import Footer from "./components/Footer/Footer";
import "./App.css"

/*
*一：组件化开发：
*   1.一个页面按照功能进行一层层的划分，直到划分到最小位置，随后在将拆分开来的组件依据他们之间的相互关系使他们有机的整合在一起
*   2.一般情况下，react中的根组件是App组件，其余组件都是App组件的子组件
*二：React的组件相对于Vue更加的灵活和多样，按照不同的方式可以分成很多类组件：
*   1.根据组件的定义方式可以分为：函数组件和类组件
*   2.根据组件内部是否有状态需要维护可以分为：有状态组件和无状态组件
*   3.根据组件得到不同职责可以分为：展示型组件和容器型组件
*/
class App extends Component {
    render() {
        return (
            <div className="app">
                <Header></Header>
                <div className="content">
                    <Aside></Aside>
                    <Main></Main>
                </div>
                <Footer></Footer>
            </div>
        );
    }
}

export default App;
