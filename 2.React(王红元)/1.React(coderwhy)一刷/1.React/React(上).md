# React介绍和特点

## React是什么

React是用于构建用户界面的JavaScript库

## React特点

> React采用声明范式可以轻松描述应用。通过虚拟DOM,最大限度的减少与DOM的交互，速度很快
>
> 使用JSX,代码可读性很好，React可以与已有的库或者框架很好的配合
>
> 使用React构建组件使代码更加容易得到复用，可以很好的应用在大项目的开发过程中
>
> 单向数据流减少了重复的代码，轻松实现数据绑定



# React开发依赖

开发React必须依赖三个库：react、react-dom、babel

> react：包含React所需要的核心代码
>
> react-dom：react渲染在不同平台所需要的核心代码
>
> babel：将JSX语法转换成React代码的工具



# React初体验

## createRoot()

createRoot(domNode，options?)

React将会为domNode创建一个根节点，并控制其中的DOM,需要调用 root.render()来显示React组件

## root.render()

调用root.render以将一段JSX（“React 节点”）在 React的根节点中渲染为 DOM 节点并显示。

## root.unmount()

调用 root.unmount 以销毁 React 根节点中的一个已经渲染的树

```JavaScript
<body>
<div id="root"></div>
<!--引入react核心包-->
<script src="../../javascript/react.js"></script>
<!--引入react-dom包-->
<script src="../../javascript/react-dom.js"></script>
<!--引入babel包-->
<script src="../../javascript/babel.js"></script>
<script type="text/babel">
    /*
    * 一：createRoot(domNode，options?)
    *   允许在浏览器的DOM节点中创建根节点以显示React组件
    *   React将会为domNode创建一个根节点，并控制其中的DOM。在已经创建根节点之后，需要调用 root.render()来显示React组件
    * 二：root.render(reactNode)
    *   调用root.render以将一段JSX（“React 节点”）在 React的根节点中渲染为 DOM 节点并显示。
    *三：root.unmount()
    *   调用 root.unmount 以销毁 React 根节点中的一个已经渲染的树
    * */
    class App extends React.Component {
        constructor(props) {
            super(props)
            // constructor方法this指向为App组件实例
            console.log(this)
            //组件的数据
            this.state={
                text:"hello-react"
            }
        }

        render() {
            // render方法this指向为App组件实例
            console.log(this)
            const {text}=this.state
            return (
                <div>
                    <h1>{text}</h1>
                    <button onClick={()=>this.changeText()}>切换文本</button>
                </div>
            );
        }

        changeText() {
            // 事件绑定里的方法默认指向的为undefined(类里面的方法开启了严格模式+babel的存在)
            this.setState({
                text:"李银河"
            })
        }
    }
    // 创建一个根节点
    const root = ReactDOM.createRoot(document.querySelector("#root"))
    // 渲染App组件
    root.render(<App/>)
    // 销毁组件
    // root.unmount()
</script>
</body>
```

## 电影列表案例

```JavaScript
class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {movies: [1, 2, 3]}
    }

    render() {
        const {movies} = this.state
        // 映射函数
        const renderMovies = () => movies.map(item => <li key={item}>{item}</li>)
        return (
            <div>
                <ul>
                {/*
                   实现电影案例的第二种方式
                  {movies.map(item => <li key={item}>{item}</li>)}
                */}
                    
                  {/* 实现电影案例的第二种方式 */}
                  {renderMovies()}
                </ul>
                <button onClick={this.pushElement}>push一个元素</button>
            </div>
        );
    }

    pushElement = () => {
        this.setState((state) => {
            return {
                movies: [Math.random(), ...state.movies]
            }
        })
    }
}

const root = ReactDOM.createRoot(document.querySelector("#root"))
root.render(<App/>)
```

## 计数器案例

```JavaScript
    class App extends React.Component {
        constructor(props) {
            super(props)
            this.state={count:0}
        }

        render() {
            const {count}=this.state
            return (
                <div>
                <h1>当前计数为:{count}</h1>
                 <button onClick={this.changeCount(1)}>+1</button>
                 <button onClick={this.changeCount(-1)}>-1</button>
                </div>
            );
        }

        changeCount=(value)=>{
           return ()=>{
            /* 
            this.setState()可以传入两个参数：第一个参数：对象或者是回调函数，第二个参数：回调函数
            */
            this.setState(state=>({count:state.count+value}),()=>{
                console.log(this.state.count);
            })
           }
        }

    }
    const root = ReactDOM.createRoot(document.querySelector("#root"))
    root.render(<App/>)
```



# JSX基本语法

React 使用 JSX 来替代常规的 JavaScript，JSX 是一个看起来很像 XML 的 JavaScript 语法扩展

```JavaScript
class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        {/*返回一个JSX元素*/}
        return <h1>hello-JSX语法</h1>
    }
}

const root = ReactDOM.createRoot(document.querySelector("#root"))
root.render(<App/>)
```

## JSX的书写规范

> JSX的书写规范 ；
>
> ​	1. JSX结构中只能返回一个根元素
>
> ​	2. JSX的外层通常包裹一个小括号，将整个JSX当做一个整体
>
> ​	3. JSX中标签可以是单标签和双标签(单标签必须以 / 自闭合)
>
> ​        4. JSX中的注释必须是 {/* jsx的注释 */}

```JavaScript
/*
* JSX的书写规范 ；
*   1.JSX结构中只能返回一个根元素
*   2.JSX的外层通常包裹一个小括号，将整个JSX当做一个整体
*   3.JSX中标签可以是单标签和双标签(单标签必须以 / 自闭合)
* */
class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return(
            <div className="app">
                <input/>
            </div>
        )
    }
}

const root = ReactDOM.createRoot(document.querySelector("#root"))
root.render(<App/>)
```

## JSX插入不同的内容

情况一：当变量是Number、String、Array的类型时，可以直接显示

情况二：当变量是null、undefined、Boolean类型时，内容为空(如果希望显示可以转为字符串)

情况三：Object对象类型不能作为子元素插入到JSX的结构中

```JavaScript
    class App extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                count: 0,
                message: "字符串",
                array: [1, 2, 3, 4, 5],
                aaa: null,
                bbb: undefined,
                ccc: true,
                object: {name: "ATO"},
                firstName: "陶",
                lastName: "品奇"
            }
        }

        render() {
            const {count, message, array} = this.state
            const {aaa, bbb, ccc} = this.state
            const {object} = this.state
            const fullName = () => this.state.firstName + '' + this.state.lastName
            const result = this.state.count + this.state.message
            return (
                <div className="app">
                    {/*Number/String/Array可以直接显示*/}
                    <h2>{count}</h2>
                    <h2>{message}</h2>
                    <h2>{array}</h2>
                    {/*null/undefined/Boolean显示为空*/}
                    <h2>{aaa + ''}</h2>
                    <h2>{bbb}</h2>
                    <h2>{ccc}</h2>
                    {/*Object类型不能作为内容插入*/}
                    {<h2>{object.name}</h2>}

                    {/*插入不同的表达式*/}
                    <h2>{10 + 20}</h2>
                    <h2>{ccc ? "成年" : "未成年"}</h2>
                    <h2>{fullName()}</h2>
                    <h2>{result}</h2>
                </div>
            )
        }
    }

    const root = ReactDOM.createRoot(document.querySelector("#root"))
    root.render(<App/>)
```

## JSX中绑定基本属性

```JavaScript
    class App extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                count: 0,
                classname: "header",
                isActive: true
            }
        }

        render() {
            const {count, isActive} = this.state
            const className = `box1 box2 ${isActive ? 'active' : ''}`
            const classList = ["1", "2", "3"]
            if (isActive) classList.push("active")

            const styleObj={backgroundColor: "red"}
        
            return (
                <div className="app">
                    {/*绑定基本的属性*/}
                    <h1 title={count}>绑定title属性</h1>
                    {/*绑定class对象写法*/}
                    <div className={className}>绑定class对象写法</div>
                    {/*绑定class数组写法*/}
                    <div className={classList.join(" ")}>绑定class数组写法</div>
                    {/*绑定style对象写法*/}
                    <div style={styleObj}>绑定style对象写法</div>
                </div>
            )
        }
    }

    const root = ReactDOM.createRoot(document.querySelector("#root"))
    root.render(<App/>)
```



# 事件绑定

React 事件绑定属性的命名采用驼峰式写法，而不是小写，例如原生的事件全是小写 onclick，React 里的事件是驼峰 onClick

如果采用 JSX 的语法需要传入一个函数作为事件处理函数，而不是一个字符串(DOM 元素的写法)

React的事件并不是原生事件，而是合成事件

在 React 中另一个不同是你不能使用返回 false 的方式阻止默认行为， 你必须明确的使用 e.preventDefault()

> 当使用 ES6 class 语法来定义一个组件的时候，事件处理器会成为类的一个方法
>
> 类的方法默认是不会绑定 this 的。如果忘记绑定 this.handleClick 并把它传入 onClick, 当调用这个函数的时候 this 的值会是 undefined

## bind绑定this

在 render() 方法中，使用 bind 绑定 this(不推荐)。直接在组件内定义一个非箭头函数的方法，然后在render里直接使用`onClick={this.handleClick.bind(this)}`，这种方式的缺点是，每次都使用 bind 绑定 this，代码会冗余

```JavaScript
class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {count: 1}
        {/*bind函数绑定this绑定的第二形式(在constructor(函数中直接绑定)*/}
        this.subCount = this.subCount.bind(this)
    }

    render() {
        const {count} = this.state
        return (
            <div className="app">
                <h1>count的值是:{count}</h1>
                {/*bind函数绑定this绑定的第一形式(在render函数中直接绑定)*/}
                <button onClick={this.addCount.bind(this)}>+1</button>
                <button onClick={this.subCount}>-1</button>
            </div>
        );
    }

    addCount() {
        this.setState({count: this.state.count += 1})
    }

    subCount() {
        this.setState({count: this.state.count -= 1})
    }
}

const root = ReactDOM.createRoot(document.querySelector("#root"))
root.render(<App/>)
```

## classfields语法

使用属性初始化器来正确的绑定回调函数(推荐)，在组件内使用箭头函数定义一个方法，这种方式的缺点是不能自定义传参

```JavaScript
class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {count: 1}

    }

    render() {
        const {count} = this.state
        return (
            <div className="app">
                <h1>count的值是:{count}</h1>
                <button onClick={this.addCount}>+1</button>
                <button onClick={this.subCount}>-1</button>
            </div>
        );
    }

    /*使用ES6的classFields语法*/
    addCount = () => {
        this.setState({count: this.state.count += 1})
    }

    subCount = () => {
        this.setState({count: this.state.count -= 1})
    }
}

const root = ReactDOM.createRoot(document.querySelector("#root"))
root.render(<App/>)
```

## 箭头函数

在回调函数中使用箭头函数，直接在 render() 里写行内的箭头函数(不推荐)，这种写法存在的问题就是，当每次执行render()的时候都会创建一个不同的回调函数

在大多数情况下，这没有问题。然而如果这个回调函数作为一个属性值传入低阶组件，这些组件可能会进行额外的重新渲染。

```JavaScript
class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {count: 1}

    }

    render() {
        const {count} = this.state
        return (
            <div className="app">
                <h1>count的值是:{count}</h1>
                {/*给事件处理函数传递箭头函数*/}
                <button onClick={()=>this.addCount()}>+1</button>
                {/*给事件处理函数传递箭头函数*/}
                <button onClick={()=>this.subCount()}>-1</button>
            </div>
        );
    }

    addCount ()  {
        this.setState({count: this.state.count += 1})
    }

    subCount  (){
        this.setState({count: this.state.count -= 1})
    }
}

const root = ReactDOM.createRoot(document.querySelector("#root"))
root.render(<App/>)
```

# 事件传参

和普通浏览器一样，事件 handleClick 会被自动传入一个 event 对象，这个对象和普通的浏览器 event 对象所包含的方法和属性都基本一致

不同的是 React中的 event 对象并不是浏览器提供的，而是它自己内部所构建的。它同样具有event.stopPropagation、event.preventDefault 这种常用的方法

## 传递事件对象

```JavaScript
    /*
    * 事件 handleClick 会被自动传入一个 event 对象，这个对象和普通的浏览器 event 对象所包含的方法和属性都基本一致
    *不同的是 React中的 event 对象并不是浏览器提供的，而是它自己内部所构建的。它同样具有event.stopPropagation、event.preventDefault这种常用的方法
    * */
    class App extends React.Component {
        constructor(props) {
            super(props)
            this.clickHandle4 = this.clickHandle4.bind(this)
        }

        render() {
            return (
                <div className="app">
                    {/*普通绑定事件处理函数*/}
                    <button onClick={this.clickHandle1}>点击</button>
                    {/*箭头函数+赋值语句绑定事件处理函数*/}
                    <button onClick={this.clickHandle2}>点击</button>
                    {/*bind函数绑定事件处理函数*/}
                    <button onClick={this.clickHandle3.bind(this)}>点击</button>
                    {/*bind函数绑定事件处理函数*/}
                    <button onClick={this.clickHandle4}>点击</button>
                    {/*箭头函数绑定事件处理函数*/}
                    <button onClick={(event) => this.clickHandle5(event)}>点击</button>

                </div>
            )
        }

        clickHandle1(event) {
            // 接收事件对象
            event.target.style.color = "red"
        }

        clickHandle2 = (event) => {
            // 接收事件对象
            event.target.style.color = "red"
        }

        clickHandle3(event) {
            // 接收事件对象
            event.target.style.color = "red"
        }

        clickHandle4(event) {
            // 接收事件对象
            event.target.style.color = "red"
        }

        clickHandle5(event) {
            // 接收事件对象
            event.target.style.color = "red"
        }
    }

    const root = ReactDOM.createRoot(document.querySelector("#root"))
    root.render(<App/>)
```

## 传递普通参数

```JavaScript
class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {count: 1}
    }

    render() {
        const {count} = this.state
        return (
            <div className="app">
                <h1>count的值是:{count}</h1>
                
                <button onClick={this.addCount1(10)}>+10</button>
                <button onClick={this.subCount1(20)}>-20</button>

                <button onClick={(event) => this.addCount2(event, 30)}>+30</button>
                <button onClick={(event) => this.subCount2(event, 40)}>-40</button>
            </div>
        )
    }

    addCount1(value) {
        /*注意：return的这个函数才是真正的事件处理函数，React会执行这个函数并且传入事件对象*/
        return (event) => {
            console.log(event.target)
            this.setState({count: this.state.count + value})

        }
    }

    subCount1(value) {
        /*注意：return的这个函数才是真正的事件处理函数，React会执行这个函数并且传入事件对象*/
        return (event) => {
            console.log(event.target)
            this.setState({count: this.state.count - value})
        }
    }

    addCount2(event, value) {
        console.log(event.target)
        this.setState({count: this.state.count + value})
    }

    subCount2(event, value) {
        console.log(event.target)
        this.setState({count: this.state.count - value})
    }
}
const root = ReactDOM.createRoot(document.querySelector("#root"))
root.render(<App/>)
```

## 电影列表选中案例

```JavaScript
class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            movies: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            currentIndex: 0,
        }
    }

    render() {
        const {movies, currentIndex} = this.state
        const liEls = movies.map((item, index) => {
            return (<li className={currentIndex === index ? "active" : ""}
                        key={item} onClick={this.clickHandle(index)}>{item}</li>)
        })
        return (
            <ul>{liEls}</ul>
        );
    }

    clickHandle(index) {
        return () => {
            this.setState({currentIndex: index})
        }
    }
}

const root = ReactDOM.createRoot(document.querySelector("#root"))
root.render(<App/>)
```



# 条件渲染

在 React 中，可以创建不同的组件来封装各种需要的行为。然后还可以根据应用的状态变化只渲染其中的一部分

React 中的条件渲染和 JavaScript 中的一致，使用 JavaScript 操作符 if 或条件运算符来创建表示当前状态的元素，然后让 React 根据它们来更新 UI

```JavaScript
/*
* 在 React 中，可以创建不同的组件来封装各种需要的行为。然后还可以根据应用的状态变化只渲染其中的一部分
* React 中的条件渲染和 JavaScript 中的一致，使用 JavaScript 操作符 if 或条件运算符来创建表示当前状态的元素，然后让 React 根据它们来更新 UI
*/
class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {flag: true}
    }

    render() {
        const {flag} = this.state
        const isLogin = () => {
            return flag ? <h1>准备好了</h1> : <h1>没有准备好</h1>
        }
        /*
        *   1.使用if语句判断
        *   let element = null
        *   if (flag) {element = <h1>准备好了</h1>
        *   } else {element = <h1>没有准备好</h1>}
        *   return element
        * 
        *  2.使用三元表达式判断
        *   return flag ? <h1>准备好了</h1> : <h1>没有准备好</h1>
        *
        *  3.封装函数调用函数进行渲染
        *   {isLogin()}
        *  
        * 4.使用短路与进行渲染
        *   {flag && <h1>准备好了</h1>}
        */
        return (
            <div>
                <div>{isLogin()}</div>
                <div>{flag ? <h1>准备好了</h1> : <h1>没有准备好</h1>}</div>
                <div>{flag && <h1>准备好了</h1>}</div>
            </div>
        )
    }
}

const root = ReactDOM.createRoot(document.querySelector("#root"))
root.render(<App/>)
```

## 条件渲染案例

```JavaScript
class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {flag: true}
    }

    render() {
        const {flag} = this.state
        return (
            <div>
                <button onClick={this.changeFlag}>切换</button>
                {/*销毁了元素*/}
                {flag && <h1>我显示了</h1>}
                {/*销毁了元素*/}
                {flag ? <h1>我显示了</h1> : ""}
                {/*没有销毁元素使用的是display*/}
                <h1 style={{display: flag ? "block" : "none"}}>我显示了</h1>
            </div>
        )
    }

    changeFlag = () => {
        this.setState((state) => {
            return {flag: !state.flag}
        })
    }
}

const root = ReactDOM.createRoot(document.querySelector("#root"))
root.render(<App/>)
```

```JavaScript
class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const isLogin = () => {
            const token = localStorage.getItem("token")
            return token ? <button onClick={this.logout}>退出登录</button> : <button onClick={this.login}>登录</button>
        }
        return (<div>{isLogin()}</div>)
    }

    logout = () => {
        localStorage.removeItem("token")
        //更新UI
        this.setState({})
    }
    login = () => {
        localStorage.setItem("token", "fefJk121e")
        //更新UI
        this.setState({})

    }
}

const root = ReactDOM.createRoot(document.querySelector("#root"))
root.render(<App/>)
```



# 列表渲染

React中的列表渲染需要使用map高阶函数，map() 方法返回一个新数组，数组中的元素为原始数组元素调用函数处理后的值

```JavaScript
/*
*  React中的列表渲染需要使用map高阶函数，map() 方法返回一个新数组，数组中的元素为原始数组元素调用函数处理后的值
*/
class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            persons: [
                {id: 1, name: "马云", age: 18},
                {id: 2, name: "马化腾", age: 15},
                {id: 3, name: "马保国", age: 10},
            ]
        }
    }

    render() {
        const {persons} = this.state
        return (
            <ul>
                {
                    // 链式调用
                    persons.filter(item => item.age >= 18).map(item => <li key={item.id}>id：{item.id} 姓名：{item.name} 年龄：{item.age}</li>)
                }
            </ul>
        )
    }
}

const root = ReactDOM.createRoot(document.querySelector("#root"))
root.render(<App/>)
```



# JSX的本质和原理

## JSX的本质

JSX是React.createElement()函数的语法糖，所有的JSX最终会被转换为React.createElement()函数的调用

```JavaScript
class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        /*这段结构会被转换为React.createElement函数的调用形成虚拟DOM*/
        const VDOM = (<div>
            <div className="header">
                <span>我是头部--左边</span>
                <span>我是头部--右边</span>
            </div>
            <div className="content">我是身体</div>
            <div className="footer">我是脚部</div>
        </div>)
        console.log(VDOM) // VOM是一个虚拟DOM
        return VDOM
    }
}

const root = ReactDOM.createRoot(document.querySelector("#root"))
root.render(<App/>)
```

## React.createElement()

`createElement` 允许创建一个 React 元素。它可以作为JSX的替代方案

```JavaScript
const element = createElement(type, props, ...children)
```

```JavaScript
    /*
    * createElement允许创建一个React元素。它可以作为JSX的替代方案
    *   第一个参数是必填，传入的是似HTML标签名称，eg: ul, li
    *   第二个参数是选填，表示的是属性，eg: className
    *   第三个参数是选填, 子节点，eg: 要显示的文本内容
    * */
    class App extends React.Component {
        constructor(props) {
            super(props)
        }
        render() {
            return React.createElement("div", null, "头部")
        }
    }

    const root = ReactDOM.createRoot(document.querySelector("#root"))
    root.render(React.createElement(App,null))


    /*
    * 通过createElement函数最终会创建一个ReactElement对象
    * createElement函数创建一个ReactElement对象实际就是一个虚拟DOM
    * */
    class App extends React.Component {
        constructor(props) {
            super(props)
        }

        render() {
            const VOM = React.createElement("div", null, "头部")
            console.log(VOM)
            console.log(typeof VOM)  // object
            console.log(VOM instanceof Object) // true
            return VOM
        }
    }

    const root = ReactDOM.createRoot(document.querySelector("#root"))
    root.render(React.createElement(App, null))
```

<img src="E:\01.前端\15.React\React(why)\1.React\images\React.createElement.png" style="zoom:150%;" />

# 虚拟DOM创建方式

在React中，虚拟DOM（Virtual DOM）是一种用于优化UI渲染性能的技术。它是React的核心概念之一，通过使用虚拟DOM，React能够高效地比较和更新实际DOM的变化。

React中，有两种主要的方式来创建虚拟DOM：JSX和React.createElement()

## JSX创建虚拟DOM

要使用JSX创建虚拟DOM元素，只需像编写HTML标记一样编写JSX代码。在JSX中，可以使用尖括号`< >`来定义元素，并使用大括号`{ }`来插入JavaScript表达式

```JavaScript
/*使用JSX创建虚拟DOM*/
const VDOM = <div>hello-React</div>
console.log(VDOM)
const root = ReactDOM.createRoot(document.querySelector("#root"))
root.render(VDOM)
```

## createElement()创建虚拟DOM

```JavaScript
    /*使用React.createElement创建虚拟DOM*/
    const VDOM=React.createElement("div", null,"hello-react")
    console.log(VDOM)
    const root = ReactDOM.createRoot(document.querySelector("#root"))
    root.render(VDOM)
```



# React脚手架

create-react-app简称 CRA）是一个官方支持的创建 React 单页应用的脚手架，它提供了一个零配置的现代构建设置，将一些复杂工具（比如 webpack， Babel）的配置封装了起来，让使用者不用关心这些工具的具体配置

## 创建项目并启动

create-react-app 是官方支持的创建单页 React 应用的方法

```
npx create-react-app my-app
cd my-app
npm start
```

```
npm i -g create-react-app
create-react-app my-app
cd my-app
npm start
```

## 脚手架项目结构

> public ---- 静态资源文件夹
>
> ​            favicon.icon ------ 网站页签图标
>
> ​            **index.html --------** **主页面**
>
> ​            logo192.png ------- logo图
>
> ​            logo512.png ------- logo图
>
> ​            manifest.json ----- 应用加壳的配置文件
>
> ​            robots.txt -------- 爬虫协议文件
>
> src ---- 源码文件夹
>
> ​            App.css -------- App组件的样式
>
> ​            **App.js --------- App****组件**
>
> ​            App.test.js ---- 用于给App做测试
>
> ​            index.css ------ 样式
>
> ​            **index.js -------** **入口文件**
>
> ​            logo.svg ------- logo图
>
> ​            reportWebVitals.js
>
> ​                    --- 页面性能分析文件(需要web-vitals库的支持)
>
> ​            setupTests.js
>
> ​                    ---- 组件单元测试的文件(需要jest-dom库的支持)



## 一个简单的组件

```jsx
// 引入React核心库
import React from "react";
// 默认导出App组件并且基继承React.Component
export default class App extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return(
            <div className="app"></div>
        )
    }
}
```

```JavaScript
// 引入React核心库
import React from "react";
// 引入ReactDom库
import ReactDom from "react-dom"
// 引入App根组件
import App from "./App";
// 使用createRoot函数创建根节点
const root=ReactDOM.createRoot(document.querySelector("#root"))
// 渲染组件
root.render(<App/>)
```

# 组件化编码流程

1.拆分组件: 拆分界面,抽取组件

2.实现静态组件: 使用组件实现静态页面效果

3.实现动态组件：动态显示初始化数据、数据类型、数据名称、保存在哪个组件?

4.交互(从绑定事件监听开始)



# 组件化开发

一个页面按照功能进行一层层的划分，直到划分到最小位置，随后在将拆分开来的组件依据他们之间的相互关系使他们有机的整合在一起

一般情况下，react中的`根组件`是`App组件`，其余组件都是`App组件`的子组件

![](E:\01.前端\15.React\React(why)\1.React\images\组件化开发.png)

> React的组件相对于Vue更加的灵活和多样，按照不同的方式可以分成很多类组件：
>
>   1.根据组件的定义方式可以分为：函数组件和类组件
>
>   2.根据组件内部是否有状态需要维护可以分为：有状态组件和无状态组件
>
>   3.根据组件得到不同职责可以分为：展示型组件和容器型组件

> 函数组件、无状态组件、展示型组件主要关注UI的展示
>
> 类组件、有状态组件、容器型组件主要关注数据逻辑

```JavaScript
import React, {Component} from 'react';
import Header from "./components/Header/Header";
import Aside from "./components/Aside/Aside";
import Main from "./components/Main/Main";
import Footer from "./components/Footer/Footer";
import "./App.css"

class App extends Component {
    render() {
        return (
            <div className="app">
                //Header组件
                <Header></Header>
                <div className="content">
                    //Aside组件
                    <Aside></Aside>
                    //Main组件
                    <Main></Main>
                </div>
                // Footer组件
                <Footer></Footer>
            </div>
        );
    }
}

export default App;
```

## 类组件

类组件的定义必须有如下要求：`组件的名称必须是大写字母开头`、`类组件必须继承React.Component`、`类组件必须实现render方法`

```JavaScript
import React, {Component} from 'react';
/*
*一：类组件的定义必须有如下要求：
*       组件的名称必须是大写字母开头、类组件必须继承React.Component、类组件必须实现render方法
*
*二：使用class定义一个组件
*       1.constructor方法是可选的
*       2.this.state中维护的就是组件内部的数据
*       3.render()方法是class组件中必须实现的方法
*
*三：render()函数的返回值  
*       1.react元素
*       2.组件或者fragments
*       3.Protals
*       4.字符串/数字类型
*       5.布尔类型、null、undefined(不会进行显示)
* */
class App extends Component {
    constructor(props) {
        super(props)
        this.state = {info: "类组件"}
    }

    render() {
        const {info} = this.state
        return (
            <div className="app"><h1>{info}</h1></div>
        );
    }
}

export default App;
```

## 函数组件

函数组件是使用function来定义的，函数组件中的render()返回值和类组件中的render()函数返回值是一样的内容

函数式组件在没有使用hooks的情况下有如下特点：

> 1. 没有生命周期，也会被更新并且挂载，但是函数式组件没有生命周期
>
> 2. this关键字不能指向组件实例(因为没有组件实例)
>
> 3. 没有内部的状态(state)

```javascript
import react, { useState,useEffect } from "react"
export default function App(props) {
    const [info, setInfo] = useState("函数式组件")

    useEffect(()=>{
        console.log("组件挂载完毕");
    },[])

    const changeInfo = () => {
        setInfo("react")
    }

    return (
        <div className="app">
            <h1>{info}</h1>
            <button onClick={changeInfo}>修改文本</button>
        </div>
    )
}
```



# 生命周期

react的生命周期，是react组件从挂载到更新再到卸载的这样一个过程，react内部为了告诉我们当前处于哪个阶段，会对组件内部的生命周期函数进行调用

## 组件挂载流程

挂载阶段(mounted)：组件第一次在DOM树中被渲染的过程

更新阶段(update)：组件状态发生变化，重新更新渲染的过程

卸载阶段(unmount)：组件从DOM中移除的过程

> componentDidMount()：在组件挂在后（插入到dom树中）后立即调用
>
> componentDidUpdate()：该函数在组件更新之后执行，它是组件更新的最后一个环节
>
> componentWillUnmount()：在组件卸载和销毁之前调用

## 常用的生命周期

![常用的生命周期](E:\01.前端\15.React\React(why)\1.React\images\常用的生命周期.png)

```JavaScript
import React, { Component } from 'react'
import ReactDOM from 'react-dom/client';
export default class Header extends Component {
  constructor(props) {
    super(props)
    /* 
    *constructor()数据的初始化:
    *  接收props和context，当想在函数内使用这两个参数需要在super传入参数，
    *  当使用constructor时必须使用super，否则可能会有this的指向问题，如果不初始化state或者不进行方法绑定，则可以不为组件实现构造函数
    */
    console.log("Header的constructor")
  }
  componentDidMount() {
    /*
    * componentDidMount()在组件挂在后（插入到dom树中）后立即调用:
    *   可以在这里调用Ajax请求，返回的数据可以通过setState使组件重新渲染，或者添加订阅
    *   但是要在conponentWillUnmount中取消订阅
    */
    console.log("Header的componentDidMount")
  }
  render() {
    /*
    *render()class组件中唯一必须实现的方法:
    *  render函数会插入jsx生成的dom结构，react会生成一份虚拟dom树，在每一次组件更新时
    *  在此react会通过其diff算法比较更新前后的新旧DOM树，比较以后，找到最小的有差异的DOM节点，并重新渲染
    */
    console.log("Header的render")
    return (<div>Header组件</div>)
  }
  componentDidUpdate() {
    /*componentDidUpdate()该函数在组件更新之后执行，它是组件更新的最后一个环节*/
    console.log("Header的componentDidUpdate")
  }
  componentWillUnmount() {
    /* 
    *componentWillUnmount()在组件卸载和销毁之前调用
    *  在这执行必要的清理操作，例如，清除timer（setTimeout,setInterval），取消网络请求，或者取消在componentDidMount的订阅，移除所有监听
    */
    console.log("Header的componentWillUnmount")
  }
}

```

## 完整的生命周期

<img src="E:\01.前端\15.React\React(why)\1.React\images\完整的生命周期.png" style="zoom:150%;" />

```JavaScript
import React, { Component } from 'react'

export default class Aside extends Component {
  constructor(props) {
    super(props)
    this.state = { count: 1 }
    /* 
    *constructor()数据的初始化:
    *  接收props和context，当想在函数内使用这两个参数需要在super传入参数，
    *  当使用constructor时必须使用super，否则可能会有this的指向问题，如果不初始化state或者不进行方法绑定，则可以不为组件实现构造函数
    */
    console.log("Aside的constructor")
  }
  componentDidMount() {
    /*
    * componentDidMount()在组件挂在后（插入到dom树中）后立即调用:
    *   可以在这里调用Ajax请求，返回的数据可以通过setState使组件重新渲染，或者添加订阅
    *   但是要在conponentWillUnmount中取消订阅
    */
    console.log("Aside的componentDidMount")
  }
  render() {
    /*
    *render()class组件中唯一必须实现的方法:
    *  render函数会插入jsx生成的dom结构，react会生成一份虚拟dom树，在每一次组件更新时
    *  在此react会通过其diff算法比较更新前后的新旧DOM树，比较以后，找到最小的有差异的DOM节点，并重新渲染
    */
    console.log("Aside的render")
    return (<div>Aside组件</div>)
  }
  componentDidUpdate(preProps,preState,value) {
    /*componentDidUpdate()该函数在组件更新之后执行，它是组件更新的最后一个环节*/
    console.log("Aside的componentDidUpdate",preProps,preState,value)
  }
  componentWillUnmount() {
    /* 
    *componentWillUnmount()在组件卸载和销毁之前调用
    *  在这执行必要的清理操作，例如，清除timer（setTimeout,setInterval），取消网络请求，或者取消在componentDidMount的订阅，移除所有监听
    */
    console.log("Aside的componentWillUnmount")
  }

  static getDerivedStateFromProps(props, state) {
    console.log("Aside的getDerivedStateFromProps得到派生的状态", props, state)
    return state
  }


  getSnapshotBeforeUpdate() {
    console.log("getSnapshotBeforeUpdate()")
    return "coderWhy"
  }

  shouldComponentUpdate(nextProps, nextState) {
    //组件是否需要更新，需要返回一个布尔值，返回true则更新，返回flase不更新
    //shouldComponentUpdate()的返回值，判断React组件的输出是否受当前state或props更改的影响。默认行为是state每次发生变化组件都会重新渲染
    console.log("Aside的shouldComponentUpdate返回的布尔值确定是否更新", nextProps, nextState)
    return true
  }
}
```



# 组件通信

react组件之间的通信，大致可以分为以下几类

> 父传子---props传递自定义属性给子组件
>
> 子传父---props传递一个函数子组件然后子组件调用函数
>
> 任意组件之间的通信---pubSub消息订阅与发布
>
> 数据全局共享---redux

## 父组件传递给子组件

当父组件需要传递数据给子组件的使用可以使用props自定义属性

```JavaScript
import React, { Component } from 'react'
import Children from './components/Children'
export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = { count: 1 ,name:"陶品奇",age:22}
    }
    render() {
        const {state}=this
        return (
            <div>
             {/* 使用props自定义属性将父组件的值传递给子组件（单个传递props）
             <Children count={count} ></Children>*/}
              
              {/* 使用props自定义属性将父组件的值传递给子组件（批量传递props） */}
              <Children {...state}></Children>
                <button onClick={this.addCount}>+1</button>
            </div>
        )
    }

    addCount=()=>{
        // 更新父组件的state，子组件用到了父组件传递过去的props，当父组件更新数据时子组件也会被更新
        this.setState((state)=>({count:state.count+=1}))
    }
}
```

```JavaScript
import React, { Component } from 'react'

export default class Children extends Component {
  render() {
    // 父组件传递的props自定义属性都会在组件实例的props对象身上
    console.log(this.props);
    const {count}=this.props
    return (
      <div>{count}</div>
    )
  }
}

```

## 子组件传递给父组件

子组件传递数据给父组件可以通过props传递函数，父组件给子组件传递一个函数类型的props然后子组件通过props调用传递过来的函数

```JavaScript
import React, { Component } from 'react'
import Children from './components/Children'

export default class App extends Component {
    constructor(props) {
        super(props)
        /* 父组件的数据value */
        this.state = { value: null }
    }
    render() {
        const { value } = this.state
        return (
            <div>
                {value}
                {/* 给子组件传递一个函数 */}
                <Children changeValue={this.changeValue}></Children>
            </div>
        )
    }
    /* 传递给子组件的函数接收子组件传递过来的数据value */
    changeValue = (value) => {
        /* 调用setState()更新父组件value属性 */
        this.setState(state => ({ value }))
    }
}

```

```JavaScript
import React, { Component } from 'react'

export default class Children extends Component {
  constructor(props) {
    super(props)
    this.state = { count: 0 }
  }
  render() {
    const { count } = this.state
    return (
      <div className='children'>
        {count}
        <button onClick={this.addCount}>+1</button>
      </div>
    )
  }
  addCount = () => {
    this.setState((state) => ({ count: state.count += 1 }), () => {
      /* 立即调用父组件传递过来函数，并且将值作为参数传递过去 */
      this.props.changeValue(this.state.count)
    })

  }
}
```

## 消息订阅与发布

- 当我们想要父组件传值给子组件时，通常会使用props传值。
- 子组件传值给父组件时，通常会子组件中的事件触发一个回调函数（也是props），父组件中的对应函数再去修改值。
- 兄弟组件间传值，通常会将子组件A的值传回父组件，父组件再传给子组件B

以上三种情况都可以使用消息订阅与发布机制来解决。

- 当然父传子还是用props比较好。
- 子组件传值给父组件，在子组件中发布，在父组件中订阅，就可以拿到相应的值
- 兄弟组件间传值，在子组件A中发布，在子组件B中订阅

### 下载pubsub-js

```
npm install pubsub-js
```

### 发布消息

```jsx
class App extends React . Component  {
  componentDidMount() {
   // publish 发布消息 消息名为：publish_one 内容为：This is publish
      PubSub.publish("publish_one","This is publish")
  }
  render() {
    return (
      <div className = 'ArticleContainer'>
        <Data/>
      </div>
    )
  }
}
```

### 订阅消息

```jsx
lass Data extends React.Component{
  state={
    publishData:''
  }
  componentDidMount(){
   // 订阅消息 消息名：publish_one  第二个参数是一个函数
   // 此函数又有两个参数：消息名和消息数据
    PubSub.subscribe("publish_one",(msg,data)=>{
      this.setState({publishData:data})
    })
  }
  render(){
    return(
    <div>
      {this.state.publishData}
    </div>)
  }
}
```

### 定义token

```jsx
componentDidMount(){
    this.token = PubSub.subscribe('publish_one',(msg,data)=>{
       this.setState({publishData:data})
    })
}
```

### 取消订阅

```jsx
componentWillUnmount(){
     PubSub.unsubscribe(this.token)
 }
```

## 全局事件总线

```
npm i hy-event-store
```

```javascript
import {HYEventBus} from "hy-event-store"
const eventBus=new HYEventBus()
export default eventBus
```

### 发送事件

```JavaScript
eventBus.emit("事件名", 值)
```

### 监听事件

```JavaScript
 eventBus.on("事件名",callback)
```

### 取消事件

```JavaScript
eventBus.off("事件名",要卸载的事件函数)卸载事件
```

```JavaScript
import React, { Component } from 'react'
import Left from './components/Left/Left'
import Right from './components/Right/Right'

export default class App extends Component {
    render() {
        return (
            <div className='app'>
                <Left></Left>
                <Right></Right>
            </div>
        )
    }
}
```

```JavaScript
import React, { Component } from 'react'
import eventBus from '../../utils/eventBus'

export default class Left extends Component {
    render() {
        return (
            <div>
                <button onClick={this.clickHandle}>+1</button>
            </div>
        )
    }
    clickHandle() {
        //  使用eventBus.emit("事件名", 值)
        eventBus.emit("changeCount", 1)
    }

}
```

```JavaScript
import React, { Component } from 'react'
import eventBus from '../../utils/eventBus'
export default class Right extends Component {
    constructor(props) {
        super(props)
        this.state = { count: 0 }
    }
    render() {
        const { count } = this.state
        return (
            <div>
                {count}
            </div>
        )
    }
    changeCount(count) {
        this.setState({ count: this.state.count += count })
    }
    componentDidMount() {
        // 组件挂载完毕使用 eventBus.on("事件名",callback)监听事件
        eventBus.on("changeCount", this.changeCount.bind(this))
    }
    componentWillUnmount() {
        // 组件即将卸载完毕使用 eventBus.off("事件名",要卸载的事件函数)卸载事件
        eventBus.off("changeCount", this.changeCount)
    }
}
```



# 类式组件的四大核心属性



## 类组件的state

state是组件对象最重要的属性，值是对象(可以包含多个键值对的集合)

组件被称为状态机，通过更新组件的state来更新页面的显示(重新渲染组件)

> 组件中render函数中的this指向的是组件实例对象
>
> 组件的自定义方法中this的指向为undefined，如何解决：1.通过bind函数绑定this、2.类的实例方法、3.箭头函数
>
> state状态不能直接修改需要调用setState()

```JavaScript
/*
*state是组件对象最重要的属性，值是对象(可以包含多个键值对的集合)
*组件被称为状态机，通过更新组件的state来更新页面的显示(重新渲染组件)
* */
class App extends React.Component {
    constructor(props, context) {
        super(props, context)
        this.state = {flag: true}
        this.changeFlag = this.changeFlag.bind(this)
    }

    render() {
        const {flag} = this.state
        console.log("render函数被触发了")
        return (
            <div className="app">
                <h1>今天天气很{flag ? "炎热" : "凉爽"}</h1>
                <button onClick={this.changeFlag}>切换</button>
            </div>
        );
    }

    changeFlag() {
        // this.state.flag=!this.state.flag 注意state状态不可以直接更改，需要借助setState函数来修改

        // setState函数做了两件事件：1.修改数据、2.触发render()函数更新视图
        this.setState({flag: !this.state.flag}, () => {
             // 能拿到更新后的最新值
            console.log("修改了flag", this.state.flag)
        })
        // 拿不到更新后的最新值
        console.log(this.state.flag) 
    }

}

ReactDOM.render(<App></App>, document.querySelector("#root"))
```

## 类组件的props

props是组件（包括函数组件和class组件）间的内置属性，其作用是可以传递数据给子组件

state和props主要的区别在于props是不可变的(props遵循单向数据流)，而state可以根据与用户交互来改变

> 理解props：每个组件对象都会有props属性，组件标签的所有属性都保存在props中
>
> props的作用：通过标签属性从组件外向组件内传递变化的数据
>
> 组件的props是只读属性，不能直接对prop的值进行修改

```JavaScript
/*
* state 和 props 主要的区别在于 props 是不可变的，而 state 可以根据与用户交互来改变。这就是为什么有些组件需要定义state来更新和修改数据，而子组件只   能通过props来传递数据
* */
class Person extends React.Component {
    constructor(props, context) {
        super(props, context)
    }

    render() {
        // 从组件实例的props中解构赋值拿到外部传递过来的自定义属性prop
        const {name, age, sex} = this.props
        return (
            <div className="app">
                <ul>
                    <li>姓名是：{name}</li>
                    <li>年龄是：{age}</li>
                    <li>性别是：{sex}</li>
                </ul>
            </div>
        );
    }
}

// 给Person组件传递三个props分别是name、age、sex
ReactDOM.render(<Person name="陶品奇" age="22" sex="男"></Person>, document.querySelector("#root"))
```

### propTypes类型校验

```
npm i prop-types
```

```JavaScript
import PropTypes from 'prop-types'
```

```JavaScript
import React, { Component } from 'react'
/* 引入类型校验的库 */
import PropTypes from 'prop-types'
export default class Main extends Component {
    /* 类型的校验 */
    static propTypes = {
        // person必须是对象类型且为必填
        person: PropTypes.object.isRequired,
        hoby: PropTypes.string.isRequired
    }
    /*对props的值进行默认值的指定*/
    static defaultProps = {
            hoby:"???"
    }
    constructor(props) {
        super(props)
    }
    render() {
        const { person, hoby } = this.props
        return (
            <ul>
                <li>姓名是：{person.name}</li>
                <li>年龄是：{person.age}</li>
                <li>地址是：{person.address}</li>
                <li>爱好是：{hoby}</li>
                <li>ID是：{person.id}</li>
            </ul>
        )
    }
}

```



## 类组件的refs







## 类组件的context

> context提供了一种在组件之间共享此类值的方式，而不必显式的通过组件树的逐层的传递props

context.Provider：通过value属性提供数据

context.Consumer：通过回调函数中的形参消费数据，类组件可以通过this.context消费数据

```JavaScript
import React from 'react'
/* 创建一个Context */
export default React.createContext()
```

```JavaScript
import React, { Component } from 'react'
import Children from './components/Children/Children'
/* 引入创建好的context */
import ThemeContext from './context/ThemeContext'

export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = { color: "#c00000" }
    }
    render() {
        const { color } = this.state
        return (
            <div className='app'>
                {/* 使用context中的Provider的value属性提供数据 */}
                <ThemeContext.Provider value={{ color }}>
                    <Children></Children>
                </ThemeContext.Provider>
            </div>
        )
    }
}
```

```JavaScript
import React, { Component } from 'react'
/*引入创建好的context */
import ThemeContext from '../../context/ThemeContext';
class Son extends Component {

    render() {
        const { color } = this.context
        return (
            <div>
                <span style={{ color }}>context</span>
            </div>
        )
    }
}
/*设置组件的contextType类型为某一个context */
Son.contextType = ThemeContext
export default Son
```

```JavaScript
import React, { Component } from 'react';
import ThemeContext from "../../context/ThemeContext";
class Sun extends Component {
    render() {
        return (
            <div>
                {/*使用context中的Consumer属性消费数据*/}
                <ThemeContext.Consumer>
                    {value => <span style={{color:value.color}}>context</span>}
                </ThemeContext.Consumer>
            </div>
        );
    }
}

Sun.contextType = ThemeContext
export default Sun;
```



# React中的插槽

React拥有强大的组合模式，有些组件我们无法提前知晓他们子组件的具体内容如侧边栏sideBar和Dialog对话框，此时react官方建议我们使用一个特殊的props （children）来将这些无法提前预知内容的子组件传递到渲染到结果中

jsx中的所有内容都会通过children prop传递到父组件中，使用react组合的方式可以实现类似于Vue插槽的功能

> react中没有插槽的概念，但是我们可以通过组合的形式实现‘插槽’

## children实现

> 父组件：在组件的标签中书写html结构
>
> 子组件：在children中获取传递过来的结构

弊端：索引值获取传入的元素容易出错，不能精准的获取传入的元素

```JavaScript
import React, { Component } from 'react'
import NavBar from './components/NavBar/NavBar'

export default class App extends Component {
    render() {
        return (
            <div className='app'>
                <NavBar>
                    {/* 在组件标签中书写正常的html结构，此时子组件的children属性中会存储父组件中书写的结构(children可能是数组也可能是对象) */}
                    <button>返回</button>
                    <input type='text' placeholder='请输入关键字' />
                    <button>前进</button>
                </NavBar>
            </div>
        )
    }
}

```

```JavaScript
import React, { Component } from 'react'
import "./NavBar.css"
export default class NavBar extends Component {
    render() {
        // 此时children是一个数组
        const [back, input, fowrk] = this.props.children
        return (
            <div className='navbar'>
            	{/*children实现插槽*/}
                <div className="left">{back}</div>
                <div className="center">{input}</div>
                <div className="right">{fowrk}</div>
            </div>
        )
    }
}

```

## props实现

> 父组件：通过props给子组件传递html结构
>
> 子组件：在子组件的props中获取传递过来的props

```javascript
import React, { Component } from 'react'
import NavBar from './components/NavBar/NavBar'

export default class App extends Component {
    render() {
        return (
            <div className='app'>
                {/* props实现插槽，使用props传递一个html元素 */}
                <NavBar
                    leftSlot={<button>后退</button>}
                    centerSlot={<input placeholder='关键字' />}
                    rightSlot={<button>前进</button>}>
                </NavBar>
            </div>
        )
    }
}
```

```JavaScript
import React, { Component } from 'react'
import "./NavBar.css"
export default class NavBar extends Component {
    render() {
        /* 接收父组件传递过来的html元素 */
        const { leftSlot, centerSlot, rightSlot } = this.props
        return (
            <div className='navbar'>
                <div className="left">{leftSlot}</div>
                <div className="center">{centerSlot}</div>
                <div className="right">{rightSlot}</div>
            </div>
        )
    }
}
```

## 作用域插槽的实现

> 父组件：通过props给子组件传递一个函数，并且这个函数接收子组件传递出来的参数，而且这个函数返回的是html结构
>
> 子组件：调用父组件传递来的函数，并且传递参数过去

```JavaScript
import React, { Component } from 'react'
import Tab from './components/Tab/Tab'


export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            navList: ["流行", "新款", "精选"],
            index: 0
        }
    }
    render() {
        const { navList, index } = this.state
        return (
            <div>
               {/*通过props给子组件传递一个函数，并且这个函数接收子组件传递出来的参数，而且这个函数返回的是html结构*/}
                <Tab navList={navList} getIndex={this.getIndex} itemType={(item => <button>{item}</button>)}></Tab>
                <div style={{ textAlign: "center" }}>{navList[index]}</div>
            </div>
        )
    }
    getIndex = (index) => {
        this.setState({ index })
    }
}

```

```JavaScript
import React, { Component } from 'react'
import "./Tab.css"
export default class Tab extends Component {
    constructor(props) {
        super(props)
        this.state = { currentIndex: 0 }
    }
    render() {
        const { navList, itemType } = this.props
        const { currentIndex } = this.state
        return (
            <ul>
                {
                    navList.map((item, index) => <li key={index}>
                        <span className={currentIndex === index ? "active" : ""} onClick={this.changeIndex(index)}>
                            {/*调用父组件传递来的函数，并且传递参数过去*/}
                            {itemType(item)}
                        </span>
                    </li>)
                }
            </ul>
        )


    }
    changeIndex = (index) => {
        return () => {
            this.setState({ currentIndex: index }, () => {
                this.props.getIndex(index)
            })
        }
    }
}
```

# setState函数

在 React 中，组件分为有状态组件和无状态组件，有状态组件就是能够定义state的组件，比如类组件

state 就是用来描述事物在某时刻的数据，可以被改变，改变后与视图相映射，用来保存数据和响应视图

> 虽然状态可以改变，但不是响应式的，动态改变并没有与视图响应，想要改变并响应视图则需要 调用setState函数修改并更新视图
>
> setState函数是从React.Component中继承过来的

## 为什么使用setState

开发中并不能直接通过修改state的值让界面发生更新

因为我们修改了state之后，希望React根据最新的state来重新渲染界面，但是这种方式的修改React并不知道数据发生了变化

React没有实现类似于Vue2的Object.defineproperty或者Vue3的proxy的方式来监听数据的变化

React中必须调用setState函数来告知React数据已经发生了变化



## setState的详细使用

```JavaScript
import React, { Component } from 'react'


export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            message: "hello",
            count: 0
        }
    }
    render() {
        const { message, count } = this.state
        return (
            <div className='app'>
                <h1>count的值是：{count}</h1>
                <button onClick={this.addCount}>+</button>

                <h1>message的值是：{message}</h1>
                <button onClick={this.changeText}>修改文本</button>
            </div>
        )
    }
    addCount = () => {

    }
    changeText = () => {
        /*
         1.基本使用(传入对象)
            this.setState({ message: "李银河" })
        */

        /*
        2.传入回调函数返回一个对象
            this.setState((state, props) => {
                return {
                    message: "李银河"
                }
            })
        */

        // 3.setState在React的事件处理中是一个异步调用
        // 如果希望在数据更新之后(合并)想要获取更新后的数据，可以在setState的第二个参数传入callback
        this.setState({ message: "李银河" }, () => {
            console.log(this.state.message);
        })
        console.log(this.state.message) // hello(更新之前的结果)
    }
}
```

## setState异步更新

> 设置为异步可以显著提升性能：如果每次调用setState都进行一次更新，意味着render函数会频繁的调用界面重新渲染，React18将获取到多个更新，之后进行批量更新
>
> 如果同步更新了state，但还没有执行render函数，那么state和props不能保持同步，这对开发是很大问题

```JavaScript
  this.setState({ message: "李银河" }, () => {
       console.log(this.state.message)//(更新之后的结果)
  })
  console.log(this.state.message) //(更新之前的结果)
```

# 性能优化

## PureComponent

除了使用shouldComponentUpdate来判断是否需要更新组件，还可以用PureComponent, PureComponent实际上自动加载shouldComponentUpdate函数，当组件更新时，shouldComponentUpdate对props和state进行了一层浅比较

```JavaScript
import React, { Component, PureComponent } from 'react'

export default class Children extends PureComponent {
    render() {
        console.log("Children组件的render");
        return (
            <div>Children组件</div>
        )
    }
}
```

## memo

在使用PureComponent的时候，只能把react组件写成是class的形式，不能使用函数的形式,react v16.6.0之后，可以使用React.memo来实现函数式的组件，也有了PureComponent的功能

```javascript
import { memo } from "react";

export default memo(function Header(props) {
    return (
        <div>
            Header
        </div>
    )
})
```



# 受控和非受控组件

## 受控组件

在HTML中，标签<input>、<textarea>、<select>的值的改变通常是根据用户输入进行更新。在React中，可变状态通常保存在组件的状态属性中，并且只能使用 setState() 更新，而呈现表单的React组件也控制着在后续用户输入时该表单中发生的情况，以这种由React控制的输入表单元素而改变其值的方式，称为：“受控组件”

```JavaScript
import React, { Component } from 'react'
/* 
## 受控组件
在HTML中，标签<input>、<textarea>、<select>的值的改变通常是根据用户输入进行更新。
在React中，可变状态通常保存在组件的状态属性中，并且只能使用 setState()更新
而呈现表单的React组件也控制着在后续用户输入时该表单中发生的情况，以这种由React控制的输入表单元素而改变其值的方式，称为：“受控组件”
*/
export default class Header extends Component {
  constructor(props) {
    super(props)
    this.state = { username: "陶品奇" }
  }
  render() {
    const { username } = this.state
    return (
      <div>
        <input type="text" value={username} onChange={this.changeUserName} />
        <span>{username}</span>
      </div>
    )
  }
  changeUserName = (event) => {
    const { value } = event.target
    this.setState({ username: value })
  }
}

```

## 非受控组件

表单数据由DOM本身处理。即不受setState()的控制，与传统的HTML表单输入相似，input输入值即显示最新值（使用 `ref`从DOM获取表单值）

```JavaScript
import React, { Component } from 'react'
/* 
## 非受控组件
表单数据由DOM本身处理。即不受setState()的控制，与传统的HTML表单输入相似，input输入值即显示最新值（使用 `ref`从DOM获取表单值）
*/
export default class Children extends Component {
  constructor(props) {
    super(props)
    this.state = { username: "" }
  }
  render() {
    const { username } = this.state
    return (
      <div>
        <input type="text" onChange={this.changeUserName} />
        <span>{username}</span>
      </div>
    )
  }
  changeUserName = (event) => {
    const { value } = event.target
    this.setState({ username: value })
  }
}
```

# 收集表单数据

```JavaScript
import React, { Component } from 'react'


export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: "",
            password: "",
            message: "",
            done: true,
            hobby: [
                { value: "吃饭", text: "吃饭", isChecked: true },
                { value: "睡觉", text: "睡觉", isChecked: true },
                { value: "打豆豆", text: "打豆豆", isChecked: false },
            ],
            address: "北京"
        }
    }
    render() {
        const { userName, password, message, done, hobby, address } = this.state
        return (
            <div>
                <form>
                    <div>用户名：<input type='text' name='userName' value={userName} onChange={this.handleInput} /></div>
                    <div>密码：<input type='password' name='password' value={password} onChange={this.handleInput} /></div>
                    <div>意见箱：<textarea type='textarea' name='message' value={message} onChange={this.handleInput} /></div>
                    <div>同意协议：<input type='checkbox' checked={done} name='done' onChange={this.handleAgree} /></div>
                    <div>爱好：
                        {
                            hobby.map((item, index) => {
                                return (
                                    <label key={item.value}>{item.text}
                                       <input type='checkbox'
                                        value={item.value}
                                        checked={item.isChecked}
                                        onChange={(e) => this.changeHobby(e, index)} />
                                    </label>
                                )
                            })
                        }
                    </div>
                    <select value={address} onChange={this.changeAddress}>
                        <option value='北京'>北京</option>
                        <option value='上海'>上海</option>
                        <option value='广州'>广州</option>
                    </select>
                </form>
            </div>
        )
    }
    handleInput = (event) => {
        const { name, value, checked } = event.target
        this.setState({ [name]: value })
    }
    handleAgree = (event) => {
        const { checked } = event.target
        this.setState({ done: checked })
    }
    changeHobby = (event, index) => {
        const { hobby } = this.state
        const { checked } = event.target
        let newHobby = [...hobby]
        newHobby[index].isChecked = checked
        this.setState({ hobby: newHobby })
    }
    changeAddress = (event) => {
        const { value } = event.target
        this.setState({ address: value })
    }
}
```

# 高阶组件

定义：高阶组件是参数为组件，返回值是新组件的函数

高阶组件本身不是一个组件而是一个函数，其次这个函数的参数是一个组件返回值也是一个组件

```JavaScript
import {PureComponent} from "react";

/*定义一个高阶组件：参数为组件，并且返回一个新组件*/
function hocFunction(Component) {
    // Component参数：一个组件
    return class Hoc extends PureComponent {
        constructor(props) {
            super(props)
            this.state = {
                userInfo: {name: "陶品奇", age: 22, address: "衡阳"}
            }
        }

        render() {

            // 返回一个新的组件，并且这个组件携带name属性的props
            return (<Component {...this.state.userInfo}></Component>)
        }
    }
}

export default hocFunction
```

```JavaScript
import React, {Component} from 'react';
import hocFunction from "../../hoc/hoc";

class Header extends Component {
    render() {
        const {name,age,address}=this.props
        return (
            <div>
                <ul>
                    <li>{name}</li>
                    <li>{age}</li>
                    <li>{address}</li>
                </ul>
            </div>
        )
    }
}

export default hocFunction(Header);
```

```JavaScript
import React, {Component, PureComponent} from 'react';
import Header from "./components/Header/Header";

class App extends Component {
    render() {
        return (
            <div>
                <Header></Header>
            </div>
        );
    }
}

export default App;
```



# portals的使用

Portal 提供了一种将子节点渲染到存在于父组件以外的 DOM 节点的优秀的方案

```JavaScript
ReactDOM.createPortal(child, container)
// 第一个参数（child）是任何可渲染的 React 子元素，例如一个元素，字符串或 fragment。第二个参数（container）是一个 DOM 元素
```

```JavaScript
import React, { Component } from 'react'
import { createPortal } from "react-dom"

export default class App extends Component {
    render() {
        return (
            <div>
                <h1>App组件</h1>
                {
                    createPortal(<h1>hello</h1>,document.querySelector("body"))
                }
            </div>
        )
    }
}

```



# fragment的使用

React Fragment 是 React 中的一个特性，它允许你对一组子元素进行分组，而无需向 DOM 添加额外的节点，从而允许你从 React 组件中返回多个元素。

要从 React 组件返回多个元素，需要将元素封装在根元素中

```JavaScript
import React, { Component,Fragment } from 'react'

export default class App extends Component {
    render() {
        return (
            <Fragment>
                <h1>1</h1>
                <h1>2</h1>
                <h1>3</h1>
            </Fragment>
        )
    }
}
```

# StrictMode的使用

使用`React.StrictMode` 内置组件来启用 React 执行的一组检查，并向你发出警告。

一个简单的方法是在index.js文件中用`<React.StrictMode></React.StrictMode>` 包裹整个App组件

这个组件的主要用例之一是作为一个自动的最佳实践、潜在问题和废弃检查。

```JavaScript
import React from 'react'
import ReactDOM from 'react-dom'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
document.getElementById('root')
```



# 过渡动画

## react-transition-group

<img src="E:\01.前端\15.React\React(why)\1.React\images\transition.png" style="zoom:80%;" />

```
npm i react-transition-group
```

## CSSTransition

CSSTransition是基于Transition组件构建的

CSSTransition执行过程中，有三个状态:appear、enter、exit

它们有三种状态，需要定义对应的CSS样式

- 开始状态:对于的类是-appear、-enter、-exit

- 执行动画:对应的类是-appear-active、-enter-active、-exit-active

- 执行结束:对应的类是-appear-done、-enter-done、-exit-done

  <img src="E:\01.前端\15.React\React(why)\1.React\images\csstransition组件的属性.png" style="zoom: 80%;" />

  <img src="E:\01.前端\15.React\React(why)\1.React\images\csstransition的钩子函数.png" style="zoom: 150%;" />

```css
/*元素进入*/
.enter {
    opacity: 0;
}

.enter-active {
    opacity: 1;
    transition: all 2s;
}

/*元素离开*/
.exit {
    opacity: 1;

}

.exit-active {
    opacity: 0;
    transition: all 2s;
}
```

```html
<CSSTransition in={flag} timeout={2000}  unmountOnExit={true}>
    <h2>react动画</h2>
</CSSTransition>
```

## SwitchTransition

SwitchTransition可以完成两个组件之间或者一个组件的两个状态直接的炫酷动画

这个动画在vue中被称之为 vue transition modes

react-transition-group中使用SwitchTransition来实现该动画

SwitchTransition中主要有一个属性:mode，有两个值

- in-out:表示新组件先进入，旧组件再移除
- out-in:表示旧组件先移除，新组件再进入

```JavaScript
import React, { PureComponent } from 'react'
import { CSSTransition, SwitchTransition } from 'react-transition-group'
import './style.css'

export class App extends PureComponent {
	constructor(props) {
		super(props)

		this.state = {
			isShow: true
		}
	}

	render() {
		const { isShow } = this.state

		return (
			<div>
				<button onClick={() => this.changeState()}>click me</button>
                 {/* mode用于控制动画出入顺序，默认就是out-in */}
				<SwitchTransition mode="out-in">
					{/*
						SwitchTransition组件里面要有CSSTransition或者Transition组件，不能直接包裹你想要切换的组件
						CSSTransition如果被SwitchTransition组件所包裹，需要使用key属性来标识各个状态，以便于在各个状态之间进行切换
					*/}
					<CSSTransition
				    	key={ isShow ? 'show' : 'hidden' }
						timeout={ 1000 }
						classNames='fade'
						appear>
						<div>Hello Wolrd</div>
					</CSSTransition>
				</SwitchTransition>
			</div>
		)
	}

	changeState() {
		this.setState({
			isShow: !this.state.isShow
		})
	}
}

export default App
```

## TransitionGroup

当我们有一组动画时，需要将这些CSSTransition放入到一个TransitionGroup中来完成动画

```JavaScript
import React, { PureComponent } from 'react'
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import './style.css'

export class App extends PureComponent {
	constructor(props) {
		super(props)

		this.state = {
			friends: [
				{
					id: 0,
					name: 'Klaus'
				},
				{
					id: 1,
					name: 'Alex'
				},
				{
					id: 2,
					name: 'Alice'
				},
				{
					id: 3,
					name: 'Jhon'
				}
			]
		}
	}

	render() {
		const { friends } = this.state

		return (
			<div>
				{/* 默认情况下，TransitionGroup会被渲染为div元素如果不希望渲染为div元素的时候，可以通过component属性来进行指定*/}
				<TransitionGroup component='ul'>
					{
						friends.map((friend, index) => (
							// CSSTransition的key必须唯一，且不能是index
							// 因为移除列表元素的时候，对应元素的index可能会发生改变
							// 从而导致离场动画执行混乱
							<CSSTransition
							timeout={ 1000 }
							classNames='fade'
							key={ friend.id }>
							<li>
								<span>{ friend.name }</span>
								<button onClick={() => this.removeFriend(index)}>remove</button>
							</li>
						</CSSTransition>
						))
					}
				</TransitionGroup>
				<button onClick={() => this.addFriend()}>add</button>

			</div>
		)
	}

	addFriend() {
		const friends = [...this.state.friends]

		friends.push({
			id: friends.length,
			name: 'Steven'
		})

		this.setState({
			friends
		})
	}

	removeFriend(index) {
		const friends = [...this.state.friends]
		friends.splice(index, 1)

		this.setState({
			friends
		})
	}
}

export default App
```

