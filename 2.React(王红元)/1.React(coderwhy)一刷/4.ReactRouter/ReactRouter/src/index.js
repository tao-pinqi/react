import React from 'react';
import ReactDOM from 'react-dom/client';
/*引入BrowserRouter, HashRouter组件用于创建history路由模式或者是hash路由模式*/
import {BrowserRouter, HashRouter} from "react-router-dom";
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    /*使用HashRouter组件包裹整个App组件*/
    <HashRouter>
        <App/>
    </HashRouter>
);
