import React, {Component} from 'react';
import {Routes, Route, Navigate, useRoutes} from "react-router-dom";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import routes from "./router";

/*import Home from "./views/Home/Home";
import About from "./views/About/About";
import My from "./views/My/My";
import Login from "./views/Login/Login";
import NotFound from "./views/NotFound/NotFound";
import Recommend from "./views/Home/Recommend/Recommend";
import Ranking from "./views/Home/Ranking/Ranking";*/

/*class App extends Component {
    render() {
        return (
            <div className="app">
                <Header></Header>
                <div className="main">
                    {/!*<Routes>
                        <Route path="/home" element={<Home/>}>
                            <Route path="/home" element={<Navigate to="/home/recommend"/>}></Route>
                            <Route path="/home/recommend" element={<Recommend/>}></Route>
                            <Route path="/home/ranking" element={<Ranking/>}></Route>
                        </Route>
                        <Route path="/about" element={<About/>}></Route>
                        <Route path="/my" element={<My/>}></Route>
                        <Route path="/login" element={<Login/>}></Route>
                        <Route path="/" element={<Navigate to="/home"/>}></Route>
                        <Route path="*" element={<NotFound/>}></Route>
                    </Routes>*!/}
                    {useRoutes(routes)}
                </div>
                <Footer></Footer>
            </div>
        )
    }
}*/

function App() {
    return (
        <div className="app">
            <Header></Header>
            <div className="main">
                {useRoutes(routes)}
            </div>
            <Footer></Footer>
        </div>
    )

}

export default App;
