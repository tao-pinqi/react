import {Navigate, Route, Routes} from "react-router-dom";
import Home from "../views/Home/Home";
import Recommend from "../views/Home/Recommend/Recommend";
import Ranking from "../views/Home/Ranking/Ranking";
import About from "../views/About/About";
import My from "../views/My/My";
import Login from "../views/Login/Login";
import NotFound from "../views/NotFound/NotFound";
import React from "react";

const routes = [
    {
        path: "/home",
        element: <Home></Home>,
        children: [
            {
                path: "/home",
                element: <Navigate to="/home/recommend"/>,
            },
            {
                path: "/home/recommend",
                element: <Recommend></Recommend>
            },
            {
                path: "/home/ranking",
                element: <Ranking></Ranking>
            }
        ]
    },
    {
        path: "/login",
        element: <Login></Login>
    },
    {
        path: "/my",
        element: <My></My>
    },
    {
        path: "/about",
        element: <About></About>
    },
    {
        path: "/",
        element: <Navigate to="/home"></Navigate>
    },
    {
        path: "*",
        element: <NotFound/>
    },
]
export default routes

