import {useNavigate, useParams, useSearchParams, useLocation} from "react-router-dom";


function withRouter(Component) {
    return function (props) {
        // 导航
        const navigate = useNavigate()
        // params参数
        const params = useParams()
        // search(query)参数
        const [searchParams] = useSearchParams()
        const query = Object.fromEntries(searchParams)
        // search(query)参数
        const location = useLocation()
        const router = {navigate, params, query, location}
        return (<Component {...props} router={router}></Component>)
    }
}

export default withRouter
