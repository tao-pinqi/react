import React, {Component, PureComponent} from 'react';
import {Link, NavLink, useNavigate} from "react-router-dom";
import "./Header.css"
import withRouter from "../../hoc/withRouter";

/*
function Header(props) {
    const navigate = useNavigate()
    const navigateTo = (path) => {
        return () => {
            navigate(path)
        }
    }
    return (
        <div className="header">
            <ul className="container">
                {/!*<li><NavLink to="/home" style={({isActive}) => ({color: isActive ? "red" : ""})}>首页</NavLink></li>
                    <li><NavLink to="/about" style={({isActive}) => ({color: isActive ? "red" : ""})}>关于</NavLink></li>
                    <li><NavLink to="/my" style={({isActive}) => ({color: isActive ? "red" : ""})}>我的</NavLink></li>*!/}

                {/!*<li><NavLink to="/home" className={({isActive}) => isActive ? "active" : ""}>首页</NavLink></li>
                    <li><NavLink to="/about" className={({isActive}) => isActive ? "active" : ""}>关于</NavLink></li>
                    <li><NavLink to="/my" className={({isActive}) => isActive ? "active" : ""}>我的</NavLink></li>*!/}

                <li><NavLink to="/home">首页</NavLink></li>
                <li><NavLink to="/about">关于</NavLink></li>
                <li><NavLink to="/my">我的</NavLink></li>
                <li><NavLink to="/login">登录</NavLink></li>
                <li>
                    <button onClick={navigateTo("/home")}>首页</button>
                </li>
                <li>
                    <button onClick={navigateTo("/about")}>关于</button>
                </li>
                <li>
                    <button onClick={navigateTo("/my")}>我的</button>
                </li>
                <li>
                    <button onClick={navigateTo("/login")}>登录</button>
                </li>
            </ul>
        </div>
    )
}

*/


class Header extends Component {
    render() {
        const {navigate} = this.props.router
        return (
            <div className="header">
                <ul className="container">
                    <li><NavLink to="/home">首页</NavLink></li>
                    <li><NavLink to="/about">关于</NavLink></li>
                    <li><NavLink to="/my">我的</NavLink></li>
                    <li><NavLink to="/login">登录</NavLink></li>
                    <li>
                        <button onClick={this.navigateTo("/home")}>首页</button>
                    </li>
                    <li>
                        <button onClick={this.navigateTo("/about")}>关于</button>
                    </li>
                    <li>
                        <button onClick={this.navigateTo("/my")}>我的</button>
                    </li>
                    <li>
                        <button onClick={this.navigateTo("/login")}>登录</button>
                    </li>
                </ul>
            </div>
        )
    }

    navigateTo = (path) => {
        return () => {
            this.props.router.navigate(path)
        }
    }
}


export default withRouter(Header);
