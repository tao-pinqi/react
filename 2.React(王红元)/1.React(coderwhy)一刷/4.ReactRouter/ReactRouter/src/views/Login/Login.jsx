import React, {Component} from 'react';
import {Navigate} from "react-router-dom";

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            flag: false
        }
    }

    render() {
        const {flag} = this.state
        return (
            <div className="login">
                <div className="container">
                    {!flag ? <button onClick={this.loginHandle}>登录</button> : <Navigate to="/"></Navigate>}
                </div>
            </div>
        );
    }

    loginHandle = () => {
        this.setState((state) => ({flag: !state.flag}))
    }
}

export default Login;
