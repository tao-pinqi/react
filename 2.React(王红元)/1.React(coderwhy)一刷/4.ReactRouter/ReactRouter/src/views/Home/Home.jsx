import React, {Component} from 'react';
import {NavLink, Outlet} from "react-router-dom";
import "./Home.css"

class Home extends Component {
    render() {
        return (
            <div className="home">
                <div className="top-nav">
                    <div className="container">
                        <div>
                            <NavLink to="/home/recommend">推荐</NavLink>
                            <NavLink to="/home/ranking">排行榜</NavLink>
                        </div>
                        <div>
                            我是首页
                        </div>
                        {/*占位组件*/}
                        <Outlet></Outlet>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;
