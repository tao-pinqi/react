# ReactRedux的模型图

<img src="E:\01.前端\15.React\React(张天宇)\1.React\React(上)\images\react-redux模型图.png" style="zoom: 50%;" />

```
npm i react-redux
```



# connect函数的使用

React-Redux提供connect函数，用于从 UI 组件生成容器组件。connect用于将UI组件和容器组件链接起来

conne函数允许我们将 store 中的数据作为 props 绑定到组件上

> import { connect } from 'react-redux'
>
> export default connect(mapStateToProps, mapDispatchToProps)(AppUI);

```JavaScript
import React from 'react';
import ReactDOM from 'react-dom/client';
//从redux中引入Provider组件将其包裹整个App组件
import { Provider } from "react-redux"
import store from './redux/store';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    // 传入store
    <Provider store={store}>
        <App />
    </Provider>
);
```

```JavaScript
import React, { Component } from 'react'
/* 引入connect()函数用于创建高阶组件 */
import { connect } from 'react-redux'
/* 引入创建同步action的函数 */
import { createAddAction, createChangeNameAction } from '../redux/actions/action'
class Header extends Component {
    render() {
        const { name, age } = this.props
        return (
            <div>
                <h1>姓名：{name}</h1>
                <h1>年龄：{age}</h1>
                <button onClick={this.addAgeHandle}>年龄+1</button>
                <button onClick={this.changeHandle}>修改姓名</button>
            </div>
        )
    }
    addAgeHandle = () => {
        this.props.createAddAction(1)
    }
    changeHandle = () => {
        this.props.createChangeNameAction("马化腾")
    }
}

/*const mapStateToProps = (state) => {
    return {
        name: state.name,
        age: state.age
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        createAddAction: value => dispatch(createAddAction(value)),
        createChangeNameAction: value => dispatch(createChangeNameAction(value))

    }
}*/

/* 
Connect()返回值是一个高阶组件 
第一次调用的时候传入两个函数：mapStateToProps、 mapDispatchToProps（可以是对象也可以是函数）
第二次调用的时候传入要使用store中数据的组件
*/
export default connect(({ name, age }) => ({ name, age }), {
    createAddAction,
    createChangeNameAction
})(Header)
```

# 异步action

```JavaScript
/*从redux中引入createStore()函数用于创建一个store,引入applyMiddleware()创建中间件*/
import {createStore, applyMiddleware} from "redux";
/*引入reducer*/
import {reducer} from "./reducer/reducer";
/*引入thunk用于支持异步action*/
import thunk from "redux-thunk"
/*DevTools开发者工具*/
import {composeWithDevTools} from "redux-devtools-extension";
/*调用createStore()传入reducer作为参数*/
const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)))
/*导出创建好的store*/
export default store
```

```JavaScript
import {changeNews} from "../constants/constants";
import axios from "axios";

export const createChangeNewsAction = () => {
    /*
    * 如果action返回的是一个函数(异步action)，redux默认是不支持的
    * 1.返回一个函数进行异步操作需要借助中间件 redux-thunk
    */
    return async (dispatch, getState) => {
        /*
        *  dispatch函数：用于再次派发action
        *  getState函数：考虑到之后的一些操作需要依赖原来状态，用于获取之前的状态
        */
        try {
            const {data: {data}} = await axios.get("http://ajax-api.itheima.net/api/news")
            dispatch({type: changeNews, news: data})
            return data
        } catch (error) {
            return Promise.reject(error.message)
        }
    }
}
```

```JavaScript
import {changeNews} from "../constants/constants";

const initState = {
    news: []
}

export const reducer = (preState = initState, action) => {
    const {type, news} = action
    switch (type) {
        case changeNews:
            return {news: [...news]}
        default:
            return preState
    }
}
```

# 模块化的reducer

```JavaScript
import {createStore, applyMiddleware} from "redux";
import thunk from "redux-thunk"
import {composeWithDevTools} from "redux-devtools-extension";
import totalReducer from "./totalReducer";

const store = createStore(totalReducer, composeWithDevTools(applyMiddleware(thunk)))
export default store
```

```JavaScript
import {combineReducers} from "redux";
import {newsReducer} from "./newsStore/reducer/reducer";
import {countReducer} from "./countStore/reducer/reducer";

export default combineReducers({
    news: newsReducer,
    count: countReducer
})
```

```JavaScript
import {changeNews} from "../constants/constants";
import axios from "axios";

export const createChangeNewsAction = () => {
    return async (dispatch, getState) => {
        try {
            const {data: {data}} = await axios.get("http://ajax-api.itheima.net/api/news")
            dispatch({type: changeNews, news: data})
            return data
        } catch (error) {
            return Promise.reject(error.message)
        }
    }
}
```

```JavaScript
import {changeNews} from "../constants/constants";

const initState = {
    news: []
}

export const newsReducer = (preState = initState, action) => {
    const {type, news} = action
    switch (type) {
        case changeNews:
            return {news: [...news]}
        default:
            return preState
    }
}
```

```JavaScript
import React, {Component} from 'react';
import {connect} from "react-redux";

class Content extends Component {
    render() {
        const {news,total} = this.props
        return (
            <div>
                <div className='Main'>
                    <h1>总共{total}条数据</h1>
                    <ul>
                        {news.map(item => <li key={item.id} style={{margin: 10 + "px"}}>{item.title}</li>)}
                    </ul>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        news: state.news.news,
        total:state.news.news.length
    }
}
export default connect(mapStateToProps, null)(Content);
```
