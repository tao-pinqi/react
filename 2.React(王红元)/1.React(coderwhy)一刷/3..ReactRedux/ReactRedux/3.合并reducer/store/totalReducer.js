import {combineReducers} from "redux";
import {newsReducer} from "./newsStore/reducer/reducer";
import {countReducer} from "./countStore/reducer/reducer";

export default combineReducers({
    news: newsReducer,
    count: countReducer
})
