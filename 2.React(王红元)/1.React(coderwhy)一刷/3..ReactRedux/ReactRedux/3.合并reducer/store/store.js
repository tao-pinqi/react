import {createStore, applyMiddleware} from "redux";
import thunk from "redux-thunk"
import {composeWithDevTools} from "redux-devtools-extension";
import totalReducer from "./totalReducer";

const store = createStore(totalReducer, composeWithDevTools(applyMiddleware(thunk)))
export default store
