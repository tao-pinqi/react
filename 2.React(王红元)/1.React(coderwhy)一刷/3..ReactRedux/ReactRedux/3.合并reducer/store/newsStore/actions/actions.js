import {changeNews} from "../constants/constants";
import axios from "axios";

export const createChangeNewsAction = () => {
    return async (dispatch, getState) => {
        try {
            const {data: {data}} = await axios.get("http://ajax-api.itheima.net/api/news")
            dispatch({type: changeNews, news: data})
            return data
        } catch (error) {
            return Promise.reject(error.message)
        }
    }
}
