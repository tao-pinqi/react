import React, {Component} from 'react';
import {connect} from "react-redux";

class Content extends Component {
    render() {
        const {news,total} = this.props
        return (
            <div>
                <div className='Main'>
                    <h1>总共{total}条数据</h1>
                    <ul>
                        {news.map(item => <li key={item.id} style={{margin: 10 + "px"}}>{item.title}</li>)}
                    </ul>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        news: state.news.news,
        total:state.news.news.length
    }
}
export default connect(mapStateToProps, null)(Content);
