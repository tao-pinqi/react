import {changeNews} from "../constants/constants";
import axios from "axios";

export const createChangeNewsAction = () => {
    /*
    * 如果action返回的是一个函数(异步action)，redux默认是不支持的
    * 1.返回一个函数进行异步操作需要借助中间件 redux-thunk
    */
    return async (dispatch, getState) => {
        /*
        *  dispatch函数：用于再次派发action
        *  getState函数：考虑到之后的一些操作需要依赖原来状态，用于获取之前的状态
        */
        try {
            const {data: {data}} = await axios.get("http://ajax-api.itheima.net/api/news")
            dispatch({type: changeNews, news: data})
            return data
        } catch (error) {
            return Promise.reject(error.message)
        }
    }
}
