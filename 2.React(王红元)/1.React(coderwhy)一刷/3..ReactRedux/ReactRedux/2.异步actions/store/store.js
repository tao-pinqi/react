/*从redux中引入createStore()函数用于创建一个store,引入applyMiddleware()创建中间件*/
import {createStore, applyMiddleware} from "redux";
/*引入reducer*/
import {reducer} from "./reducer/reducer";
import thunk from "redux-thunk"
/*DevTools开发者工具*/
import {composeWithDevTools} from "redux-devtools-extension";
/*调用createStore()传入reducer作为参数*/
const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)))
/*导出创建好的store*/
export default store
