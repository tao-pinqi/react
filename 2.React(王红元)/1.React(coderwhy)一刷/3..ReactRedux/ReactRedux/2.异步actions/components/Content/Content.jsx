import React, {Component} from 'react';
import {connect} from "react-redux";

class Content extends Component {
    render() {
        const {news} = this.props
        return (
            <div>
                <div className='Main'>
                    <h1>新闻列表2</h1>
                    <ul>
                        {news.map(item => <li key={item.id} style={{margin: 10 + "px"}}>{item.title}</li>)}
                    </ul>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        news: state.news
    }
}
export default connect(mapStateToProps, null)(Content);
