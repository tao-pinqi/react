import React, {Component} from 'react'
import axios from "axios";
import {connect} from "react-redux";
import {createChangeNewsAction} from "../../store/actions/actions";

class Main extends Component {
    render() {
        const {news} = this.props
        return (
            <div className='Main'>
                <h1>新闻列表1</h1>
                <ul>
                    {news.map(item => <li key={item.id} style={{margin: 10 + "px"}}>{item.title}</li>)}
                </ul>
            </div>
        )
    }

    fetchNews = async () => {
        try {
            const result = await this.props.changeNews()
            console.log(result, "请求新闻列表成功")
        } catch (error) {
            console.log(error.message)
        }
    }

    componentDidMount() {
        this.fetchNews()
    }
}

const mapStateToProps = (state) => {
    return {
        news: state.news
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeNews: () => dispatch(createChangeNewsAction())
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Main)
