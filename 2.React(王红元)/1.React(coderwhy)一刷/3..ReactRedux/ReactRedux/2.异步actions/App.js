import React, {Component} from 'react'
import "./App.css"
import Main from './components/Main/Main'
import Content from "./components/Content/Content";

export default class App extends Component {
    constructor(props) {
        super(props)
    }

    render() {

        return (
            <div className='app layui-panel'>
                <Main></Main>
                <hr/>
                <Content></Content>
            </div>
        )
    }
}
