import React from 'react';
import ReactDOM from 'react-dom/client';
/*引入Provider组件并且用这个组件包裹整个App组件*/
import {Provider} from "react-redux";
/*引入store*/
import store from "./store/store";
import App from './App';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    /*将store传给Provider组件*/
    <Provider store={store}>
        <App></App>
    </Provider>
)

