import {changeNews} from "../constants/constants";

const initState = {
    news: []
}

export const newsReducer = (preState = initState, action) => {
    const {type, news} = action
    switch (type) {
        case changeNews:
            return {news: [...news]}
        default:
            return preState
    }
}
