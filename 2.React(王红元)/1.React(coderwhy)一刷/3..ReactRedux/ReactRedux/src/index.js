import React from 'react';
import ReactDOM from 'react-dom/client';
import {Provider} from "react-redux";
import store from "./store/store";
import App from './App';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    /*将store传给Provider组件*/
    <Provider store={store}>
        <App></App>
    </Provider>
)

