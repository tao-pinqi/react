import React, {Component} from 'react'
/*引入connect()函数用于链接UI组件和容器组件*/
import {connect} from "react-redux"
/*引入store*/
import store from "../../store/store";
import "./Footer.css"
import {changeAllTodo, clearCheckToto} from "../../store/constants/constants";
import {createClearCheckedTodoAction, createChangeAllTodoAction} from "../../store/actions/actions";

class Footer extends Component {
    render() {
        const {totalTodoList, checkedTodoList, isAll} = this.props
        return (
            <div className='footer'>
                <div className='left'>
                    <input type="checkbox" checked={isAll && checkedTodoList} onChange={(event) => this.changeHandler(event.target.checked)}/>
                    <span>已完成 {checkedTodoList} / 全部 {totalTodoList}</span>
                </div>
                <div className='right'>
                    <button type="button" className="layui-btn layui-bg-blue layui-btn-sm" onClick={this.clickHandler}>清除已完成任务</button>
                </div>
            </div>
        )
    }

    clickHandler = () => {
        this.props.clearCheckToto()
    }
    changeHandler = (done) => {
        this.props.changeAllTodo(!this.props.isAll)
    }
}

/*stateToProps是一个函数，这个函数需要返回一个对象（用于将store中state映射到组件中的props中*/
const mapStateToProps = (state) => {
    return {
        totalTodoList: state.length,
        checkedTodoList: state.filter(todo => todo.done).length,
        isAll: state.every(todo => todo.done)
    }
}
/*dispatchToProps是一个函数，这个函数需要返回一个对象（用于将store中dispatch映射到组件中的props中*/
const mapDispatchToProps = (dispatch) => {
    return {
        clearCheckToto: () => dispatch(createClearCheckedTodoAction()),
        changeAllTodo: (done) => dispatch(createChangeAllTodoAction(done))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Footer)
