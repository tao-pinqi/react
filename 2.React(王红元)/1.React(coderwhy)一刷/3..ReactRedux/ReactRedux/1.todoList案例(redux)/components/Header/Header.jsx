import React, {Component} from 'react'
/*引入connect()函数用于链接UI组件和容器组件*/
import {connect} from "react-redux"
/*引入创建action的函数*/
import {createAddTodoAction} from "../../store/actions/actions";
import "./Header.css"

class Header extends Component {
    inputRef = React.createRef()

    render() {
        return (
            <div className='header'>
                <input type="text" name="" placeholder="请输入任务的名称" className="layui-input" ref={this.inputRef}/>
                <button type="button" className="layui-btn layui-btn layui-btn-sm layui-icon-addition layui-icon" onClick={this.clickHandle}>添加任务</button>
            </div>
        )
    }

    clickHandle = () => {
        const {value} = this.inputRef.current
        const newTodo = {id: Math.random(), name: value, done: true}
        this.props.addTodo(newTodo)
    }
}

/*stateToProps是一个函数，这个函数需要返回一个对象（用于将store中state映射到组件中的props中*/
const mapStateToProps = (state) => {
    return {}
}
/*dispatchToProps是一个函数，这个函数需要返回一个对象（用于将store中dispatch映射到组件中的props中*/
const mapDispatchToProps = (dispatch) => {
    return {
        addTodo: todoObject => {
            dispatch(createAddTodoAction(todoObject))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header)
