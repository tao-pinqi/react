import React, {Component} from 'react'
import Item from './Item/Item'
/*引入connect()函数用于链接UI组件和容器组件*/
import {connect} from "react-redux"
import "./Main.css"

class Main extends Component {

    render() {
        const {todoList} = this.props
        return (
            <div className="main">
                {todoList.map(item => <Item todo={item} key={item.id}></Item>)}
            </div>
        )
    }
}

/*stateToProps是一个函数，这个函数需要返回一个对象（用于将store中state映射到组件中的props中*/
const stateToProps = (state) => {
    return {
        // todoList列表数组
        todoList: state,
    }
}
/*dispatchToProps是一个函数，这个函数需要返回一个对象（用于将store中dispatch映射到组件中的props中*/
const dispatchToProps = (dispatch) => {
    return {}
}
export default connect(stateToProps, dispatchToProps)(Main)
