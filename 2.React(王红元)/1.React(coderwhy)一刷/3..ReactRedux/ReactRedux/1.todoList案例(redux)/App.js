import React, {Component} from 'react'
import Header from './components/Header/Header'
import Main from './components/Main/Main'
import Footer from './components/Footer/Footer'
import "./App.css"

export default class App extends Component {
    constructor(props) {
        super(props)
    }

    render() {

        return (
            <div className='app layui-panel'>
                {/* 头部组件 */}
                <Header></Header>
                {/* 主体组件 */}
                <Main></Main>
                {/* 底部组件 */}
                <Footer></Footer>
            </div>
        )
    }
}
