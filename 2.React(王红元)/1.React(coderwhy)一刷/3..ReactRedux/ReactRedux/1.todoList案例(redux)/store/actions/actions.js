import {addTodo, deleteTodo, clearCheckToto, changeTodoDone,changeAllTodo} from "../constants/constants";

/*添加一个todo对象的action*/
export const createAddTodoAction = (todoObject) => ({type: addTodo, todoObject})

/*删除一个todo对象的action*/
export const createDeleteTodoAction = (id) => ({type: deleteTodo, id})

/*清空已经完成的todo*/
export const createClearCheckedTodoAction = () => ({type: clearCheckToto})

/*修改某一个todo状态*/
export const createChangeTodoDoneAction = ({id, done}) => ({type: changeTodoDone, id, done})

/*全选和取消全选*/
export const createChangeAllTodoAction=(done)=>({type:changeAllTodo,done})
