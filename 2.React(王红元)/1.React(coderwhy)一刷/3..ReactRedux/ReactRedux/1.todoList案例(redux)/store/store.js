/*从redux中引入createStore()函数用于创建一个store*/
import {createStore} from "redux";
/*引入reducer*/
import {reducer} from "./reducer/reducer";
/*调用createStore()传入reducer作为参数*/
const store = createStore(reducer)
/*导出创建好的store*/
export default store
