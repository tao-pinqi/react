import {createContext} from "react";

const themeContext = createContext(null)
const userContext = createContext(null)

export {
    themeContext,
    userContext
}
