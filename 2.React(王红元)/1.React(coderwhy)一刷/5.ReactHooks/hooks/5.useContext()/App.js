import {useEffect, useState, useContext} from "react";
import {themeContext, userContext} from "./context/context";

/*
useContext是一个 React Hook，可以让你读取和订阅组件中的context
    参数：SomeContext：先前用`createContext`创建的 context，context本身不包含信息，它只代表你可以提供或从组件中读取的信息类型
    返回值：
        useContext为调用组件返回 context 的值。它被确定为传递给树中调用组件上方最近的SomeContext.Provider的value
        如果没有这样的 provider，那么返回值将会是为创建该context传递给createContext的defaultValue。返回的值始终是最新的。
        如果 context 发生变化，React 会自动重新渲染读取 context 的组件
*/
function App(props) {
    const theme = useContext(themeContext)
    const user = useContext(userContext)
    return (
        <div className="app">
            <div className="header">
                <h1 style={{...theme}}>themeContext：{theme.color} {theme.fontSize}</h1>
            </div>
            <div className="header">
                <h1>userContext：{user.name} {user.age}</h1>
            </div>
        </div>
    );
}

export default App;
