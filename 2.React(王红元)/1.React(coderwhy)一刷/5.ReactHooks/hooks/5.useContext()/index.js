import React from 'react';
import ReactDOM from 'react-dom/client';
import {themeContext, userContext} from "./context/context";
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <themeContext.Provider value={{color: "#c00000", fontSize: "20px"}}>
        <userContext.Provider value={{name: "陶品奇", age: 22}}>
            <App></App>
        </userContext.Provider>
    </themeContext.Provider>)
