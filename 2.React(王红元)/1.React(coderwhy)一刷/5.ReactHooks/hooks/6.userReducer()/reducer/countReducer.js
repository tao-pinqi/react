function counterReducer(state, action) {
    const {type, value} = action
    switch (type) {
        case "add":
            return {...state, count: state.count + value}
        case "sub":
            return {...state, count: state.count - value}
        default:
            return state
    }
}

export default counterReducer
