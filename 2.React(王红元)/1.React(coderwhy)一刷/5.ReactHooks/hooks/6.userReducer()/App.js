import {useEffect, useState, useContext, useReducer} from "react";
import counterReducer from "./reducer/countReducer";
/*
*参数：
*	reducer：用于更新state的纯函数。参数为state和action，返回值是更新后的state。state与action可以是任意合法值。
*	initialArg：用于初始化state的任意值。初始值的计算逻辑取决于接下来的init参数。
*	可选参数init：用于计算初始值的函数。如果存在，使用init(initialArg)的执行结果作为初始值，否则使用initialArg。
*返回值：useReducer 返回一个由两个值组成的数组：
*	当前的state：初次渲染时，它是init(initialArg)或initialArg（如果没有init函数）
*    dispatch函数：用于更新 state 并触发组件的重新渲染。
*/
function App(props) {
    const [state, dispatch] = useReducer(counterReducer, {count: 0})
    const add = (value) => {
        dispatch({type: "add", value})
    }
    const sub = (value) => {
        dispatch({type: "sub", value})
    }
    return (
        <div>
            <h1>count的值为：{state.count}</h1>
            <button onClick={() => add(1)}>+1</button>
            <button onClick={() => sub(1)}>-1</button>
        </div>
    );
}

export default App;
