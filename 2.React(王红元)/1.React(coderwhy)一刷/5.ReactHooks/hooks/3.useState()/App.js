import { useState } from "react";
/*
* useState()来自react需要从react中导入，它是一个hook
* 1.参数：初始化值，如果不设置默认为undefined
* 2.返回值：数组，包含两个元素
*   元素一：当前状态的值(第一次调用的初始化值)
*   元素二：设置状态值的函数
*/

/*
* useState 是一个 React Hook，它允许你向组件添加一个状态变量
* const [state, setState] = useState(initialState)
*
*/
function App(props) {
    const [count, setCount] = useState(0)
    const changeCount = (value) => {
        setCount(count + value)
    }
    return (
        <div className="app">
            <h1>count的值为：{count}</h1>
            <button onClick={() => changeCount(+1)}>+1</button>
            <button onClick={() => changeCount(-1)}>-1</button>
        </div>
    );
}

export default App;
