import {memo} from "react";

function Add(props) {
    console.log("Add组件被重新渲染")
    return (
        <div>
            <button onClick={props.add}>+1</button>
        </div>
    );
}

export default memo(Add);
