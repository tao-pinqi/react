import {memo} from "react";

function Sub(props) {
    console.log("Sub组件被重新渲染")
    return (
        <div>
            <button onClick={props.sub}>-1</button>
        </div>
    );
}

export default memo(Sub);
