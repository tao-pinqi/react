import {useEffect, useState, useContext, useReducer, useCallback, useRef} from "react";
import Add from "./components/add";
import Sub from "./components/sub";

/*
* useCallback性能优化的点：
*   1.当需要将一个函数作为props传递给子组件时，最好使用useCallback进行性能优化，将优化之后的函数传递给子组件
*useRef()在组件多次渲染时，返回同一个值
*/

function App(props) {
    /*
    * 优化：当count发生改变时，也使用同一个函数
    *   做法一：将count的依赖值移除掉，缺点：闭包陷阱
    *   做法二：useRef，在组件多次渲染时，返回的是同一个值
    * */
    const [count, setCount] = useState(0)

    const countRef = useRef()
    countRef.current = count

    const add = useCallback(() => {
        setCount(countRef.current + 1)
    }, [])

    const sub = useCallback(() => {
        setCount(countRef.current - 1)
    }, [])

    return (
        <div>
            <h1>count的值为：{count}</h1>
            <Add add={add}></Add>
            <Sub sub={sub}></Sub>
        </div>
    );
}

export default App;
