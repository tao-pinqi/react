import {useEffect, useState} from "react";

/*
* 通过useEffect()可以告诉React需要在渲染后执行某些操作
* useEffect()要求我们传入一个回调函数，在React执行完更新DOM操作之后就会回调这个函数
* 默认情况下，无论是第一次渲染之后，还是每次更新之后，都会执行这个回调函数
*
*useEffect()实际上有两个参数
*   参数一：执行的回调函数
*   参数二：该useEffect在哪些state发生变化的时候才重新执行(受谁的影响)
*如果一个函数不希望依赖任何的内容时，也可以传入一个空的数组（所对于componentDidmount和componentWillUnmount生命周期函数了）
*/
function App(props) {
    const [count, setCount] = useState(0)
    const changeCount = () => {
        setCount(count + 1)
    }

    useEffect(() => {
        document.title = count
    }, [])

    /*一个函数组件中可以有多个useEffect()*/
    useEffect(() => {
        /*返回值：回调函数=>组件被重新渲染或者组件被卸载的时候执行*/
        return () => {
            console.log("组件被卸载的时候执行")
        }
    }, [])

    useEffect(() => {
        console.log(count, "count发生变化了")
    }, [count]);

    return (
        <div className="app">
            <h1>count的值为：{count}</h1>
            <button onClick={changeCount}>+1</button>
        </div>
    )
}

export default App;
