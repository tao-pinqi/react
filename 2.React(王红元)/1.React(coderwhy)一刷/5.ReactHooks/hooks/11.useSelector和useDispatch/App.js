import {useSelector, useDispatch, connect} from "react-redux";

import {addNumber, subNumber} from "./store/modules/counter";

function App(props) {
    // 使用useSelector()将redux中store中的数据映射到组件
    const {counter} = useSelector((state) => {
        return {
            counter: state.counter.counter
        }
    })

    // 使用useDispatch()将redux中action映射到组件
    const dispatch = useDispatch()

    const add = () => {
        dispatch(addNumber(1))
    }
    const sub = () => {
        dispatch(subNumber(1))
    }
    return (
        <div>
            <h1>counter的值为:{counter}</h1>
            <button onClick={add}>+1</button>
            <button onClick={sub}>-1</button>
        </div>
    )
}

export default App


/*function App(props) {
    const {counter, addNumber, subNumber} = props
    return (
        <div>
            <h1>counter的值为:{counter}</h1>
            <button onClick={addNumber}>+1</button>
            <button onClick={subNumber}>-1</button>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        counter: state.counter.counter
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        addNumber: () => {
            dispatch(addNumber(1))
        },
        subNumber: () => {
            dispatch(subNumber(1))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(App)*/
