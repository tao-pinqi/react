import {configureStore} from "@reduxjs/toolkit";

import counterReducer from "../store/modules/counter"

const store = configureStore({
    reducer: {
        counter: counterReducer
    }
})
export default store
