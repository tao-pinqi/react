import {useEffect, useState, useContext, useReducer, useCallback, useRef, useMemo} from "react";

function App(props) {
    const inputRef = useRef()

    const countRef = useRef()
    const [count, setCount] = useState(0)
    countRef.current = count

    const add=()=>{
        setCount(countRef.current+1)
    }

    useEffect(() => {
        inputRef.current.focus()
    }, [])

    return (
        <div className="app">
            <input type="text" ref={inputRef}/>
            <h1>count的值为:{count}</h1>
            <button onClick={add}>+1</button>
        </div>
    );
}

export default App;
