import React, { Component } from 'react'

export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = { count: 0 }
    }
    render() {
        const { count } = this.state
        return (
            <div className='app'>
                <h1>count的值为：{count}</h1>
                <button onClick={() => this.changeCount(+1)}>+1</button>
                <button onClick={() => this.changeCount(-1)}>-1</button>
            </div>
        )
    }
    changeCount = (value) => {
        this.setState((state) => {
            return {
                count: state.count + value
            }
        })
    }
}
