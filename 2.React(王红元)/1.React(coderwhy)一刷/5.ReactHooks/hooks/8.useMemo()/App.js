import {useEffect, useState, useContext, useReducer, useCallback, useRef, useMemo} from "react";

function getSum(num) {
    console.log("getSum函数被调用---")
    let sum = 0
    for (let i = 0; i <= num; i++) {
        sum += i
    }
    return sum
}

function App(props) {
    const [count, setCount] = useState(10)
    const result = useMemo(() => getSum(count), [])
    const info = useMemo(() => ({name: "陶品奇", age: 22}))
    return (
        <div>
            <h1>count的值为:{count}</h1>
            <button onClick={() => setCount(count + 1)}>+1</button>

        </div>
    );
}

export default App;
