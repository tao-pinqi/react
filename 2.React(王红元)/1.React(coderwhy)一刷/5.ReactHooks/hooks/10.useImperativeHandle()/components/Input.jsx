import {forwardRef, useRef, useImperativeHandle} from 'react';

const Input = forwardRef(function Input(props, ref) {
    const inputRef = useRef()
    useImperativeHandle(ref, () => {
        return {
            focus() {
                inputRef.current.focus()
            },
            setValue(value){
                inputRef.current.value=value
            }
        }
    })
    return (
        <div>
            <input type="text" {...props} ref={inputRef}/>
        </div>
    );
})

export default Input;
