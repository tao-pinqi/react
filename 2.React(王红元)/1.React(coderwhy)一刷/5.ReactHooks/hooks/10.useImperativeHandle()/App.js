import {useEffect, useState, useContext, useReducer, useCallback, useRef, useMemo, useImperativeHandle, forwardRef} from "react";
import Input from "./components/Input";


function App(props) {
    const InputRef = useRef()

    useEffect(() => {
        InputRef.current.focus()
        InputRef.current.setValue("陶品奇")
    }, [])

    return (
        <div>
            <Input ref={InputRef}></Input>
        </div>
    );

}

export default App;
