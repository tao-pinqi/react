# useState()

`useState` 是一个 React Hook，它允许你向组件添加一个 状态变量

```JavaScript
const [state, setState] = useState(initialState);
```

**参数**：`initialState`：你希望 state 初始化的值。它可以是任何类型的值，但对于函数有特殊的行为。在初始渲染后，此参数将被忽略

**返回值**：`useState` 返回一个由两个值组成的数组：

​	当前的 state。在首次渲染时，它将与你传递的 `initialState` 相匹配。

​	set函数，它可以让你将 state 更新为不同的值并触发重新渲染。

```JavaScript
import { useState } from "react";
/*
* useState()来自react需要从react中导入，它是一个hook
* 1.参数：初始化值，如果不设置默认为undefined
* 2.返回值：数组，包含两个元素
*   元素一：当前状态的值(第一次调用的初始化值)
*   元素二：设置状态值的函数
*/

/*
* useState 是一个 React Hook，它允许你向组件添加一个状态变量
* const [state, setState] = useState(initialState)
*/
function App(props) {
    const [count, setCount] = useState(0)
    const changeCount = (value) => {
        setCount(count + value)
    }
    return (
        <div className="app">
            <h1>count的值为：{count}</h1>
            <button onClick={() => changeCount(+1)}>+1</button>
            <button onClick={() => changeCount(-1)}>-1</button>
        </div>
    );
}
export default App;
```



# useEffect()

```JavaScript
useEffect(setup, dependencies?)
```

```JavaScript
import {useEffect, useState} from "react";

/*
* 通过useEffect()可以告诉React需要在渲染后执行某些操作
* useEffect()要求我们传入一个回调函数，在React执行完更新DOM操作之后就会回调这个函数
* 默认情况下，无论是第一次渲染之后，还是每次更新之后，都会执行这个回调函数
*
*useEffect()实际上有两个参数
*   参数一：执行的回调函数
*   参数二：该useEffect在哪些state发生变化的时候才重新执行(受谁的影响)
*如果一个函数不希望依赖任何的内容时，也可以传入一个空的数组（所对于componentDidmount和componentWillUnmount生命周期函数了）
*/
function App(props) {
    const [count, setCount] = useState(0)
    const changeCount = () => {
        setCount(count + 1)
    }

    useEffect(() => {
        document.title = count
    }, [])

    /*一个函数组件中可以有多个useEffect()*/
    useEffect(() => {
        /*返回值：回调函数=>组件被重新渲染或者组件被卸载的时候执行*/
        return () => {
            console.log("组件被卸载的时候执行")
        }
    }, [])

    useEffect(() => {
        console.log(count, "count发生变化了")
    }, [count]);

    return (
        <div className="app">
            <h1>count的值为：{count}</h1>
            <button onClick={changeCount}>+1</button>
        </div>
    )
}

export default App;
```



# useContext()

`useContext` 是一个 React Hook，可以让你读取和订阅组件中的context

```javascript
const value = useContext(SomeContext)
```

**参数** :`SomeContext`：先前用`createContext`创建的 context，context本身不包含信息，它只代表你可以提供或从组件中读取的信息类型

**返回值**：`useContext` 为调用组件返回 context 的值。它被确定为传递给树中调用组件上方最近的 `SomeContext.Provider` 的 `value`。如果没有这样的 provider，那么返回值将会是为创建该 context 传递给`createContext`的 `defaultValue`。返回的值始终是最新的。如果 context 发生变化，React 会自动重新渲染读取 context 的组件

```javascript
import {useEffect, useState, useContext} from "react";
import {themeContext, userContext} from "./context/context";


function App(props) {
    const theme = useContext(themeContext)
    const user = useContext(userContext)
    return (
        <div className="app">
            <div className="header">
                <h1 style={{...theme}}>themeContext：{theme.color} {theme.fontSize}</h1>
            </div>
            <div className="header">
                <h1>userContext：{user.name} {user.age}</h1>
            </div>
        </div>
    );
}

export default App;
```



# useReducer()

`useReducer` 是一个 React Hook，它允许你向组件里面添加一个reducer

```JavaScript
const [state, dispatch] = useReducer(reducer, initialArg, init?)
```

**参数：**

​	`reducer`：用于更新 state 的纯函数。参数为 state 和 action，返回值是更新后的 state。state 与 action 可以是任意合法值。

​	`initialArg`：用于初始化 state 的任意值。初始值的计算逻辑取决于接下来的 `init` 参数。

​	**可选参数** `init`：用于计算初始值的函数。如果存在，使用 `init(initialArg)` 的执行结果作为初始值，否则使用 `initialArg`。

**返回值：**

​	`useReducer` 返回一个由两个值组成的数组：

​		当前的 state：初次渲染时，它是 `init(initialArg)` 或 `initialArg` （如果没有 `init` 函数）

​       	`dispatch`函数：用于更新 state 并触发组件的重新渲染。

```JavaScript
import {useEffect, useState, useContext, useReducer} from "react";
import counterReducer from "./reducer/countReducer";
function App(props) {
    const [state, dispatch] = useReducer(counterReducer, {count: 0})
    const add = (value) => {
        dispatch({type: "add", value})
    }
    const sub = (value) => {
        dispatch({type: "sub", value})
    }
    return (
        <div>
            <h1>count的值为：{state.count}</h1>
            <button onClick={() => add(1)}>+1</button>
            <button onClick={() => sub(1)}>-1</button>
        </div>
    );
}

export default App;
```

```JavaScript
function counterReducer(state, action) {
    const {type, value} = action
    switch (type) {
        case "add":
            return {...state, count: state.count + value}
        case "sub":
            return {...state, count: state.count - value}
        default:
            return state
    }
}

export default counterReducer
```



# useCallback()

`useCallback` 是一个允许你在多次渲染中缓存函数的 React Hook

useCallback会返回一个函数的memoized(记忆的)值，在依赖不变的情况下，多次定义的时候，返回的值是相同的

```JavaScript
const cachedFn = useCallback(fn, dependencies)
```

```JavaScript
import {useEffect, useState, useContext, useReducer, useCallback, useRef} from "react";
import Add from "./components/add";
import Sub from "./components/sub";

/*
* useCallback性能优化的点：
*   1.当需要将一个函数作为props传递给子组件时，最好使用useCallback进行性能优化，将优化之后的函数传递给子组件
*useRef()在组件多次渲染时，返回同一个值
*/

function App(props) {
    /*
    * 优化：当count发生改变时，也使用同一个函数
    *   做法一：将count的依赖值移除掉，缺点：闭包陷阱
    *   做法二：useRef，在组件多次渲染时，返回的是同一个值
    * */
    const [count, setCount] = useState(0)

    const countRef = useRef()
    countRef.current = count

    const add = useCallback(() => {
        setCount(countRef.current + 1)
    }, [])

    const sub = useCallback(() => {
        setCount(countRef.current - 1)
    }, [])

    return (
        <div>
            <h1>count的值为：{count}</h1>
            <Add add={add}></Add>
            <Sub sub={sub}></Sub>
        </div>
    );
}

export default App;
```



# useMemo()

`useMemo` 是一个 React Hook，它在每次重新渲染的时候能够缓存计算的结果

useMemo返回的也是一个memoized(记忆的)值，在依赖不变的情况下，多次定义的时候，返回的值是相同的

```JavaScript
const cachedValue = useMemo(calculateValue, dependencies)
```

```JavaScript
import {useEffect, useState, useContext, useReducer, useCallback, useRef, useMemo} from "react";

function getSum(num) {
    console.log("getSum函数被调用---")
    let sum = 0
    for (let i = 0; i <= num; i++) {
        sum += i
    }
    return sum
}

function App(props) {
    const [count, setCount] = useState(10)
    const result = useMemo(() => getSum(count), [])
    const info = useMemo(() => ({name: "陶品奇", age: 22}))
    return (
        <div>
            <h1>count的值为:{count}</h1>
            <button onClick={() => setCount(count + 1)}>+1</button>

        </div>
    );
}

export default App;
```



# useRef()

`useRef` 是一个 React Hook，它能让你引用一个不需要渲染的值

useRef返回的是一个ref对象，返回的ref对象在组件的整个生命周期保持不变

用法一：引入DOM或者是组件(class组件)

用法二：保存一个数据，这个对象在整个生命周期中可以保持不变

```JavaScript
const ref = useRef(initialValue)
```

```JavaScript
import {useEffect, useState, useContext, useReducer, useCallback, useRef, useMemo} from "react";

function App(props) {
    const inputRef = useRef()

    const countRef = useRef()
    const [count, setCount] = useState(0)
    countRef.current = count

    const add=()=>{
        setCount(countRef.current+1)
    }

    useEffect(() => {
        inputRef.current.focus()
    }, [])

    return (
        <div className="app">
            <input type="text" ref={inputRef}/>
            <h1>count的值为:{count}</h1>
            <button onClick={add}>+1</button>
        </div>
    );
}

export default App;
```



# useImperativeHandle()

`useImperativeHandle` 是 React 中的一个 Hook，它能让你自定义由ref暴露出来的句柄

```javascript
useImperativeHandle(ref, createHandle, dependencies?)
```

```JavaScript
import {useEffect, useState, useContext, useReducer, useCallback, useRef, useMemo, useImperativeHandle, forwardRef} from "react";
import Input from "./components/Input";

function App(props) {
    const InputRef = useRef()

    useEffect(() => {
        InputRef.current.focus()
        InputRef.current.setValue("陶品奇")
    }, [])

    return (
        <div>
            <Input ref={InputRef}></Input>
        </div>
    );

}
export default App;
```

```JavaScript
import {forwardRef, useRef, useImperativeHandle} from 'react';

const Input = forwardRef(function Input(props, ref) {
    const inputRef = useRef()
    useImperativeHandle(ref, () => {
        return {
            focus() {
                inputRef.current.focus()
            },
            setValue(value){
                inputRef.current.value=value
            }
        }
    })
    return (
        <div>
            <input type="text" {...props} ref={inputRef}/>
        </div>
    );
})

export default Input;
```
